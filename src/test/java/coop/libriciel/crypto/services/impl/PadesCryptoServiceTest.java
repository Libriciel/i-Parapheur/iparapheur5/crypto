/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import coop.libriciel.crypto.models.DataToSign;
import coop.libriciel.crypto.models.dss.StreamableDssDocument;
import coop.libriciel.crypto.models.request.DataToSignHolder;
import coop.libriciel.crypto.models.request.PadesParameters;
import coop.libriciel.crypto.models.request.legacy.LegacyDataToSignHolder;
import coop.libriciel.crypto.models.request.legacy.LegacyPadesSignatureParameters;
import coop.libriciel.crypto.models.stamp.PdfSignatureStamp;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.pades.PAdESSignatureParameters;
import lombok.extern.log4j.Log4j2;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static coop.libriciel.crypto.TestUtils.*;
import static coop.libriciel.crypto.services.impl.PadesCryptoService.PAYLOAD_KEY_SIGNATURE_STAMP_YAML_BASE64;
import static coop.libriciel.crypto.utils.PdfAssert.assertSignatureLocation;
import static eu.europa.esig.dss.enumerations.SignatureAlgorithm.RSA_SHA256;
import static java.util.Collections.singletonList;
import static org.apache.commons.io.FileUtils.writeByteArrayToFile;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;


@Log4j2
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class PadesCryptoServiceTest {


    private @Autowired PadesCryptoService padesCryptoService;
    protected final ClassLoader classLoader = getClass().getClassLoader();


    @Test
    void generateDataToSign() throws Exception {

        // Preparing parameters

        LegacyPadesSignatureParameters requestParams = new LegacyPadesSignatureParameters();
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setSignatureDateTime(new Date().getTime());
        PAdESSignatureParameters params = padesCryptoService.getSignatureParameters(requestParams, null);

        InputStream inputStream = classLoader.getResourceAsStream(PDF_RESOURCE_NAME);
        assertNotNull(inputStream);
        DSSDocument document = new StreamableDssDocument(inputStream, null);

        // Actual test

        DataToSign dataToSign = padesCryptoService.getDataToSign(document, params);
        LegacyDataToSignHolder dataToSignHolder = new LegacyDataToSignHolder(singletonList(dataToSign), params, null);

        assertNotNull(dataToSignHolder);
        assertNotNull(dataToSignHolder.getDataToSignBase64List());
        assertNotNull(dataToSignHolder.getDigestBase64List());
        assertNotNull(dataToSignHolder.getSignatureDateTime());
        assertEquals(1, dataToSignHolder.getDigestBase64List().size());
        assertEquals(1, dataToSignHolder.getDataToSignBase64List().size());
        assertEquals(44, dataToSignHolder.getDigestBase64List().get(0).length());
        assertEquals(444, dataToSignHolder.getDataToSignBase64List().get(0).length());
    }


    @Test
    void generateSignature_defaultPdfPosition() throws Exception {

        // Preparing parameters

        LegacyPadesSignatureParameters requestParams = new LegacyPadesSignatureParameters();
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setSignatureDateTime(new Date().getTime());

        PAdESSignatureParameters params = padesCryptoService.getSignatureParameters(requestParams, null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream inputStream = classLoader.getResourceAsStream(PDF_BLANK_RESOURCE_NAME);
        assertNotNull(inputStream);
        DSSDocument document = new StreamableDssDocument(inputStream, baos);

        DataToSign dataToSign = padesCryptoService.getDataToSign(document, params);
        LegacyDataToSignHolder dataToSignHolder = new LegacyDataToSignHolder(singletonList(dataToSign), params, null);
        String signature = sign(classLoader, dataToSignHolder.getDataToSignBase64List().get(0), RSA_SHA256);

        // Actual test

        padesCryptoService.getSignature(params, document, signature);

        File signedFile = new File("build/test-results/generateSignature_defaultPdfPosition_" + new Date().getTime() + ".pdf");
        writeByteArrayToFile(signedFile, baos.toByteArray());
        assertSignatureLocation(signedFile.getAbsolutePath(), new Rectangle(275, 200, 80, 80));
    }


    @Test
    void generateSignature_pageOverflow() throws Exception {

        String yamlString =
                """
                x: 20
                y: 10
                page: 99999
                width: 100
                height: 100
                elements:
                  - type: text
                    value: "Test\\nValue\\nPlop"
                    colorCode: "#000000"
                    font: helvetica
                    fontSize: 12
                    x: 35
                    y: 20
                """;

        // Preparing parameters

        LegacyPadesSignatureParameters requestParams = new LegacyPadesSignatureParameters();
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setSignatureDateTime(new Date().getTime());
        requestParams.setPayload(Map.of(PAYLOAD_KEY_SIGNATURE_STAMP_YAML_BASE64, yamlString));
        PAdESSignatureParameters params = padesCryptoService.getSignatureParameters(requestParams, null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream inputStream = classLoader.getResourceAsStream(PDF_BLANK_RESOURCE_NAME);
        assertNotNull(inputStream);
        DSSDocument document = new StreamableDssDocument(inputStream, baos);

        DataToSign dataToSign = padesCryptoService.getDataToSign(document, params);
        LegacyDataToSignHolder dataToSignHolder = new LegacyDataToSignHolder(singletonList(dataToSign), params, null);
        String signature = sign(classLoader, dataToSignHolder.getDataToSignBase64List().get(0), RSA_SHA256);

        // Actual test

        padesCryptoService.getSignature(params, document, signature);

        File signedFile = new File("build/test-results/generateSignature_overflowPage_" + new Date().getTime() + ".pdf");
        writeByteArrayToFile(signedFile, baos.toByteArray());
        assertSignatureLocation(signedFile.getAbsolutePath(), new Rectangle(20, 10, 100, 100));
    }


    @Test
    void generateSignature_pageZero() throws Exception {

        String yamlString =
                """
                x: 20
                y: 10
                page: 0
                width: 100
                height: 100
                elements:
                  - type: text
                    value: "Test\\nValue\\nPlop"
                    colorCode: "#000000"
                    font: helvetica
                    fontSize: 12
                    x: 35
                    y: 20
                """;

        // Preparing parameters

        LegacyPadesSignatureParameters requestParams = new LegacyPadesSignatureParameters();
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setSignatureDateTime(new Date().getTime());
        requestParams.setPayload(Map.of(PAYLOAD_KEY_SIGNATURE_STAMP_YAML_BASE64, yamlString));
        PAdESSignatureParameters params = padesCryptoService.getSignatureParameters(requestParams, null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream inputStream = classLoader.getResourceAsStream(PDF_BLANK_RESOURCE_NAME);
        assertNotNull(inputStream);
        DSSDocument document = new StreamableDssDocument(inputStream, baos);

        DataToSign dataToSign = padesCryptoService.getDataToSign(document, params);
        LegacyDataToSignHolder dataToSignHolder = new LegacyDataToSignHolder(singletonList(dataToSign), params, null);
        String signature = sign(classLoader, dataToSignHolder.getDataToSignBase64List().get(0), RSA_SHA256);

        // Actual test

        padesCryptoService.getSignature(params, document, signature);

        File signedFile = new File("build/test-results/generateSignature_pageZero.pdf");
        writeByteArrayToFile(signedFile, baos.toByteArray());
        assertSignatureLocation(signedFile.getAbsolutePath(), new Rectangle(20, 10, 100, 100));
    }


    @Test
    void generateSignature_cropboxPosition() throws Exception {

        // Preparing parameters

        LegacyPadesSignatureParameters requestParams = new LegacyPadesSignatureParameters();
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setSignatureDateTime(new Date().getTime());
        requestParams.setPayload(Map.of(PAYLOAD_KEY_SIGNATURE_STAMP_YAML_BASE64, BOTTOM_LEFT_YAML_STRING));
        PAdESSignatureParameters params = padesCryptoService.getSignatureParameters(requestParams, null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream inputStream = classLoader.getResourceAsStream(PDF_CROPPED_RESOURCE_NAME);
        assertNotNull(inputStream);
        DSSDocument document = new StreamableDssDocument(inputStream, baos);

        DataToSign dataToSign = padesCryptoService.getDataToSign(document, params);
        LegacyDataToSignHolder dataToSignHolder = new LegacyDataToSignHolder(singletonList(dataToSign), params, null);
        String signature = sign(classLoader, dataToSignHolder.getDataToSignBase64List().get(0), RSA_SHA256);

        // Actual test

        padesCryptoService.getSignature(params, document, signature);

        File signedFile = new File("build/test-results/generateSignature_cropboxPosition.pdf");
        writeByteArrayToFile(signedFile, baos.toByteArray());
        assertSignatureLocation(signedFile.getAbsolutePath(), new Rectangle(74, 564, 200, 150));
    }


    @Test
    void generateSignature_cropboxPosition_rotation90() throws Exception {

        // Preparing parameters

        LegacyPadesSignatureParameters requestParams = new LegacyPadesSignatureParameters();
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setSignatureDateTime(new Date().getTime());
        requestParams.setPayload(Map.of(PAYLOAD_KEY_SIGNATURE_STAMP_YAML_BASE64, BOTTOM_LEFT_YAML_STRING));
        PAdESSignatureParameters params = padesCryptoService.getSignatureParameters(requestParams, null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream inputStream = classLoader.getResourceAsStream(PDF_CROPPED_ROTATION_90_RESOURCE_NAME);
        assertNotNull(inputStream);
        DSSDocument document = new StreamableDssDocument(inputStream, baos);

        DataToSign dataToSign = padesCryptoService.getDataToSign(document, params);
        LegacyDataToSignHolder dataToSignHolder = new LegacyDataToSignHolder(singletonList(dataToSign), params, null);
        String signature = sign(classLoader, dataToSignHolder.getDataToSignBase64List().get(0), RSA_SHA256);

        // Actual test

        padesCryptoService.getSignature(params, document, signature);

        File signedFile = new File("build/test-results/generateSignature_cropboxPosition_rotation90.pdf");
        writeByteArrayToFile(signedFile, baos.toByteArray());
        assertSignatureLocation(signedFile.getAbsolutePath(), new Rectangle(240, 74, 150, 200));
    }


    @Test
    void generateSignature_cropboxPosition_rotation180() throws Exception {

        // Preparing parameters

        LegacyPadesSignatureParameters requestParams = new LegacyPadesSignatureParameters();
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setSignatureDateTime(new Date().getTime());
        requestParams.setPayload(Map.of(PAYLOAD_KEY_SIGNATURE_STAMP_YAML_BASE64, BOTTOM_LEFT_YAML_STRING));
        PAdESSignatureParameters params = padesCryptoService.getSignatureParameters(requestParams, null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream inputStream = classLoader.getResourceAsStream(PDF_CROPPED_ROTATION_180_RESOURCE_NAME);
        assertNotNull(inputStream);
        DSSDocument document = new StreamableDssDocument(inputStream, baos);

        DataToSign dataToSign = padesCryptoService.getDataToSign(document, params);
        LegacyDataToSignHolder dataToSignHolder = new LegacyDataToSignHolder(singletonList(dataToSign), params, null);
        String signature = sign(classLoader, dataToSignHolder.getDataToSignBase64List().get(0), RSA_SHA256);

        // Actual test

        padesCryptoService.getSignature(params, document, signature);

        File signedFile = new File("build/test-results/generateSignature_cropboxPosition_rotation180.pdf");
        writeByteArrayToFile(signedFile, baos.toByteArray());
        assertSignatureLocation(signedFile.getAbsolutePath(), new Rectangle(330, 240, 200, 150));
    }


    @Test
    void generateSignature_cropboxPosition_rotation270() throws Exception {

        // Preparing parameters

        LegacyPadesSignatureParameters requestParams = new LegacyPadesSignatureParameters();
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setSignatureDateTime(new Date().getTime());
        requestParams.setPayload(Map.of(PAYLOAD_KEY_SIGNATURE_STAMP_YAML_BASE64, BOTTOM_LEFT_YAML_STRING));
        PAdESSignatureParameters params = padesCryptoService.getSignatureParameters(requestParams, null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream inputStream = classLoader.getResourceAsStream(PDF_CROPPED_ROTATION_270_RESOURCE_NAME);
        assertNotNull(inputStream);
        DSSDocument document = new StreamableDssDocument(inputStream, baos);

        DataToSign dataToSign = padesCryptoService.getDataToSign(document, params);
        LegacyDataToSignHolder dataToSignHolder = new LegacyDataToSignHolder(singletonList(dataToSign), params, null);
        String signature = sign(classLoader, dataToSignHolder.getDataToSignBase64List().get(0), RSA_SHA256);

        // Actual test

        padesCryptoService.getSignature(params, document, signature);

        File signedFile = new File("build/test-results/generateSignature_cropboxPosition_rotation270.pdf");
        writeByteArrayToFile(signedFile, baos.toByteArray());
        assertSignatureLocation(signedFile.getAbsolutePath(), new Rectangle(564, 330, 150, 200));
    }


    @Test
    void generateSignature_rotation0_customPdfPosition() throws Exception {

        // Preparing parameters

        LegacyPadesSignatureParameters requestParams = new LegacyPadesSignatureParameters();
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setSignatureDateTime(new Date().getTime());
        requestParams.setPayload(Map.of(PAYLOAD_KEY_SIGNATURE_STAMP_YAML_BASE64, BOTTOM_LEFT_YAML_STRING));
        PAdESSignatureParameters params = padesCryptoService.getSignatureParameters(requestParams, null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream inputStream = classLoader.getResourceAsStream(PDF_RESOURCE_NAME);
        assertNotNull(inputStream);
        DSSDocument document = new StreamableDssDocument(inputStream, baos);

        DataToSign dataToSign = padesCryptoService.getDataToSign(document, params);
        LegacyDataToSignHolder dataToSignHolder = new LegacyDataToSignHolder(singletonList(dataToSign), params, null);
        String signature = sign(classLoader, dataToSignHolder.getDataToSignBase64List().get(0), RSA_SHA256);

        // Actual test

        padesCryptoService.getSignature(params, document, signature);

        File signedFile = new File("build/test-results/generateSignature_rotation0_customPdfPosition.pdf");
        writeByteArrayToFile(signedFile, baos.toByteArray());
        assertSignatureLocation(signedFile.getAbsolutePath(), new Rectangle(20, 10, 200, 150));
    }


    @Test
    void generateSignature_rotation90_customPdfPosition() throws Exception {

        // Preparing parameters

        LegacyPadesSignatureParameters requestParams = new LegacyPadesSignatureParameters();
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setSignatureDateTime(new Date().getTime());
        requestParams.setPayload(Map.of(PAYLOAD_KEY_SIGNATURE_STAMP_YAML_BASE64, BOTTOM_LEFT_YAML_STRING));
        PAdESSignatureParameters params = padesCryptoService.getSignatureParameters(requestParams, null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream inputStream = classLoader.getResourceAsStream(PDF_ROTATION_90_RESOURCE_NAME);
        assertNotNull(inputStream);
        DSSDocument document = new StreamableDssDocument(inputStream, baos);

        DataToSign dataToSign = padesCryptoService.getDataToSign(document, params);
        LegacyDataToSignHolder dataToSignHolder = new LegacyDataToSignHolder(singletonList(dataToSign), params, null);
        String signature = sign(classLoader, dataToSignHolder.getDataToSignBase64List().get(0), RSA_SHA256);

        // Actual test

        padesCryptoService.getSignature(params, document, signature);

        File signedFile = new File("build/test-results/generateSignature_rotation90_customPdfPosition.pdf");
        writeByteArrayToFile(signedFile, baos.toByteArray());
        assertSignatureLocation(signedFile.getAbsolutePath(), new Rectangle(682, 20, 150, 200));
    }


    @Test
    void generateSignature_rotation180_customPdfPosition() throws Exception {

        // Preparing parameters

        LegacyPadesSignatureParameters requestParams = new LegacyPadesSignatureParameters();
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setSignatureDateTime(new Date().getTime());
        requestParams.setPayload(Map.of(PAYLOAD_KEY_SIGNATURE_STAMP_YAML_BASE64, BOTTOM_LEFT_YAML_STRING));
        PAdESSignatureParameters params = padesCryptoService.getSignatureParameters(requestParams, null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream inputStream = classLoader.getResourceAsStream(PDF_ROTATION_180_RESOURCE_NAME);
        assertNotNull(inputStream);
        DSSDocument document = new StreamableDssDocument(inputStream, baos);

        DataToSign dataToSign = padesCryptoService.getDataToSign(document, params);
        LegacyDataToSignHolder dataToSignHolder = new LegacyDataToSignHolder(singletonList(dataToSign), params, null);
        String signature = sign(classLoader, dataToSignHolder.getDataToSignBase64List().get(0), RSA_SHA256);

        // Actual test

        padesCryptoService.getSignature(params, document, signature);

        File signedFile = new File("build/test-results/generateSignature_rotation180_customPdfPosition.pdf");
        writeByteArrayToFile(signedFile, baos.toByteArray());
        assertSignatureLocation(signedFile.getAbsolutePath(), new Rectangle(375, 682, 200, 150));
    }


    @Test
    void generateSignature_rotation270_customPdfPosition() throws Exception {

        // Preparing parameters

        LegacyPadesSignatureParameters requestParams = new LegacyPadesSignatureParameters();
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setSignatureDateTime(new Date().getTime());
        requestParams.setPayload(Map.of(PAYLOAD_KEY_SIGNATURE_STAMP_YAML_BASE64, BOTTOM_LEFT_YAML_STRING));
        PAdESSignatureParameters params = padesCryptoService.getSignatureParameters(requestParams, null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream inputStream = classLoader.getResourceAsStream(PDF_ROTATION_270_RESOURCE_NAME);
        assertNotNull(inputStream);
        DSSDocument document = new StreamableDssDocument(inputStream, baos);

        DataToSign dataToSign = padesCryptoService.getDataToSign(document, params);
        LegacyDataToSignHolder dataToSignHolder = new LegacyDataToSignHolder(singletonList(dataToSign), params, null);
        String signature = sign(classLoader, dataToSignHolder.getDataToSignBase64List().get(0), RSA_SHA256);

        // Actual test

        padesCryptoService.getSignature(params, document, signature);

        File signedFile = new File("build/test-results/generateSignature_rotation270_customPdfPosition.pdf");
        writeByteArrayToFile(signedFile, baos.toByteArray());
        assertSignatureLocation(signedFile.getAbsolutePath(), new Rectangle(10, 375, 150, 200));
    }


    @Test
    void generateSignature_removeNeedAppearances() throws Exception {

        // Preparing parameters

        LegacyPadesSignatureParameters requestParams = new LegacyPadesSignatureParameters();
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setSignatureDateTime(new Date().getTime());
        requestParams.setPayload(new HashMap<>() {{
            put(PAYLOAD_KEY_SIGNATURE_STAMP_YAML_BASE64, BOTTOM_LEFT_YAML_STRING);
        }});
        PAdESSignatureParameters params = padesCryptoService.getSignatureParameters(requestParams, null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream inputStream = classLoader.getResourceAsStream(PDF_NEED_APPEARANCES_RESOURCE_NAME);
        assertNotNull(inputStream);
        DSSDocument document = new StreamableDssDocument(inputStream, baos);

        DataToSign dataToSign = padesCryptoService.getDataToSign(document, params);
        LegacyDataToSignHolder dataToSignHolder = new LegacyDataToSignHolder(singletonList(dataToSign), params, null);
        String signature = sign(classLoader, dataToSignHolder.getDataToSignBase64List().get(0), RSA_SHA256);

        // Actual test

        padesCryptoService.getSignature(params, document, signature);

        String signatureResult = "build/test-results/generateSignature_removeNeedAppearances.pdf";
        File signedFile = new File(signatureResult);
        writeByteArrayToFile(signedFile, baos.toByteArray());

        try (PDDocument signedDocument = PDDocument.load(signedFile)) {
            assertFalse(signedDocument.getDocumentCatalog().getAcroForm().getNeedAppearances());
        }
    }


    /**
     * Some rotated PDF may require some division on positioning.
     * Having a zero-sized stamp (it is allowed) may produce a division-by-zero error.
     *
     * @throws Exception catch all
     */
    @Test
    void generateSignature_zeroSizedSignature() throws Exception {

        String yamlString =
                """
                x: 100
                y: 575
                page: 0
                width: 0
                height: 0
                elements: []
                """;

        // Preparing parameters

        PadesParameters requestParams = new PadesParameters();
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setSignatureDateTime(new Date().getTime());
        requestParams.setStamp(new ObjectMapper(new YAMLFactory()).readValue(yamlString, PdfSignatureStamp.class));
        PAdESSignatureParameters params = padesCryptoService.getSignatureParameters(requestParams, null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream inputStream = classLoader.getResourceAsStream("pdf/rotation_90.pdf");
        assertNotNull(inputStream);
        DSSDocument document = new StreamableDssDocument(inputStream, baos);

        DataToSign dataToSign = padesCryptoService.getDataToSign(document, params);
        DataToSignHolder dataToSignHolder = new DataToSignHolder(singletonList(dataToSign), null);
        String toSignBase64 = dataToSignHolder.getDataToSignList().stream().findFirst().map(DataToSign::getDataToSignBase64).orElse(null);
        assertNotNull(toSignBase64);
        String signature = sign(classLoader, toSignBase64, RSA_SHA256);

        // Nothing to test here,
        // this is a convenient method to generate and work on the default IPv5 signature model

        padesCryptoService.getSignature(params, document, signature);
        File signedFile = new File("build/test-results/generateSignature_zeroSizedSignature_" + new Date().getTime() + ".pdf");
        writeByteArrayToFile(signedFile, baos.toByteArray());
    }


    @Test
    void generateSignature_defaultIparapheurSignature() throws Exception {

        String yamlString =
                """
                x: 100
                y: 575
                page: 0

                width: 200
                height: 70

                elements:

                  - type: IMAGE
                    x: 0
                    y: 0
                    width: 200
                    height: 70
                    value: "iVBORw0KGgoAAAANSUhEUgAAAMgAAABGCAYAAACJ4ts2AAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TRZGKQ4uIOGSoThZFRRy1CkWoEGqFVh1MLv2CJg1J\\
                      iouj4Fpw8GOx6uDirKuDqyAIfoA4OjkpukiJ/0sKLWI8OO7Hu3uPu3eAUC8zzeoYBzTdNlOJuJjJropdrxDQjzBEjMnMMuYkKQnf8XWPAF/vYjzL/9yfo1fNWQwIiMSzzDBt4g\\
                      3i6U3b4LxPHGFFWSU+Jx416YLEj1xXPH7jXHBZ4JkRM52aJ44Qi4U2VtqYFU2NeIo4qmo65QsZj1XOW5y1cpU178lfGMrpK8tcpzmEBBaxBIk6UlBFCWXYiNGqk2IhRftxH/+g\\
                      65fIpZCrBEaOBVSgQXb94H/wu1srPznhJYXiQOeL43wMA127QKPmON/HjtM4AYLPwJXe8lfqwMwn6bWWFj0C+raBi+uWpuwBlzvAwJMhm7IrBWkK+TzwfkbflAXCt0DPmtdbcx\\
                      +nD0CaukreAAeHwEiBstd93t3d3tu/Z5r9/QCf33K5uvHXIQAAAAZiS0dEAP8AAAAAMyd88wAAAAlwSFlzAAAuIwAALiMBeKU/dgAAALpJREFUeNrt01ENgDAQBcErQUBV4F8P\\
                      Jt45KN8kRQEzEjbZkWQVsHVWVc05byngrbuvQwb4ZhAwCBgEDAIGAYOAQcAgYBAwCGAQMAgYBAwCBgGDgEHAIGAQMAhgEDAIGAQMAgYBg4BBwCBgEMAgYBAwCBgEDAIGAYOAQc\\
                      AgYBDAIGAQMAgYBAwCBgGDgEHAIIBBwCBgEDAIGAQMAgYBg4BBwCCAQcAgYBAwCBgEDAIGAYPAf4wkSwbYewCMSwwl/2+hogAAAABJRU5ErkJggg=="

                  - type: IMAGE
                    x: 20
                    y: 20
                    width: 38
                    height: 30
                    value: "iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV/TakUqHewg4pChOlkQFemoVShC\\
                      hVArtOpgcv2EJg1Jiouj4Fpw8GOx6uDirKuDqyAIfoC4uTkpukiJ/0sKLWI8OO7Hu3uPu3eA0Kwy1QxMAKpmGelkQszmVsXgK/zoRQBxhGVm6nOSlILn+L\\
                      qHj693MZ7lfe7PMZAvmAzwicSzTDcs4g3imU1L57xPHGFlOU98Tjxu0AWJH7muuPzGueSwwDMjRiY9TxwhFktdrHQxKxsq8TRxNK9qlC9kXc5z3uKsVuus\\
                      fU/+wlBBW1nmOs0RJLGIJUgQoaCOCqqwEKNVI8VEmvYTHv5hxy+RSyFXBYwcC6hBhez4wf/gd7dmcWrSTQolgJ4X2/4YBYK7QKth29/Htt06AfzPwJXW8d\\
                      eaQPyT9EZHix4B4W3g4rqjKXvA5Q4w9KTLhuxIfppCsQi8n9E35YDBW6B/ze2tvY/TByBDXaVugINDYKxE2ese7+7r7u3fM+3+fgA+ZnKStEjndwAAAAZi\\
                      S0dEAP8A/wD/oL2nkwAAAAlwSFlzAAALEgAACxIB0t1+/AAAAg5JREFUeNrtmqFv20AUh3+rYtABD6QgxKSgBSHDkU4jZv5zp2NmlgpKRlbQAZOQgBrMoA\\
                      UmA71JWWXfufWuaZzvk0JyyVn6dO/uveeTAAAAAAAAAAAAAGAUn2JNbGyZScr2vtpJ2lZF3iEwLG8jadkz1Eq6mZPEswjyLgfkSVIqaT2nFXgWYc5VYPwC\\
                      gdM4R+A0nhDoZxcYf0Cgh6rIa0nNwHAr6Y405m15YCOpnlseCDDPSmTpGW5DYWxs+dmlO39zxuWLZDxxp/mj+zSSmqrIH99b4CLSvBvP2E3fIeOkrZ2sZG\\
                      Q+ee5+n7k5dm6fbeacB/qErEbK81VBG2PL9SkK/J9cGlt+M7ZMEPh2UklfETixseG6Qwh0JeKvvU89sq6+ihnKiyMS2He63hlbXku68vwvcad0TQj31973\\
                      bkX6yAjhwOqU1AUOFAR6VmEnaTuhOuIUVrgPmSBwGl8Q6A/j5hDPnY1AY8sUgdMI7XEPCDzAHndKAleH2CNnIdDYcqXh6yTS89vAk1+Bmetav5SXKdy22t\\
                      JMeK5nM2PLbm9FpSMT5B0C/z1tX1OW1TFfNs29EnlSuFODwAE6Sbexb0IsZiqvlfSjKvI29oMWRyYlHRGyW73jHZwoAqsi/x5h2p97Ei96wvX3IRoKRxXC\\
                      blU1Gr4+Ryl3bCAQgQhEIAIBgQAAAAAAAAAAAADwwfkDivGIjEUIa/EAAAAASUVORK5CYII="
                  
                  # Convenient vertical/horizontal guides to narrow the upcoming text element
                  #
                  #- type: IMAGE
                  #  x: 70
                  #  y: 14
                  #  width: 122
                  #  height: 42
                  #  value: "iVBORw0KGgoAAAANSUhEUgAAAAoAAADICAIAAABecOdVAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TxSpVETtIcchQnSyIijhqFYpQ\\
                  #    odQKrTqYXPoFTRqSFBdHwbXg4Mdi1cHFWVcHV0EQ/ABxdHJSdJES/5cUWsR4cNyPd/ced+8AoV5mqtkxDqiaZaTiMTGTXRW7XtGNAPowgLDETH0umUzAc3\\
                  #    zdw8fXuyjP8j735+hVciYDfCLxLNMNi3iDeHrT0jnvE4dYUVKIz4nHDLog8SPXZZffOBccFnhmyEin5olDxGKhjeU2ZkVDJZ4ijiiqRvlCxmWF8xZntVxl\\
                  #    zXvyFwZz2soy12kOI45FLCEJETKqKKEMC1FaNVJMpGg/5uEPO/4kuWRylcDIsYAKVEiOH/wPfndr5icn3KRgDOh8se2PEaBrF2jUbPv72LYbJ4D/GbjSWv\\
                  #    5KHZj5JL3W0iJHQP82cHHd0uQ94HIHGHrSJUNyJD9NIZ8H3s/om7LA4C3Qs+b21tzH6QOQpq4SN8DBITBaoOx1j3cH2nv790yzvx8oIXKJVjfhLwAAAAlw\\
                  #    SFlzAAAuIwAALiMBeKU/dgAAADJJREFUWMPtyTENADAIADDGhX9TuFpQwAQs7dtzu2KX8aS11lprrbXWWmuttdZa6596AKHOA10nhJXZAAAAAElFTkSuQm\\
                  #    CC"
                  #
                  #- type: IMAGE
                  #  x: 70
                  #  y: 14
                  #  width: 122
                  #  height: 42
                  #  value: "iVBORw0KGgoAAAANSUhEUgAAAMgAAAAKCAIAAAB+GoZ2AAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TxSpVETtIcchQnSyIijhqFYpQ\\
                  #    odQKrTqYXPoFTRqSFBdHwbXg4Mdi1cHFWVcHV0EQ/ABxdHJSdJES/5cUWsR4cNyPd/ced+8AoV5mqtkxDqiaZaTiMTGTXRW7XtGNAPowgLDETH0umUzAc3\\
                  #    zdw8fXuyjP8j735+hVciYDfCLxLNMNi3iDeHrT0jnvE4dYUVKIz4nHDLog8SPXZZffOBccFnhmyEin5olDxGKhjeU2ZkVDJZ4ijiiqRvlCxmWF8xZntVxl\\
                  #    zXvyFwZz2soy12kOI45FLCEJETKqKKEMC1FaNVJMpGg/5uEPO/4kuWRylcDIsYAKVEiOH/wPfndr5icn3KRgDOh8se2PEaBrF2jUbPv72LYbJ4D/GbjSWv\\
                  #    5KHZj5JL3W0iJHQP82cHHd0uQ94HIHGHrSJUNyJD9NIZ8H3s/om7LA4C3Qs+b21tzH6QOQpq4SN8DBITBaoOx1j3cH2nv790yzvx8oIXKJVjfhLwAAAAlw\\
                  #    SFlzAAAuIwAALiMBeKU/dgAAADFJREFUWMPt0jERADAIALHSCf+mcMVhgo1Ewt9HVz7Y9iXAWBgLY4GxMBbGAmNhLK4aSNEB4VuwLbEAAAAASUVORK5CYI\\
                  #    I="
                  
                  - type: TEXT
                    value: |
                      ${i_Parapheur_internal_user_name}
                      ${i_Parapheur_internal_desk_name}
                      ${i_Parapheur_internal_signature_date}
                      ${i_Parapheur_internal_delegation_from}
                    colorCode: "#000000"
                    font: HELVETICA_BOLD
                    fontSize: 0
                    x: 70
                    y: 14
                    width: 122
                    height: 42

                  - type: IMAGE
                    x: 62
                    y: 12
                    width: 2
                    height: 48
                    value: "iVBORw0KGgoAAAANSUhEUgAAAAgAAADICAYAAADV56A/AAAABmJLR0QAowAAAADOxF45AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH5QEWCyomc3ynVgAAADZJ\\
                      REFUWMPtyqEBACAIADDwL8+m+Zt8gMm25eWuc2Ow4kEQBEEQBEEQBEEQBEEQBEEQBEH4ERo3kAQ9V9AElAAAAABJRU5ErkJggg=="

                """;

        // Preparing parameters

        PadesParameters requestParams = new PadesParameters();
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setSignatureDateTime(new Date().getTime());
        requestParams.setStamp(new ObjectMapper(new YAMLFactory()).readValue(yamlString, PdfSignatureStamp.class));
        PAdESSignatureParameters params = padesCryptoService.getSignatureParameters(requestParams, null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream inputStream = classLoader.getResourceAsStream(PDF_BLANK_RESOURCE_NAME);
        assertNotNull(inputStream);
        DSSDocument document = new StreamableDssDocument(inputStream, baos);

        DataToSign dataToSign = padesCryptoService.getDataToSign(document, params);
        DataToSignHolder dataToSignHolder = new DataToSignHolder(singletonList(dataToSign), null);
        String toSignBase64 = dataToSignHolder.getDataToSignList().stream().findFirst().map(DataToSign::getDataToSignBase64).orElse(null);
        assertNotNull(toSignBase64);
        String signature = sign(classLoader, toSignBase64, RSA_SHA256);

        // Nothing to test here,
        // this is a convenient method to generate and work on the default IPv5 signature model

        padesCryptoService.getSignature(params, document, signature);
        File signedFile = new File("build/test-results/generateSignature_defaultIparapheurSignature_" + new Date().getTime() + ".pdf");
        writeByteArrayToFile(signedFile, baos.toByteArray());
    }


    @Test
    void generateSignature_overflowingSignature() throws Exception {

        String yamlString =
                """
                x: 100
                y: 575
                page: 0

                width: 70
                height: 70

                elements:

                  - type: TEXT
                    x: 0
                    y: 0
                    value: |
                      testA testB
                      test1 test2 test3 test4 test5
                      testC testD
                    colorCode: "#000000"
                    font: HELVETICA
                    fontSize: 8
                """;

        // Preparing parameters

        PadesParameters requestParams = new PadesParameters();
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setSignatureDateTime(new Date().getTime());
        requestParams.setStamp(new ObjectMapper(new YAMLFactory()).readValue(yamlString, PdfSignatureStamp.class));
        PAdESSignatureParameters params = padesCryptoService.getSignatureParameters(requestParams, null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream inputStream = classLoader.getResourceAsStream(PDF_BLANK_RESOURCE_NAME);
        assertNotNull(inputStream);
        DSSDocument document = new StreamableDssDocument(inputStream, baos);

        DataToSign dataToSign = padesCryptoService.getDataToSign(document, params);
        DataToSignHolder dataToSignHolder = new DataToSignHolder(singletonList(dataToSign), null);
        String toSignBase64 = dataToSignHolder.getDataToSignList().stream().findFirst().map(DataToSign::getDataToSignBase64).orElse(null);
        assertNotNull(toSignBase64);
        String signature = sign(classLoader, toSignBase64, RSA_SHA256);

        // Nothing to test here,
        // this is a convenient method to generate and work on the default IPv5 signature model

        padesCryptoService.getSignature(params, document, signature);
        File signedFile = new File("build/test-results/generateSignature_generateSignature_overflowingSignature_" + new Date().getTime() + ".pdf");
        writeByteArrayToFile(signedFile, baos.toByteArray());
    }


}
