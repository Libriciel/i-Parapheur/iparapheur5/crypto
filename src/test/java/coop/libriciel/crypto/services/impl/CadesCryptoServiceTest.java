/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.services.impl;

import coop.libriciel.crypto.models.DataToSign;
import coop.libriciel.crypto.models.request.CadesParameters;
import coop.libriciel.crypto.models.request.legacy.LegacyDataToSignHolder;
import coop.libriciel.crypto.models.request.legacy.LegacySignatureParameters;
import eu.europa.esig.dss.cades.CAdESSignatureParameters;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.MethodOrderer.MethodName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;
import java.util.List;

import static coop.libriciel.crypto.TestUtils.*;
import static coop.libriciel.crypto.models.DigestAlgorithm.SHA256;
import static coop.libriciel.crypto.services.CryptoService.generateDigestDocument;
import static eu.europa.esig.dss.enumerations.SignatureAlgorithm.RSA_SHA1;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;


@Log4j2
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestMethodOrder(MethodName.class)
public class CadesCryptoServiceTest {

    private static DataToSign documentHolder;
    private static LegacyDataToSignHolder dataToSignHolder;
    private static LegacySignatureParameters signatureParameters;
    @Autowired private CadesCryptoService cadesCryptoService;
    private final ClassLoader classLoader = getClass().getClassLoader();


    @Test
    void stage01_prepare() throws Exception {
        documentHolder = new DataToSign();
        documentHolder.setDigestBase64(PDF_HASH_SHA1_B64);

        CadesParameters parameters = new CadesParameters();
        parameters.setDigestAlgorithm(SHA256);
        parameters.setPayload(emptyMap());
        parameters.setPublicCertificateBase64(getCertBase64(classLoader));
        parameters.setDataToSignList(singletonList(documentHolder));

        signatureParameters = new LegacySignatureParameters();
        signatureParameters.setDigestAlgorithm(SHA256);
        signatureParameters.setPayload(emptyMap());
        signatureParameters.setPublicCertificateBase64(getCertBase64(classLoader));
        signatureParameters.setDataToSignList(singletonList(documentHolder));
        signatureParameters.setSignatureDateTime(new Date().getTime());
    }


    @Test
    void stage02_generateDataToSign() {

        CAdESSignatureParameters params = cadesCryptoService.getSignatureParameters(signatureParameters);
        List<DataToSign> dataToSignList = signatureParameters.getDataToSignList()
                .stream()
                .map(d -> generateDigestDocument(d, signatureParameters.getDigestAlgorithm().getDssValue()))
                .map(d -> cadesCryptoService.getDataToSign(d, params))
                .toList();

        dataToSignHolder = new LegacyDataToSignHolder(dataToSignList, params, null);

        assertNotNull(dataToSignHolder);
        assertNotNull(dataToSignHolder.getDataToSignBase64List());
        assertNotNull(dataToSignHolder.getSignatureDateTime());
    }


    @Test
    void stage03_generateSignature() throws Exception {

        String signature = sign(classLoader, dataToSignHolder.getDataToSignBase64List().get(0), RSA_SHA1);
        documentHolder.setSignatureValue(signature);
        signatureParameters.setSignatureDateTime(dataToSignHolder.getSignatureDateTime());
        signatureParameters.setDataToSignList(singletonList(documentHolder));

        CAdESSignatureParameters params = cadesCryptoService.getSignatureParameters(signatureParameters);
        List<String> signatureList = signatureParameters.getDataToSignList()
                .stream()
                .map(d -> Pair.of(
                        generateDigestDocument(d, signatureParameters.getDigestAlgorithm().getDssValue()),
                        d.getSignatureValue())
                )
                .map(p -> cadesCryptoService.getSignature(params, p.getLeft(), p.getRight()))
                .toList();

        assertNotNull(signatureList);
        assertEquals(1, signatureList.size());

        String finalSignatureResult = signatureList.get(0);
        assertEquals(2630, finalSignatureResult.length());
        assertTrue(finalSignatureResult.startsWith("-----BEGIN PKCS7-----\n"));
        assertTrue(finalSignatureResult.endsWith("\n-----END PKCS7-----\n"));
    }


    @Test
    void stage04_generateSignature_error() {
        signatureParameters.setPublicCertificateBase64("not_a_base64_public_key");
        assertThrows(Exception.class, () -> cadesCryptoService.getSignatureParameters(signatureParameters));
    }

}
