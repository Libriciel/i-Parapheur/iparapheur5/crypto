/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.models.stamp;

import coop.libriciel.crypto.models.stamp.PdfSignatureStampElement.SignatureStampFont;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertNotNull;


class PdfSignatureStampElementTest {


    @Test
    void assertFontExistence() throws IOException {
        try (PDDocument document = new PDDocument()) {
            for (SignatureStampFont font : SignatureStampFont.values()) {
                assertNotNull(font.getPdFontLoader().apply(document));
            }
        }
    }

}
