/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.models.request;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class SignatureParametersTest {


    @Test
    void deserialize() throws Exception {

        String serialized =
                """
                {
                  "payload":{
                    "additionalProp1":"string1",
                    "additionalProp2":"string2"
                  },
                  "publicCertificateBase64":"PUBLIC_CERT",
                  "dataToSignList":[
                    {
                      "id":"01",
                      "dataToSignBase64":"DATA_TO_SIGN",
                      "signatureBase64":"SIGNATURE"
                    }
                  ],
                  "signatureDateTime":1620291916268
                }
                """;

        CadesParameters deserializedParams = new ObjectMapper().readValue(serialized, CadesParameters.class);
        assertNotNull(deserializedParams.getPayload());
        assertEquals(2, deserializedParams.getPayload().size());
        assertEquals("PUBLIC_CERT", deserializedParams.getPublicCertificateBase64());
        assertNotNull(deserializedParams.getDataToSignList());
        assertEquals(1, deserializedParams.getDataToSignList().size());
        assertEquals("01", deserializedParams.getDataToSignList().get(0).getId());
        assertEquals("SIGNATURE", deserializedParams.getDataToSignList().get(0).getSignatureValue());
        assertEquals("DATA_TO_SIGN", deserializedParams.getDataToSignList().get(0).getDataToSignBase64());
    }


}
