/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.controller;

import coop.libriciel.crypto.services.impl.PadesCryptoService;
import org.junit.jupiter.api.Test;

import java.util.Base64;
import java.util.Date;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


public class PadesCryptoControllerTest {

    private static final String DATA_TO_SIGN_EXAMPLE = "MYIBgzAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0yMTA0MjgyMzA5MTNaMC0GCSqGSIb3DQ" +
            "EJNDEgMB4wDQYJYIZIAWUDBAIBBQChDQYJKoZIhvcNAQELBQAwLwYJKoZIhvcNAQkEMSIEIMkSKEoSnk82B4tKBKUyRb4kbQ/17StMHRIZ3d1QIhKWMIHoBgsqhkiG9w0BCRACLzG" +
            "B2DCB1TCB0jCBzwQgVy69/Ggaz3y9Tm8Z+9yEOVrsSYAMASjtSaal6065ZNQwgaowgaGkgZ4wgZsxKDAmBgkqhkiG9w0BCQEWGWlwYXJhcGhldXJAbGlicmljaWVsLmNvb3AxCzAJ" +
            "BgNVBAYTAkZSMRIwEAYDVQQIDAlPY2NpdGFuaWUxFDASBgNVBAcMC01vbnRwZWxsaWVyMRIwEAYDVQQKDAlMaWJyaWNpZWwxDTALBgNVBAsMBFNDT1AxFTATBgNVBAMMDENyeXB0b" +
            "yB0ZXN0cwIEXwXHGA==";
    private static final Date DATA_TO_SIGN_EXAMPLE_INNER_DATE = new Date(1619651353000L);
    private static final String DATA_TO_SIGN_EXAMPLE_INNER_HASH_BASE64 = "yRIoShKeTzYHi0oEpTJFviRtD/XtK0wdEhnd3VAiEpY=";


    @Test
    void getSignatureDate() {
        assertEquals(DATA_TO_SIGN_EXAMPLE_INNER_DATE, PadesCryptoService.getSignatureDate(DATA_TO_SIGN_EXAMPLE));
        assertNull(PadesCryptoService.getSignatureDate(Base64.getEncoder().encodeToString("nope".getBytes(UTF_8))));
    }


    @Test
    void getDigestBase64() {
        assertEquals(DATA_TO_SIGN_EXAMPLE_INNER_HASH_BASE64, PadesCryptoService.getDigestBase64(DATA_TO_SIGN_EXAMPLE));
        assertNull(PadesCryptoService.getDigestBase64(Base64.getEncoder().encodeToString("nope".getBytes(UTF_8))));
    }


}
