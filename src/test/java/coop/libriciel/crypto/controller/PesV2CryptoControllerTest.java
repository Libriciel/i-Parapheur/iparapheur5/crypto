/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.crypto.controller;

import coop.libriciel.crypto.controller.legacy.PesV2CryptoController;
import coop.libriciel.crypto.models.DataToSign;
import coop.libriciel.crypto.models.request.legacy.LegacyDataToSignHolder;
import coop.libriciel.crypto.models.request.legacy.LegacySignatureParameters;
import coop.libriciel.crypto.models.request.legacy.SignatureHolder;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.util.encoders.Base64;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.InputStream;

import static coop.libriciel.crypto.TestUtils.XML_HASH_SHA256_B64;
import static coop.libriciel.crypto.TestUtils.getCertBase64;
import static coop.libriciel.crypto.models.DigestAlgorithm.SHA256;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.MediaType.TEXT_XML_VALUE;


@Deprecated
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class PesV2CryptoControllerTest {


    @Autowired private PesV2CryptoController pesV2Controller;
    private final ClassLoader classLoader = getClass().getClassLoader();


    @Test
    void generateDataToSignV1() throws Exception {

        LegacySignatureParameters requestParameters = new LegacySignatureParameters();
        requestParameters.setSignatureDateTime(1750013156000L);
        requestParameters.setDigestAlgorithm(SHA256);
        requestParameters.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParameters.setSignatureFormat("xades-env-1.2.2-sha256");
        requestParameters.setDataToSignList(singletonList(new DataToSign("01", XML_HASH_SHA256_B64, null, null)));

        LegacyDataToSignHolder result = pesV2Controller.getDataToSignV1(requestParameters);

        assertNotNull(result);
        assertNotNull(result.getDataToSignBase64List());
        assertEquals(1, result.getDataToSignBase64List().size());

        assertNotNull(result.getDataToSignBase64List().get(0));
        assertEquals(
                """
                <ds:SignedInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#"><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#">\
                </ds:CanonicalizationMethod><ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"></ds:SignatureMethod><ds:Refer\
                ence URI="#01"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"></ds:Transform><ds:Transform A\
                lgorithm="http://www.w3.org/2001/10/xml-exc-c14n#"></ds:Transform></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmle\
                nc#sha256"></ds:DigestMethod><ds:DigestValue>(digest)</ds:DigestValue></ds:Reference><ds:Reference Type="http://uri.etsi.org/01903/v1.2.2#Si\
                gnedProperties" URI="#01_SIG_1_SP"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"></ds:Transform></ds:Tran\
                sforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"></ds:DigestMethod><ds:DigestValue>(digest)</ds:DigestValue></ds:\
                Reference></ds:SignedInfo>\
                """,
                new String(Base64.decode(result.getDataToSignBase64List().get(0)))
                        .replaceAll("(?<=<ds:DigestValue>).*?(?=</ds:DigestValue>)", "(digest)"));
    }


    @Test
    void generateDataToSign() throws Exception {

        LegacySignatureParameters requestParameters = new LegacySignatureParameters();
        requestParameters.setSignatureDateTime(1750013156000L);
        requestParameters.setDigestAlgorithm(SHA256);
        requestParameters.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParameters.setSignatureFormat("xades-env-1.2.2-sha256");
        requestParameters.setDataToSignList(singletonList(new DataToSign("01", XML_HASH_SHA256_B64, null, null)));

        InputStream fileInputStream = classLoader.getResourceAsStream("pes_multi.xml");
        assertNotNull(fileInputStream);

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "file.xml",
                TEXT_XML_VALUE,
                IOUtils.toByteArray(fileInputStream)
        );

        LegacyDataToSignHolder result = pesV2Controller.getDataToSignV1(requestParameters);

        System.out.println(result);
        assertNotNull(result);
        assertNotNull(result.getSignatureDateTime());
        assertEquals(1, result.getDataToSignBase64List().size());

        assertNotNull(result.getDataToSignBase64List().get(0));
        assertEquals(
                """
                <ds:SignedInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#"><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#">\
                </ds:CanonicalizationMethod><ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"></ds:SignatureMethod><ds:Refer\
                ence URI="#01"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"></ds:Transform><ds:Transform A\
                lgorithm="http://www.w3.org/2001/10/xml-exc-c14n#"></ds:Transform></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmle\
                nc#sha256"></ds:DigestMethod><ds:DigestValue>(digest)</ds:DigestValue></ds:Reference><ds:Reference Type="http://uri.etsi.org/01903/v1.2.2#Si\
                gnedProperties" URI="#01_SIG_1_SP"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"></ds:Transform></ds:Tran\
                sforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"></ds:DigestMethod><ds:DigestValue>(digest)</ds:DigestValue></ds:\
                Reference></ds:SignedInfo>\
                """.trim(),
                new String(Base64.decode(result.getDataToSignBase64List().get(0)))
                        .replaceAll("(?<=<ds:DigestValue>).*?(?=</ds:DigestValue>)", "(digest)"));
    }


    @Test
    void generateSignatureV1() throws Exception {

        String signature =
                """
                CYFofhQGy5zdmLApBzh5ET4LWVtDIarW6hSBH1a2fkHv6DrGYrjgq4m6LMeIGhZKapPqxORyx07SmyXg9SFftGbamujwMa65sTWGaN7Tzx28PmghJ0TksNXwOQ02ldw3BsBQTGzA/cbH\
                6iRcLT4QdXlTRY8PKQf0rEitiWLtoO6/rEDHLIKLGWp5nv6xKvQQzQU5VH/9RKFsxiPaxAdSe2Kdid/luW8wb0gYvCiPB+/JYYbE8WP0MUN3MyKw+ceSwmbwtBL6cXZVGIMWHlWFISK2\
                kHHT8yjLY4AaLdnBoporOWqk3ORQQu36HI2HpfuId1qs7eMiMH1kem3W/H9v8Q==\
                """;

        LegacySignatureParameters requestParameters = new LegacySignatureParameters();
        requestParameters.setSignatureDateTime(1750013156000L);
        requestParameters.setDigestAlgorithm(SHA256);
        requestParameters.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParameters.setSignatureFormat("xades-env-1.2.2-sha256");
        requestParameters.setDataToSignList(singletonList(new DataToSign("01", XML_HASH_SHA256_B64, null, signature)));

        SignatureHolder signatureHolder = pesV2Controller.getSignatureV1(requestParameters).getBody();

        assertNotNull(signatureHolder);
        assertNotNull(signatureHolder.getSignatureResultBase64List());
        assertEquals(1, signatureHolder.getSignatureResultBase64List().size());
        assertEquals(
                """
                <DocumentDetachedExternalSignature><ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#" Id="01_SIG_1"><ds:SignedInfo><ds:Canonicaliz\
                ationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"></ds:CanonicalizationMethod><ds:SignatureMethod Algorithm="http://www.w3.or\
                g/2001/04/xmldsig-more#rsa-sha256"></ds:SignatureMethod><ds:Reference URI="#01"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2\
                000/09/xmldsig#enveloped-signature"></ds:Transform><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"></ds:Transform></ds:Tr\
                ansforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"></ds:DigestMethod><ds:DigestValue>(digest)</ds:DigestValue></\
                ds:Reference><ds:Reference Type="http://uri.etsi.org/01903/v1.2.2#SignedProperties" URI="#01_SIG_1_SP"><ds:Transforms><ds:Transform Algorit\
                hm="http://www.w3.org/2001/10/xml-exc-c14n#"></ds:Transform></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sh\
                a256"></ds:DigestMethod><ds:DigestValue>(digest)</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue Id="01_SIG_1_SV">(signat\
                ure)</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>(certificate)</ds:X509Certificate></ds:X509Data></ds:KeyInfo><ds:Objec\
                t><xad:QualifyingProperties xmlns:xad="http://uri.etsi.org/01903/v1.2.2#" Target="#01_SIG_1"><xad:SignedProperties Id="01_SIG_1_SP"><xad:Si\
                gnedSignatureProperties><xad:SigningTime>2025-06-15T18:45:56Z</xad:SigningTime><xad:SigningCertificate><xad:Cert><xad:CertDigest><xad:Diges\
                tMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"></xad:DigestMethod><xad:DigestValue>Vy69/Ggaz3y9Tm8Z+9yEOVrsSYAMASjtSaal6065ZNQ\
                =</xad:DigestValue></xad:CertDigest><xad:IssuerSerial><ds:X509IssuerName>CN=Crypto tests,OU=SCOP,O=Libriciel,L=Montpellier,ST=Occitanie,C=F\
                R,1.2.840.113549.1.9.1=#161969706172617068657572406c696272696369656c2e636f6f70</ds:X509IssuerName><ds:X509SerialNumber>1594214168</ds:X509S\
                erialNumber></xad:IssuerSerial></xad:Cert></xad:SigningCertificate><xad:SignaturePolicyIdentifier><xad:SignaturePolicyId><xad:SigPolicyId><\
                xad:Identifier>urn:oid:1.2.250.1.131.1.5.18.21.1.7</xad:Identifier><xad:Description>Politique de signature Helios de la DGFiP</xad:Descript\
                ion></xad:SigPolicyId><xad:SigPolicyHash><xad:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"></xad:DigestMethod><xad:Dige\
                stValue>GbP1WjbTrHp6h9zlsz5RN7AqkJbnDNDOAQzgm1qzIJ4=</xad:DigestValue></xad:SigPolicyHash><xad:SigPolicyQualifiers><xad:SigPolicyQualifier>\
                <xad:SPURI>https://www.collectivites-locales.gouv.fr/files/finances_locales/dematerialisation/ps_helios_dgfip.pdf</xad:SPURI></xad:SigPolic\
                yQualifier></xad:SigPolicyQualifiers></xad:SignaturePolicyId></xad:SignaturePolicyIdentifier><xad:SignatureProductionPlace><xad:City>Montpe\
                llier</xad:City><xad:PostalCode>34000</xad:PostalCode><xad:CountryName>France</xad:CountryName></xad:SignatureProductionPlace><xad:SignerRo\
                le><xad:ClaimedRoles><xad:ClaimedRole>Signataire</xad:ClaimedRole></xad:ClaimedRoles></xad:SignerRole></xad:SignedSignatureProperties></xad\
                :SignedProperties></xad:QualifyingProperties></ds:Object></ds:Signature></DocumentDetachedExternalSignature>\
                """,
                new String(Base64.decode(signatureHolder.getSignatureResultBase64List().get(0)))
                        .replaceAll("(?<=<ds:SignatureValue Id=\"01_SIG_1_SV\">).*?(?=</ds:SignatureValue>)", "(signature)")
                        .replaceAll("(?<=<ds:X509Certificate>).*?(?=</ds:X509Certificate>)", "(certificate)")
                        .replaceAll("(?<=<ds:DigestValue>).*?(?=</ds:DigestValue>)", "(digest)")
        );
    }


}
