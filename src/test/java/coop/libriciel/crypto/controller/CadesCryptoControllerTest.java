/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.controller;

import coop.libriciel.crypto.models.DataToSign;
import coop.libriciel.crypto.models.request.CadesParameters;
import coop.libriciel.crypto.models.request.legacy.LegacyDataToSignHolder;
import coop.libriciel.crypto.models.request.legacy.LegacySignatureParameters;
import coop.libriciel.crypto.models.request.legacy.SignatureHolder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Base64;
import java.util.HashMap;

import static coop.libriciel.crypto.TestUtils.*;
import static coop.libriciel.crypto.models.DigestAlgorithm.SHA256;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class CadesCryptoControllerTest {


    @Autowired private CadesCryptoController cadesController;
    private final ClassLoader classLoader = getClass().getClassLoader();


    @Test
    void generateDataToSign() throws Exception {

        CadesParameters requestParams = new CadesParameters();
        requestParams.setDataToSignList(singletonList(new DataToSign(PDF_RESOURCE_NAME, PDF_HASH_SHA256_B64, null, null)));
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setPayload(new HashMap<>());
        requestParams.setDigestAlgorithm(SHA256);

        LegacyDataToSignHolder dataToSignHolder = cadesController.getDataToSignApiV1(requestParams);

        assertNotNull(dataToSignHolder);
        assertNotNull(dataToSignHolder.getSignatureDateTime());
        assertNotNull(dataToSignHolder.getDataToSignBase64List());
        assertEquals(1, dataToSignHolder.getDataToSignBase64List().size());
        assertEquals(596, dataToSignHolder.getDataToSignBase64List().get(0).length());
    }


    @Test
    void generateSignature() throws Exception {

        String signature = """
                           mS17RPjmkHWBQNPlHeADrdyceVAR9gsTbZvE1xdmiVeCbfeZKTbuEoYaUYIrLetr9hoMDRhNtStpyUURsgNbcdKe3tscuof3Gzq1r\
                           Js9bE8/vc8YSMjzSUgKqdpXkIJR0ybI1GRDhRbDiGE0/3kBFv83w+l5dRaxSUv0NKOM3MBJQEV2ahU5VKYAjjLNjp6o4DBCC0p5pd\
                           z0NnkoIwUClBGAQt4fteAj2tfp/eQf6W33RFIDcw8u/6q7xfuok0DEwXagWwfkW2RaR+Z/+//hYjeVH/vWHMSMhwbwTVaHVMTfuif\
                           mIJOVWyYJUQIxGGgHGDBv2qGSEnVbCrqVq4vzg==\
                           """;

        LegacySignatureParameters requestParams = new LegacySignatureParameters();
        requestParams.setDataToSignList(singletonList(new DataToSign(PDF_RESOURCE_NAME, PDF_HASH_SHA256_B64, null, signature)));
        requestParams.setPublicCertificateBase64(getCertBase64(classLoader));
        requestParams.setSignatureDateTime(1605890779173L);
        requestParams.setPayload(new HashMap<>());
        requestParams.setDigestAlgorithm(SHA256);

        SignatureHolder signatureHolder = cadesController.getSignatureApiV1(requestParams).getBody();

        assertNotNull(signatureHolder);
        assertNotNull(signatureHolder.getSignatureResultBase64List());
        assertEquals(1, signatureHolder.getSignatureResultBase64List().size());
        assertEquals("""
                     -----BEGIN PKCS7-----
                     MIIHfAYJKoZIhvcNAQcCoIIHbTCCB2kCAQExDTALBglghkgBZQMEAgEwCwYJKoZI
                     hvcNAQcBoIIDuDCCA7QwggKcoAMCAQICBF8FxxgwDQYJKoZIhvcNAQELBQAwgZsx
                     KDAmBgkqhkiG9w0BCQEWGWlwYXJhcGhldXJAbGlicmljaWVsLmNvb3AxCzAJBgNV
                     BAYTAkZSMRIwEAYDVQQIDAlPY2NpdGFuaWUxFDASBgNVBAcMC01vbnRwZWxsaWVy
                     MRIwEAYDVQQKDAlMaWJyaWNpZWwxDTALBgNVBAsMBFNDT1AxFTATBgNVBAMMDENy
                     eXB0byB0ZXN0czAeFw0yMDA3MDgxMzE2MDhaFw0zMDA3MDYxMzE2MDhaMIGbMSgw
                     JgYJKoZIhvcNAQkBFhlpcGFyYXBoZXVyQGxpYnJpY2llbC5jb29wMQswCQYDVQQG
                     EwJGUjESMBAGA1UECAwJT2NjaXRhbmllMRQwEgYDVQQHDAtNb250cGVsbGllcjES
                     MBAGA1UECgwJTGlicmljaWVsMQ0wCwYDVQQLDARTQ09QMRUwEwYDVQQDDAxDcnlw
                     dG8gdGVzdHMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCj9EFe8W73
                     bV7PyWvYCJiYhx9fjMwQlzU0jnGJhdAyIdkmCKAC1ex0zohs8EvbODqprR1d/pyN
                     OQhudPrFggcDds2zhTRjJ0xm668c8BdnXwcMIQARRQYfqCdWz7aFftIIXApPzeFL
                     qdV0AP7wyJvMVRmyrq7xdXRRx2yVKt57ygJDuTaO+yOiPAwEJS74NgnZ67SHjiqD
                     sMIrImbrKEwlDO3v9E5akqyUCU2ajv1zGeHoqAtGNoIJ4FVqVP1B25/T2zs8uPZq
                     MNDg2eERf65FovyYqQLnAR5DLJzwOT3VYLQjsVNRPdyOYz//GW+6h2GK88GRkZUx
                     M6JjB+iBjL6FAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAI+cBVqGdQW8oTurDcr2
                     ooo+r1i1/e23vRTp5Xb7eRowdDwYXcCCa/1tIw1MW/DF9RmUUEF5nNgI5fgD/wAc
                     izdI+S77n57P6kUltvakbGPQ/01WZhnp6TQhV8wVtMZT3NXLQvGLmA9kNjvxCnuW
                     NpViI16lJ8Rwn5OKiCnv6tdJw0yi9Quv28Ar96CTBp2Lv448vjUIy1IrsC9lT1Z8
                     XLVfTapC53W7QjFIoNKeOb3pOgn1msn3YoPa+LhB5o0CXQ/WNTq03/+GJOw8VgqG
                     q4lqrzlwbZXOEYMj47iV7LylIMy2IFq0MbUb0xxJPoFBcXvTQJSCFh9v4bMqQPWO
                     AN4xggOKMIIDhgIBATCBpDCBmzEoMCYGCSqGSIb3DQEJARYZaXBhcmFwaGV1ckBs
                     aWJyaWNpZWwuY29vcDELMAkGA1UEBhMCRlIxEjAQBgNVBAgMCU9jY2l0YW5pZTEU
                     MBIGA1UEBwwLTW9udHBlbGxpZXIxEjAQBgNVBAoMCUxpYnJpY2llbDENMAsGA1UE
                     CwwEU0NPUDEVMBMGA1UEAwwMQ3J5cHRvIHRlc3RzAgRfBccYMAsGCWCGSAFlAwQC
                     AaCCAbowEQYHBACBlTIBATEGMASgAjAAMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0B
                     BwEwHAYJKoZIhvcNAQkFMQ8XDTIwMTEyMDE2NDYxOVowJAYGBACNRQIBMRoMGGFw
                     cGxpY2F0aW9uL29jdGV0LXN0cmVhbTArBgkqhkiG9w0BCTQxHjAcMAsGCWCGSAFl
                     AwQCAaENBgkqhkiG9w0BAQsFADAvBgkqhkiG9w0BCQQxIgQgyRIoShKeTzYHi0oE
                     pTJFviRtD/XtK0wdEhnd3VAiEpYwgegGCyqGSIb3DQEJEAIvMYHYMIHVMIHSMIHP
                     BCBXLr38aBrPfL1Obxn73IQ5WuxJgAwBKO1JpqXrTrlk1DCBqjCBoaSBnjCBmzEo
                     MCYGCSqGSIb3DQEJARYZaXBhcmFwaGV1ckBsaWJyaWNpZWwuY29vcDELMAkGA1UE
                     BhMCRlIxEjAQBgNVBAgMCU9jY2l0YW5pZTEUMBIGA1UEBwwLTW9udHBlbGxpZXIx
                     EjAQBgNVBAoMCUxpYnJpY2llbDENMAsGA1UECwwEU0NPUDEVMBMGA1UEAwwMQ3J5
                     cHRvIHRlc3RzAgRfBccYMA0GCSqGSIb3DQEBCwUABIH/mS17RPjmkHWBQNPlHeAD
                     rdyceVAR9gsTbZvE1xdmiVeCbfeZKTbuEoYaUYIrLetr9hoMDRhNtStpyUURsgNb
                     cdKe3tscuof3Gzq1rJs9bE8/vc8YSMjzSUgKqdpXkIJR0ybI1GRDhRbDiGE0/3kB
                     Fv83w+l5dRaxSUv0NKOM3MBJQEV2ahU5VKYAjjLNjp6o4DBCC0p5pdz0NnkoIwUC
                     lBGAQt4fteAj2tfp/eQf6W33RFIDcw8u/6q7xfuok0DEwXagWwfkW2RaR+Z/+//h
                     YjeVH/vWHMSMhwbwTVaHVMTfuifmIJOVWyYJUQIxGGgHGDBv2qGSEnVbCrqVq4vz
                     -----END PKCS7-----
                     """.trim(),
                new String(Base64.getDecoder().decode(signatureHolder.getSignatureResultBase64List().get(0))).trim()
        );
    }


}
