/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.utils.dss;

import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.pades.SignatureImageParameters;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


@ExtendWith(MockitoExtension.class)
public class LibricielPdfBoxSignatureDrawerTest {


    private final ClassLoader classLoader = getClass().getClassLoader();


    @Test
    void filteringIncompatibleChars() {

        assertEquals(
                "no-break>> <<space",
                LibricielPdfBoxSignatureDrawer.filteringIncompatibleChars("no-break>> <<space", PDType1Font.HELVETICA)
        );

        assertEquals(
                "plop plop",
                LibricielPdfBoxSignatureDrawer.filteringIncompatibleChars("plop \nplop", PDType1Font.HELVETICA)
        );
    }


    @Test
    void getColorSpaceName() {
        assertNull(new LibricielPdfBoxSignatureDrawer().getExpectedColorSpaceName());
    }

    @Test
    void removeNeedAppearancesFlag() throws IOException {

        PDDocument mockedDocument = Mockito.mock(PDDocument.class);
        PDDocumentCatalog mockedDocumentCatalog = Mockito.mock(PDDocumentCatalog.class);
        PDAcroForm mockedAcroForm = Mockito.mock(PDAcroForm.class);
        COSDictionary mockedCosDictionary = Mockito.mock(COSDictionary.class);
        SignatureImageParameters mockedSignatureImageParameter = Mockito.mock(SignatureImageParameters.class);
        DSSDocument mockedImage = Mockito.mock(DSSDocument.class);

        Mockito.when(mockedDocument.getDocumentCatalog()).thenReturn(mockedDocumentCatalog);
        Mockito.when(mockedDocumentCatalog.getAcroForm()).thenReturn(mockedAcroForm);
        Mockito.when(mockedAcroForm.getNeedAppearances()).thenReturn(true);
        Mockito.when(mockedAcroForm.getCOSObject()).thenReturn(mockedCosDictionary);
        Mockito.when(mockedSignatureImageParameter.getImage()).thenReturn(mockedImage);
        Mockito.doThrow(new IOException("nope")).when(mockedAcroForm).refreshAppearances();

        LibricielPdfBoxSignatureDrawer signatureDrawer = new LibricielPdfBoxSignatureDrawer();
        signatureDrawer.init(mockedSignatureImageParameter, mockedDocument, null);

        // This should not crash
        signatureDrawer.removeNeedAppearancesFlag();
    }
}