/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.crypto.utils;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.apache.pdfbox.text.PDFTextStripper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.apache.pdfbox.pdmodel.common.PDRectangle.A4;
import static org.junit.jupiter.api.Assertions.*;


@SuppressWarnings({"WeakerAccess", "SameParameterValue"})
public class PdfAssert {

    private static final int COLOR_DELTA = 1;


    // <editor-fold name="Private utils">


    private static @NotNull List<PDRectangle> getFormLocation(@NotNull PDDocument pdfDoc) throws IOException {

        List<PDRectangle> result = new ArrayList<>();
        PDDocumentCatalog docCatalog = pdfDoc.getDocumentCatalog();

        for (PDPage page : docCatalog.getPages()) {
            for (PDAnnotation annotation : page.getAnnotations()) {
                result.add(annotation.getRectangle());
            }
        }

        return result;
    }


    private static @NotNull List<PDImageXObject> getImages(@NotNull PDDocument pdfDoc) throws IOException {

        List<PDImageXObject> result = new ArrayList<>();
        PDDocumentCatalog docCatalog = pdfDoc.getDocumentCatalog();
        for (PDPage page : docCatalog.getPages()) {

            for (COSName cosName : page.getResources().getXObjectNames()) {
                PDXObject o = page.getResources().getXObject(cosName);
                if (o instanceof PDImageXObject) {
                    result.add((PDImageXObject) o);
                }
            }
        }

        return result;
    }


    /**
     * Checking pixel's color differences, between both images,
     * One pixel out of 100 is tested, allowing a small color difference.
     *
     * @param bufferedImageLeft  The expected one.
     * @param bufferedImageRight The actual one.
     * @param stampCoordinates   If set, we're asserting for visual differences inside the stamp location.
     */
    private static void assertImagesEquals(@NotNull BufferedImage bufferedImageLeft,
                                           @NotNull BufferedImage bufferedImageRight,
                                           @Nullable Rectangle stampCoordinates) {

        boolean hasDifferenceInStamp = false;

        for (int x = 0; x < bufferedImageLeft.getWidth(); x += 10) {
            for (int y = 0; y < bufferedImageLeft.getHeight(); y += 10) {

                int pixelBeforeRgb = bufferedImageLeft.getRGB(x, y);
                int pxBeforeR = (pixelBeforeRgb & 0x00ff0000) >> 16;
                int pxBeforeG = (pixelBeforeRgb & 0x0000ff00) >> 8;
                int pxBeforeB = pixelBeforeRgb & 0x000000ff;

                int pixelAfterRgb = bufferedImageRight.getRGB(x, y);
                int pxAfterR = (pixelAfterRgb & 0x00ff0000) >> 16;
                int pxAfterG = (pixelAfterRgb & 0x0000ff00) >> 8;
                int pxAfterB = pixelAfterRgb & 0x000000ff;

                if ((stampCoordinates != null) && stampCoordinates.contains(x, y)) {
                    hasDifferenceInStamp = hasDifferenceInStamp || (Math.abs(pxBeforeR - pxAfterR) > COLOR_DELTA);
                    hasDifferenceInStamp = hasDifferenceInStamp || (Math.abs(pxBeforeG - pxAfterG) > COLOR_DELTA);
                    hasDifferenceInStamp = hasDifferenceInStamp || (Math.abs(pxBeforeB - pxAfterB) > COLOR_DELTA);
                } else {
                    assertEquals(pxBeforeR, pxAfterR, COLOR_DELTA);
                    assertEquals(pxBeforeG, pxAfterG, COLOR_DELTA);
                    assertEquals(pxBeforeB, pxAfterB, COLOR_DELTA);
                }
            }
        }

        if (stampCoordinates != null) {
            assertTrue(hasDifferenceInStamp);
        }
    }


    private static @NotNull Rectangle pdfToPixelCoordinates(@NotNull Rectangle pdfCoordinates,
                                                            int imageHeight,
                                                            int pdfHeight) {

        // Changing scale

        float ratio = (float) imageHeight / pdfHeight;
        Rectangle result = new Rectangle(
                (int) Math.round(pdfCoordinates.getX() * ratio),
                (int) Math.round(pdfCoordinates.getY() * ratio),
                (int) Math.round(pdfCoordinates.getWidth() * ratio),
                (int) Math.round(pdfCoordinates.getHeight() * ratio)
        );

        // Reversing Y axis
        result.y = (int) (imageHeight - result.getHeight() - result.getY());

        return result;
    }

    // </editor-fold name="Private utils">


    public static void assertIsReadable(@Nullable String absolutePath) throws IOException {

        assertNotNull(absolutePath);

        try (PDDocument pdDoc = PDDocument.load(new File(absolutePath))) {
            String defaultText = new PDFTextStripper().getText(pdDoc);
            assertNotNull(defaultText);
            assertTrue(defaultText.length() > 0);
        }
    }


    public static void assertSignatureCountEquals(@Nullable String absolutePath,
                                                  int signatureCount) throws IOException {

        assertNotNull(absolutePath);

        try (PDDocument defaultDoc = PDDocument.load(new File(absolutePath))) {
            assertEquals(defaultDoc.getSignatureFields().size(), signatureCount);
        }
    }


    public static void assertSignatureLocation(@Nullable String absolutePath,
                                               @Nullable Rectangle expected) throws IOException {

        assertNotNull(absolutePath);
        assertNotNull(expected);

        try (PDDocument doc = PDDocument.load(new File(absolutePath))) {

            assertNotNull(getFormLocation(doc));
            assertFalse(getFormLocation(doc).isEmpty());

            PDRectangle signRect = getFormLocation(doc).get(0);
            assertEquals(expected.getX(), signRect.getLowerLeftX(), 1F);
            assertEquals(expected.getY(), signRect.getLowerLeftY(), 1F);
            assertEquals(expected.getX() + expected.getWidth(), signRect.getUpperRightX(), 1F);
            assertEquals(expected.getY() + expected.getHeight(), signRect.getUpperRightY(), 1F);
        }
    }


    public static void assertStamped(@Nullable String originalAbsolutePath,
                                     @Nullable String stampedAbsolutePath) throws IOException {

        assertNotNull(originalAbsolutePath);
        assertNotNull(stampedAbsolutePath);

        try (PDDocument originalDoc = PDDocument.load(new File(originalAbsolutePath));
             PDDocument stampedDoc = PDDocument.load(new File(stampedAbsolutePath))) {

            List<PDImageXObject> originalImages = getImages(originalDoc);
            int countBefore = originalImages.size();

            List<PDImageXObject> stampedImages = getImages(stampedDoc);
            int countAfter = stampedImages.size();

            assertTrue(countBefore < countAfter);
        }
    }


    public static void assertStampLocation(@Nullable InputStream originalImageStream,
                                           @Nullable InputStream stampedImageStream,
                                           @Nullable Rectangle expectedLocation) throws IOException {

        assertNotNull(originalImageStream);
        assertNotNull(stampedImageStream);
        assertNotNull(expectedLocation);

        BufferedImage originalImage = ImageIO.read(originalImageStream);
        BufferedImage stampedImage = ImageIO.read(stampedImageStream);

        int pageA4Height = Math.round(A4.getHeight());
        Rectangle stampPixLocation = pdfToPixelCoordinates(expectedLocation, stampedImage.getHeight(), pageA4Height);

        // Tests

        assertEquals(stampedImage.getWidth(), originalImage.getWidth());
        assertEquals(stampedImage.getHeight(), originalImage.getHeight());
        assertImagesEquals(originalImage, stampedImage, stampPixLocation);
    }


}
