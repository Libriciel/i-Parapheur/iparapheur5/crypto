/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.crypto.utils;

import coop.libriciel.crypto.TestUtils;
import eu.europa.esig.dss.enumerations.SignatureAlgorithm;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.MethodOrderer.MethodName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.ByteArrayInputStream;
import java.security.KeyPair;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.Date;

import static coop.libriciel.crypto.TestUtils.XML_HASH_SHA256_B64;
import static coop.libriciel.crypto.TestUtils.getCertBase64;
import static coop.libriciel.crypto.utils.StringsUtils.CERTIFICATE_TYPE_X509;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;


@Deprecated
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestMethodOrder(MethodName.class)
public class PesV2Sha256UtilsTest {

    private final ClassLoader classLoader = getClass().getClassLoader();


    @Test
    void generateSignedProperties() throws Exception {

        byte[] publicKeyDer = Base64.getDecoder().decode(getCertBase64(classLoader));
        ByteArrayInputStream inputStream = new ByteArrayInputStream(publicKeyDer);
        CertificateFactory certFactory = CertificateFactory.getInstance(CERTIFICATE_TYPE_X509, BouncyCastleProvider.PROVIDER_NAME);
        X509Certificate certificate = (X509Certificate) certFactory.generateCertificate(inputStream);
        assertNotNull(certificate);

        byte[] result = PesV2Sha256Utils.generateSignedProperties(
                certificate,
                new Date(1522540800L),
                "ID20081042008-04-2901",
                "Montpellier",
                "France",
                "34000",
                "Signature de Maire"
        );

        assertEquals("""
                     <xad:SignedProperties xmlns:xad="http://uri.etsi.org/01903/v1.2.2#" Id="ID20081042008-04-2901_SIG_1_SP"><xad:SignedSignatureProperties>\
                     <xad:SigningTime>1970-01-18T14:55:40Z</xad:SigningTime><xad:SigningCertificate><xad:Cert><xad:CertDigest><xad:DigestMethod Algorithm="h\
                     ttp://www.w3.org/2001/04/xmlenc#sha256"></xad:DigestMethod><xad:DigestValue>Vy69/Ggaz3y9Tm8Z+9yEOVrsSYAMASjtSaal6065ZNQ=</xad:DigestVal\
                     ue></xad:CertDigest><xad:IssuerSerial><ds:X509IssuerName xmlns:ds="http://www.w3.org/2000/09/xmldsig#">CN=Crypto tests,OU=SCOP,O=Libric\
                     iel,L=Montpellier,ST=Occitanie,C=FR,1.2.840.113549.1.9.1=#161969706172617068657572406c696272696369656c2e636f6f70</ds:X509IssuerName><ds\
                     :X509SerialNumber xmlns:ds="http://www.w3.org/2000/09/xmldsig#">1594214168</ds:X509SerialNumber></xad:IssuerSerial></xad:Cert></xad:Sig\
                     ningCertificate><xad:SignaturePolicyIdentifier><xad:SignaturePolicyId><xad:SigPolicyId><xad:Identifier>urn:oid:1.2.250.1.131.1.5.18.21.\
                     1.7</xad:Identifier><xad:Description>Politique de signature Helios de la DGFiP</xad:Description></xad:SigPolicyId><xad:SigPolicyHash><x\
                     ad:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"></xad:DigestMethod><xad:DigestValue>GbP1WjbTrHp6h9zlsz5RN7AqkJbnDND\
                     OAQzgm1qzIJ4=</xad:DigestValue></xad:SigPolicyHash><xad:SigPolicyQualifiers><xad:SigPolicyQualifier><xad:SPURI>https://www.collectivite\
                     s-locales.gouv.fr/files/finances_locales/dematerialisation/ps_helios_dgfip.pdf</xad:SPURI></xad:SigPolicyQualifier></xad:SigPolicyQuali\
                     fiers></xad:SignaturePolicyId></xad:SignaturePolicyIdentifier><xad:SignatureProductionPlace><xad:City>Montpellier</xad:City><xad:Postal\
                     Code>34000</xad:PostalCode><xad:CountryName>France</xad:CountryName></xad:SignatureProductionPlace><xad:SignerRole><xad:ClaimedRoles><x\
                     ad:ClaimedRole>Signature de Maire</xad:ClaimedRole></xad:ClaimedRoles></xad:SignerRole></xad:SignedSignatureProperties></xad:SignedProp\
                     erties>""",
                new String(result));
    }


    @Test
    void generateSignedInfo() throws Exception {

        byte[] publicKeyDer = Base64.getDecoder().decode(getCertBase64(classLoader));
        ByteArrayInputStream inputStream = new ByteArrayInputStream(publicKeyDer);
        CertificateFactory certFactory = CertificateFactory.getInstance(CERTIFICATE_TYPE_X509, BouncyCastleProvider.PROVIDER_NAME);
        X509Certificate certificate = (X509Certificate) certFactory.generateCertificate(inputStream);
        assertNotNull(certificate);

        byte[] result = PesV2Sha256Utils.generateSignedInfo(
                "ID20081042008-04-2901",
                Base64.getDecoder().decode(XML_HASH_SHA256_B64),
                getCertBase64(classLoader),
                new Date(1522540800L),
                "Montpellier",
                "France",
                "34000",
                "Signature de Maire"
        );

        assertNotNull(result);

        String expectedResult =
                """
                <ds:SignedInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#"><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#">\
                </ds:CanonicalizationMethod><ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"></ds:SignatureMethod><ds:Refer\
                ence URI="#ID20081042008-04-2901"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"></ds:Transf\
                orm><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"></ds:Transform></ds:Transforms><ds:DigestMethod Algorithm="http://www.\
                w3.org/2001/04/xmlenc#sha256"></ds:DigestMethod><ds:DigestValue>XkbQ07UwXSh4VyI8/z3XEnUEzvkTyLrJ9CzBHEC7n8E=</ds:DigestValue></ds:Reference>\
                <ds:Reference Type="http://uri.etsi.org/01903/v1.2.2#SignedProperties" URI="#ID20081042008-04-2901_SIG_1_SP"><ds:Transforms><ds:Transform Al\
                gorithm="http://www.w3.org/2001/10/xml-exc-c14n#"></ds:Transform></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlen\
                c#sha256"></ds:DigestMethod><ds:DigestValue>Memy5qXY8uElmsT2GBrsLTFS2pZXj4EdhwXZ9+m0RfQ=</ds:DigestValue></ds:Reference></ds:SignedInfo>\
                """;

        assertEquals(expectedResult, new String(result));
    }


    @Test
    void signature() throws Exception {

        byte[] dataToSign = PesV2Sha256Utils.generateSignedInfo(
                "ID20081042008-04-2901",
                Base64.getDecoder().decode(XML_HASH_SHA256_B64),
                getCertBase64(classLoader),
                new Date(1522540800L),
                "Montpellier",
                "France",
                "34000",
                "Signature de Maire"
        );

        assertNotNull(dataToSign);

        KeyPair pair = TestUtils.getDemoKeyPair(classLoader);
        String signature = TestUtils.sign(dataToSign, pair.getPrivate(), SignatureAlgorithm.RSA_SHA256);

        String signatureExpected =
                """
                Lyjv/JBe7RgnBMsATM+tfACSEOKLrloR3YupJYVh9khGErogXlxEEBTIjLlrYYNM134DXBks14FvS2vjvhLZu4nojplOKqPZXJTV8Q9AfGntfUpwIcIdJlwwGE5flQSbXui2TdkhILXe\
                2G9RRDWcxqvsD88PdVl53OEiyitCx2RxrEhAzkaRUtb0qhKv0Wkl3b5CnwUDtOQzUeD8NKe8+ejoH+fX2op70tpFodRRDuAAISaUf5xM1D8uK3hhgFR96L4exEZPlSMwE83mKe8gta8d\
                JguVXHZQIKw5k1GvA0eYxsqgVYV1AE/StgqQpbEqtke6qbFGjFvtNekyx1HNhA==\
                """;

        assertEquals(signatureExpected, signature);

        byte[] result = PesV2Sha256Utils.signature(
                StringsUtils.toX509Certificate(getCertBase64(classLoader)),
                Base64.getDecoder().decode(XML_HASH_SHA256_B64),
                new Date(1522540800L),
                "ID20081042008-04-2901",
                "Montpellier",
                "France",
                "34000",
                "Signature de Maire",
                signature
        );

        assertNotNull(result);

        String expected =
                """
                <DocumentDetachedExternalSignature><ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#" Id="ID20081042008-04-2901_SIG_1"><ds:SignedInf\
                o><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"></ds:CanonicalizationMethod><ds:SignatureMethod Algorithm="h\
                ttp://www.w3.org/2001/04/xmldsig-more#rsa-sha256"></ds:SignatureMethod><ds:Reference URI="#ID20081042008-04-2901"><ds:Transforms><ds:Transfor\
                m Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"></ds:Transform><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc\
                -c14n#"></ds:Transform></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"></ds:DigestMethod><ds:DigestValue\
                >XkbQ07UwXSh4VyI8/z3XEnUEzvkTyLrJ9CzBHEC7n8E=</ds:DigestValue></ds:Reference><ds:Reference Type="http://uri.etsi.org/01903/v1.2.2#SignedPrope\
                rties" URI="#ID20081042008-04-2901_SIG_1_SP"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"></ds:Transform>\
                </ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"></ds:DigestMethod><ds:DigestValue>Memy5qXY8uElmsT2GBrsLT\
                FS2pZXj4EdhwXZ9+m0RfQ=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue Id="ID20081042008-04-2901_SIG_1_SV">Lyjv/JBe7RgnBMsA\
                TM+tfACSEOKLrloR3YupJYVh9khGErogXlxEEBTIjLlrYYNM134DXBks14FvS2vjvhLZu4nojplOKqPZXJTV8Q9AfGntfUpwIcIdJlwwGE5flQSbXui2TdkhILXe2G9RRDWcxqvsD88Pd\
                Vl53OEiyitCx2RxrEhAzkaRUtb0qhKv0Wkl3b5CnwUDtOQzUeD8NKe8+ejoH+fX2op70tpFodRRDuAAISaUf5xM1D8uK3hhgFR96L4exEZPlSMwE83mKe8gta8dJguVXHZQIKw5k1GvA0\
                eYxsqgVYV1AE/StgqQpbEqtke6qbFGjFvtNekyx1HNhA==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIIDtDCCApygAwIBAgIEXwXHGDANBg\
                kqhkiG9w0BAQsFADCBmzEoMCYGCSqGSIb3DQEJARYZaXBhcmFwaGV1ckBsaWJyaWNpZWwuY29vcDELMAkGA1UEBhMCRlIxEjAQBgNVBAgMCU9jY2l0YW5pZTEUMBIGA1UEBwwLTW9udHB\
                lbGxpZXIxEjAQBgNVBAoMCUxpYnJpY2llbDENMAsGA1UECwwEU0NPUDEVMBMGA1UEAwwMQ3J5cHRvIHRlc3RzMB4XDTIwMDcwODEzMTYwOFoXDTMwMDcwNjEzMTYwOFowgZsxKDAmBgkq\
                hkiG9w0BCQEWGWlwYXJhcGhldXJAbGlicmljaWVsLmNvb3AxCzAJBgNVBAYTAkZSMRIwEAYDVQQIDAlPY2NpdGFuaWUxFDASBgNVBAcMC01vbnRwZWxsaWVyMRIwEAYDVQQKDAlMaWJya\
                WNpZWwxDTALBgNVBAsMBFNDT1AxFTATBgNVBAMMDENyeXB0byB0ZXN0czCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKP0QV7xbvdtXs/Ja9gImJiHH1+MzBCXNTSOcYmF0D\
                Ih2SYIoALV7HTOiGzwS9s4OqmtHV3+nI05CG50+sWCBwN2zbOFNGMnTGbrrxzwF2dfBwwhABFFBh+oJ1bPtoV+0ghcCk/N4Uup1XQA/vDIm8xVGbKurvF1dFHHbJUq3nvKAkO5No77I6I\
                8DAQlLvg2CdnrtIeOKoOwwisiZusoTCUM7e/0TlqSrJQJTZqO/XMZ4eioC0Y2ggngVWpU/UHbn9PbOzy49mow0ODZ4RF/rkWi/JipAucBHkMsnPA5PdVgtCOxU1E93I5jP/8Zb7qHYYrz\
                wZGRlTEzomMH6IGMvoUCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAj5wFWoZ1BbyhO6sNyvaiij6vWLX97be9FOnldvt5GjB0PBhdwIJr/W0jDUxb8MX1GZRQQXmc2Ajl+AP/AByLN0j5L\
                vufns/qRSW29qRsY9D/TVZmGenpNCFXzBW0xlPc1ctC8YuYD2Q2O/EKe5Y2lWIjXqUnxHCfk4qIKe/q10nDTKL1C6/bwCv3oJMGnYu/jjy+NQjLUiuwL2VPVnxctV9NqkLndbtCMUig0p\
                45vek6CfWayfdig9r4uEHmjQJdD9Y1OrTf/4Yk7DxWCoariWqvOXBtlc4RgyPjuJXsvKUgzLYgWrQxtRvTHEk+gUFxe9NAlIIWH2/hsypA9Y4A3g==</ds:X509Certificate></ds:X\
                509Data></ds:KeyInfo><ds:Object><xad:QualifyingProperties xmlns:xad="http://uri.etsi.org/01903/v1.2.2#" Target="#ID20081042008-04-2901_SIG_1"\
                ><xad:SignedProperties Id="ID20081042008-04-2901_SIG_1_SP"><xad:SignedSignatureProperties><xad:SigningTime>1970-01-18T14:55:40Z</xad:SigningT\
                ime><xad:SigningCertificate><xad:Cert><xad:CertDigest><xad:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"></xad:DigestMetho\
                d><xad:DigestValue>Vy69/Ggaz3y9Tm8Z+9yEOVrsSYAMASjtSaal6065ZNQ=</xad:DigestValue></xad:CertDigest><xad:IssuerSerial><ds:X509IssuerName>CN=Cry\
                pto tests,OU=SCOP,O=Libriciel,L=Montpellier,ST=Occitanie,C=FR,1.2.840.113549.1.9.1=#161969706172617068657572406c696272696369656c2e636f6f70</d\
                s:X509IssuerName><ds:X509SerialNumber>1594214168</ds:X509SerialNumber></xad:IssuerSerial></xad:Cert></xad:SigningCertificate><xad:SignaturePo\
                licyIdentifier><xad:SignaturePolicyId><xad:SigPolicyId><xad:Identifier>urn:oid:1.2.250.1.131.1.5.18.21.1.7</xad:Identifier><xad:Description>P\
                olitique de signature Helios de la DGFiP</xad:Description></xad:SigPolicyId><xad:SigPolicyHash><xad:DigestMethod Algorithm="http://www.w3.org\
                /2001/04/xmlenc#sha256"></xad:DigestMethod><xad:DigestValue>GbP1WjbTrHp6h9zlsz5RN7AqkJbnDNDOAQzgm1qzIJ4=</xad:DigestValue></xad:SigPolicyHash\
                ><xad:SigPolicyQualifiers><xad:SigPolicyQualifier><xad:SPURI>https://www.collectivites-locales.gouv.fr/files/finances_locales/dematerialisati\
                on/ps_helios_dgfip.pdf</xad:SPURI></xad:SigPolicyQualifier></xad:SigPolicyQualifiers></xad:SignaturePolicyId></xad:SignaturePolicyIdentifier>\
                <xad:SignatureProductionPlace><xad:City>Montpellier</xad:City><xad:PostalCode>34000</xad:PostalCode><xad:CountryName>France</xad:CountryName>\
                </xad:SignatureProductionPlace><xad:SignerRole><xad:ClaimedRoles><xad:ClaimedRole>Signature de Maire</xad:ClaimedRole></xad:ClaimedRoles></xa\
                d:SignerRole></xad:SignedSignatureProperties></xad:SignedProperties></xad:QualifyingProperties></ds:Object></ds:Signature></DocumentDetachedE\
                xternalSignature>\
                """;

        assertEquals(expected, new String(result));
    }

}
