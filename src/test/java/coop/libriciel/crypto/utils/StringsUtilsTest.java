/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.utils;

import coop.libriciel.crypto.models.stamp.PdfSignatureStamp;
import coop.libriciel.crypto.models.stamp.PdfSignatureStampElement;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static coop.libriciel.crypto.TestUtils.getCertBase64;
import static org.apache.pdfbox.pdmodel.font.PDType1Font.HELVETICA;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class StringsUtilsTest {


    private final ClassLoader classLoader = getClass().getClassLoader();


    @Test
    void toIso8601String() {
        Date date = new Date(1291397040000L);
        assertEquals("2010-12-03T17:24:00Z", StringsUtils.toIso8601String(date));
    }


    @Test
    void toX509Certificate() throws Exception {

        X509Certificate certificate = StringsUtils.toX509Certificate(getCertBase64(classLoader));

        assertNotNull(certificate);
        assertEquals("E=iparapheur@libriciel.coop,C=FR,ST=Occitanie,L=Montpellier,O=Libriciel,OU=SCOP,CN=Crypto tests", certificate.getIssuerDN().getName());
        assertEquals("CN=Crypto tests,OU=SCOP,O=Libriciel,L=Montpellier,ST=Occitanie,C=FR,1.2.840.113549.1.9.1=#161969706172617068657572406c696272696369656c2e636f6f70", certificate.getIssuerX500Principal().getName());
        assertEquals(1909574168000L, certificate.getNotAfter().getTime());
        assertEquals(1594214168000L, certificate.getNotBefore().getTime());
    }


    @Test
    void calculateOptimalFontSize() throws IOException {

        PdfSignatureStampElement element = new PdfSignatureStampElement();
        element.setX(0F);
        element.setY(0F);
        element.setFont(PdfSignatureStampElement.SignatureStampFont.HELVETICA);
        element.setFontSize(0);

        PdfSignatureStamp signatureStamp = new PdfSignatureStamp();
        signatureStamp.setWidth(100F);
        signatureStamp.setHeight(100F);
        signatureStamp.setElements(Collections.singletonList(element));

        float computedFontSize = StringsUtils.calculateOptimalFontSize(
                signatureStamp,
                element,
                PDType1Font.HELVETICA,
                List.of("The quick brown fox jumps over a lazy dog",
                        "Pack my box with five dozen liquor jugs",
                        "Jackdaws love my big sphinx of quartz")
        );

        assertEquals(5.2F, computedFontSize, 0.1F);
    }


    @Test
    void splitOverflowingLine() throws Exception {
        assertEquals(List.of("test1 test2 test3 test4 test5"), StringsUtils.splitOverflowingLine("test1 test2 test3 test4 test5", 100, HELVETICA, 8));
        assertEquals(List.of("test1 test2 test3", "test4 test5"), StringsUtils.splitOverflowingLine("test1 test2 test3 test4 test5", 60, HELVETICA, 8));
        assertEquals(List.of("test1", "test2", "test3", "test4", "test5"), StringsUtils.splitOverflowingLine("test1 test2 test3 test4 test5", 20, HELVETICA, 8));
        assertEquals(List.of("test1", "test2", "test3", "test4", "test5"), StringsUtils.splitOverflowingLine("test1 test2 test3 test4 test5", 0, HELVETICA, 8));
    }


}
