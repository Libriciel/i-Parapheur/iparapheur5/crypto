/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import coop.libriciel.crypto.models.DataToSign;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONStringer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static coop.libriciel.crypto.utils.IntegrationTestUtils.*;
import static eu.europa.esig.dss.enumerations.SignatureAlgorithm.RSA_SHA256;
import static org.apache.commons.io.FileUtils.writeByteArrayToFile;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@SuppressWarnings("DuplicatedCode")
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class IP50IntegrationTest {


    @Autowired private MockMvc mockMvc;
    private final ClassLoader classLoader = getClass().getClassLoader();


    @Test
    public void padesSignature() throws Exception {

        // getDataToSign

        JSONStringer dtsStringer = new JSONStringer()
                .object()
                /**/.key("payload").object()
                /**//**/.key("additionalProp1").value("string1")
                /**//**/.key("additionalProp2").value("string2")
                /**/.endObject()
                /**/.key("publicCertificateBase64").value(getCertBase64(classLoader))
                .endObject();

        AtomicReference<String> idAtomic = new AtomicReference<>();
        AtomicReference<Long> signatureDateTimeAtomic = new AtomicReference<>();
        AtomicReference<String> dataToSignBase64Atomic = new AtomicReference<>();

        this.mockMvc
                .perform(
                        multipart("/api/v2/pades/generateDataToSign")
                                .file("file", buildFileContent(classLoader, TEST_PDF_FILENAME))
                                .param("model", dtsStringer.toString())
                                .contentType(MULTIPART_FORM_DATA_VALUE)
                                .accept(APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.dataToSignList.[*]", hasSize(1)))
                .andExpect(jsonPath("$.dataToSignList.[*].id").exists())
                .andExpect(jsonPath("$.dataToSignList.[*].dataToSignBase64").exists())
                .andExpect(jsonPath("$.signatureDateTime").exists())

                .andDo(result -> {
                    JsonObject root = JsonParser.parseString(result.getResponse().getContentAsString()).getAsJsonObject();
                    signatureDateTimeAtomic.set(root.get("signatureDateTime").getAsLong());

                    JsonObject parsedDocument = root.get("dataToSignList").getAsJsonArray().get(0).getAsJsonObject();
                    idAtomic.set(parsedDocument.get("id").getAsString());
                    dataToSignBase64Atomic.set(parsedDocument.get("dataToSignBase64").getAsString());
                });

        // Actual signature, client-side

        assertNotNull(idAtomic.get());
        assertNotNull(dataToSignBase64Atomic.get());
        String signatureBase64 = sign(classLoader, dataToSignBase64Atomic.get(), RSA_SHA256);
        assertNotNull(signatureBase64);

        // generateSignature

        JSONStringer sigStringer = new JSONStringer()
                .object()
                /**/.key("payload").object()
                /**//**/.key("additionalProp1").value("string1")
                /**//**/.key("additionalProp2").value("string2")
                /**/.endObject()
                /**/.key("publicCertificateBase64").value(getCertBase64(classLoader))
                /**/.key("dataToSignList").array()
                /**//**/.object()
                /**//**//**/.key("id").value("01")
                /**//**//**/.key("dataToSignBase64").value(dataToSignBase64Atomic.get())
                /**//**//**/.key("signatureBase64").value(signatureBase64)
                /**//**/.endObject()
                /**/.endArray()
                /**/.key("stamp").value(null)
                /**/.key("signatureDateTime").value(signatureDateTimeAtomic.get())
                .endObject();

        // The standard MockMvc#print() method will print the entire response body.
        // When the response is a PDF file, we don't want that.
        // So we fake the logs here :
        System.out.println();
        System.out.println("MockHttpServletRequest:");
        System.out.println("      Request URI = /api/v2/pades/generateSignature");
        System.out.println("      Method     = POST");
        System.out.println("      Parameters = " + sigStringer.toString());

        this.mockMvc
                .perform(
                        multipart("/api/v2/pades/generateSignature")
                                .file("file", buildFileContent(classLoader, TEST_PDF_FILENAME))
                                .param("model", sigStringer.toString())
                                .contentType(MULTIPART_FORM_DATA_VALUE)
                )
                .andExpect(status().isOk())
                .andDo(result -> {
                    byte[] finalSignedFileBytes = result.getResponse().getContentAsByteArray();
                    assertNotEquals(0, finalSignedFileBytes.length);
                    String signatureResult = "build/test-results/intTest_" + getClass().getSimpleName() + "_" + System.currentTimeMillis() + "_signed.pdf";
                    File signedFile = new File(signatureResult);
                    writeByteArrayToFile(signedFile, finalSignedFileBytes);
                });
    }


    @Test
    public void xadesSignature_enveloped() throws Exception {

        // getDataToSign

        JSONStringer dtsStringer = new JSONStringer()
                .object()
                /**/.key("payload").object()
                /**//**/.key("additionalProp1").value("string1")
                /**//**/.key("additionalProp2").value("string2")
                /**/.endObject()
                /**/.key("publicCertificateBase64").value(getCertBase64(classLoader))
                /**/.key("signaturePackaging").value("ENVELOPED")
                /**/.key("xpath").value("//Bordereau")
                .endObject();

        List<DataToSign> pendingDocumentList = new ArrayList<>();
        AtomicReference<Long> signatureDateTimeAtomic = new AtomicReference<>();

        this.mockMvc
                .perform(
                        multipart("/api/v2/xades/generateDataToSign")
                                .file("file", buildFileContent(classLoader, TEST_XML_FILENAME))
                                .param("model", dtsStringer.toString())
                                .contentType(MULTIPART_FORM_DATA_VALUE)
                                .accept(APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.dataToSignList.[*]", hasSize(3)))
                .andExpect(jsonPath("$.dataToSignList.[*].id").exists())
                .andExpect(jsonPath("$.dataToSignList.[*].dataToSignBase64").exists())
                .andExpect(jsonPath("$.signatureDateTime").exists())

                .andDo(result -> {
                    JsonObject root = JsonParser.parseString(result.getResponse().getContentAsString()).getAsJsonObject();
                    signatureDateTimeAtomic.set(root.get("signatureDateTime").getAsLong());

                    root.get("dataToSignList").getAsJsonArray().forEach(
                            jsonElement -> {
                                JsonObject parsedDocument = jsonElement.getAsJsonObject();
                                pendingDocumentList.add(new DataToSign(
                                        parsedDocument.get("id").getAsString(),
                                        null,
                                        parsedDocument.get("dataToSignBase64").getAsString(),
                                        null
                                ));
                            }
                    );
                });

        // Actual signature, client-side

        for (DataToSign doc : pendingDocumentList) {
            String signatureBase64 = sign(classLoader, doc.getDataToSignBase64(), RSA_SHA256);
            assertTrue(StringUtils.isNotEmpty(signatureBase64));
            doc.setSignatureValue(signatureBase64);
        }

        // generateSignature

        JSONStringer stringer = new JSONStringer()
                .object()
                /**/.key("payload").object()
                /**//**/.key("additionalProp1").value("string1")
                /**//**/.key("additionalProp2").value("string2")
                /**/.endObject()
                /**/.key("publicKeyBase64").value(getCertBase64(classLoader))
                /**/.key("dataToSignList").array();

        for (DataToSign document : pendingDocumentList) {
            stringer.object()
                    /**/.key("id").value(document.getId())
                    /**/.key("dataToSignBase64").value(document.getDataToSignBase64())
                    /**/.key("signatureBase64").value(document.getSignatureValue())
                    .endObject();
        }

        stringer/**/.endArray()
                /**/.key("stamp").value(null)
                /**/.key("signaturePackaging").value("ENVELOPED")
                /**/.key("signatureDateTime").value(signatureDateTimeAtomic.get())
                /**/.key("xpath").value("//Bordereau")
                .endObject();

        // The standard MockMvc#print() method will print the entire response body.
        // When the response is a PDF file, we don't want that.
        // So we fake the logs here :
        System.out.println();
        System.out.println("MockHttpServletRequest:");
        System.out.println("     Request URI = /api/v2/xades/generateSignature");
        System.out.println("      Method     = POST");
        System.out.println("      Parameters = " + stringer);

        this.mockMvc
                .perform(
                        multipart("/api/v2/xades/generateSignature")
                                .file("file", buildFileContent(classLoader, TEST_XML_FILENAME))
                                .param("model", stringer.toString())
                                .contentType(MULTIPART_FORM_DATA_VALUE)
                )
                .andExpect(status().isOk())
                .andDo(result -> {
                    byte[] finalSignedFileBytes = result.getResponse().getContentAsByteArray();
                    assertNotEquals(0, finalSignedFileBytes.length);
                    String signatureResult = "build/test-results/intTest_" + getClass().getSimpleName() + "_enveloped.xml";
                    File signedFile = new File(signatureResult);
                    writeByteArrayToFile(signedFile, finalSignedFileBytes);
                });
    }


    @Test
    public void xadesSignature_detached() throws Exception {

        // getDataToSign

        JSONStringer dtsStringer = new JSONStringer()
                .object()
                /**/.key("payload").object()
                /**//**/.key("additionalProp1").value("string1")
                /**//**/.key("additionalProp2").value("string2")
                /**/.endObject()
                /**/.key("publicCertificateBase64").value(getCertBase64(classLoader))
                /**/.key("signaturePackaging").value("DETACHED")
                /**/.key("xpath").value("/*")
                .endObject();

        List<DataToSign> pendingDocumentList = new ArrayList<>();
        AtomicReference<Long> signatureDateTimeAtomic = new AtomicReference<>();

        this.mockMvc
                .perform(
                        multipart("/api/v2/xades/generateDataToSign")
                                .file("file", buildFileContent(classLoader, TEST_PDF_FILENAME))
                                .param("model", dtsStringer.toString())
                                .contentType(MULTIPART_FORM_DATA_VALUE)
                                .accept(APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.dataToSignList.[*]", hasSize(1)))
                .andExpect(jsonPath("$.dataToSignList.[*].id").exists())
                .andExpect(jsonPath("$.dataToSignList.[*].dataToSignBase64").exists())
                .andExpect(jsonPath("$.signatureDateTime").exists())

                .andDo(result -> {
                    JsonObject root = JsonParser.parseString(result.getResponse().getContentAsString()).getAsJsonObject();
                    signatureDateTimeAtomic.set(root.get("signatureDateTime").getAsLong());

                    root.get("dataToSignList").getAsJsonArray().forEach(
                            jsonElement -> {
                                JsonObject parsedDocument = jsonElement.getAsJsonObject();
                                pendingDocumentList.add(new DataToSign(
                                        parsedDocument.get("id").getAsString(),
                                        null,
                                        parsedDocument.get("dataToSignBase64").getAsString(),
                                        null
                                ));
                            }
                    );
                });

        // Actual signature, client-side

        for (DataToSign doc : pendingDocumentList) {
            String signatureBase64 = sign(classLoader, doc.getDataToSignBase64(), RSA_SHA256);
            assertTrue(StringUtils.isNotEmpty(signatureBase64));
            doc.setSignatureValue(signatureBase64);
        }

        // generateSignature

        JSONStringer sigStringer = new JSONStringer()
                .object()
                /**/.key("payload").object()
                /**//**/.key("additionalProp1").value("string1")
                /**//**/.key("additionalProp2").value("string2")
                /**/.endObject()
                /**/.key("publicKeyBase64").value(getCertBase64(classLoader))
                /**/.key("signaturePackaging").value("DETACHED")
                /**/.key("dataToSignList").array();

        for (DataToSign document : pendingDocumentList) {
            sigStringer.object()
                    /**/.key("id").value(document.getId())
                    /**/.key("dataToSignBase64").value(document.getDataToSignBase64())
                    /**/.key("signatureBase64").value(document.getSignatureValue())
                    .endObject();
        }

        sigStringer/**/.endArray()
                /**/.key("stamp").value(null)
                /**/.key("signatureDateTime").value(signatureDateTimeAtomic.get())
                /**/.key("xpath").value("/*")
                .endObject();

        // The standard MockMvc#print() method will print the entire response body.
        // When the response is a PDF file, we don't want that.
        // So we fake the logs here :
        System.out.println();
        System.out.println("MockHttpServletRequest:");
        System.out.println("      Request URI = /api/v2/xades/generateSignature");
        System.out.println("      Method     = POST");
        System.out.println("      Parameters = " + sigStringer);

        this.mockMvc
                .perform(
                        multipart("/api/v2/xades/generateSignature")
                                .file("file", buildFileContent(classLoader, TEST_XML_FILENAME))
                                .param("model", sigStringer.toString())
                                .contentType(MULTIPART_FORM_DATA_VALUE)
                )
                .andExpect(status().isOk())
                .andDo(result -> {
                    byte[] finalSignedFileBytes = result.getResponse().getContentAsByteArray();
                    assertNotEquals(0, finalSignedFileBytes.length);
                    String signatureResult = "build/test-results/intTest_" + getClass().getSimpleName() + "_detached.xml";
                    File signedFile = new File(signatureResult);
                    writeByteArrayToFile(signedFile, finalSignedFileBytes);
                });
    }


    @Test
    public void cadesSignature() throws Exception {

        // getDataToSign

        JSONStringer dtsStringer = new JSONStringer()
                .object()
                /**/.key("payload").object()
                /**//**/.key("additionalProp1").value("string1")
                /**//**/.key("additionalProp2").value("string2")
                /**/.endObject()
                /**/.key("publicCertificateBase64").value(getCertBase64(classLoader))
                .endObject();

        AtomicReference<String> idAtomic = new AtomicReference<>();
        AtomicReference<Long> signatureDateTimeAtomic = new AtomicReference<>();
        AtomicReference<String> dataToSignBase64Atomic = new AtomicReference<>();

        this.mockMvc
                .perform(
                        multipart("/api/v2/cades/generateDataToSign")
                                .file("file", buildFileContent(classLoader, TEST_PDF_FILENAME))
                                .param("model", dtsStringer.toString())
                                .contentType(MULTIPART_FORM_DATA_VALUE)
                                .accept(APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.dataToSignList.[*]", hasSize(1)))
                .andExpect(jsonPath("$.dataToSignList.[*].id").exists())
                .andExpect(jsonPath("$.dataToSignList.[*].dataToSignBase64").exists())
                .andExpect(jsonPath("$.signatureDateTime").exists())

                .andDo(result -> {
                    JsonObject root = JsonParser.parseString(result.getResponse().getContentAsString()).getAsJsonObject();
                    signatureDateTimeAtomic.set(root.get("signatureDateTime").getAsLong());

                    JsonObject parsedDocument = root.get("dataToSignList").getAsJsonArray().get(0).getAsJsonObject();
                    idAtomic.set(parsedDocument.get("id").getAsString());
                    dataToSignBase64Atomic.set(parsedDocument.get("dataToSignBase64").getAsString());
                });

        // Actual signature, client-side

        assertNotNull(idAtomic.get());
        assertNotNull(dataToSignBase64Atomic.get());
        String signatureBase64 = sign(classLoader, dataToSignBase64Atomic.get(), RSA_SHA256);
        assertNotNull(signatureBase64);

        // generateSignature

        JSONStringer sigStringer = new JSONStringer()
                .object()
                /**/.key("payload").object()
                /**//**/.key("additionalProp1").value("string1")
                /**//**/.key("additionalProp2").value("string2")
                /**/.endObject()
                /**/.key("publicCertificateBase64").value(getCertBase64(classLoader))
                /**/.key("dataToSignList").array()
                /**//**/.object()
                /**//**//**/.key("id").value("01")
                /**//**//**/.key("dataToSignBase64").value(dataToSignBase64Atomic.get())
                /**//**//**/.key("signatureBase64").value(signatureBase64)
                /**//**/.endObject()
                /**/.endArray()
                /**/.key("signatureDateTime").value(signatureDateTimeAtomic.get())
                .endObject();

        // The standard MockMvc#print() method will print the entire response body.
        // When the response is a PDF file, we don't want that.
        // So we fake the logs here :
        System.out.println();
        System.out.println("MockHttpServletRequest:");
        System.out.println("      Request URI = /api/v2/cades/generateSignature");
        System.out.println("      Method     = POST");
        System.out.println("      Parameters = " + sigStringer.toString());

        this.mockMvc
                .perform(
                        multipart("/api/v2/cades/generateSignature")
                                .param("model", sigStringer.toString())
                                .contentType(MULTIPART_FORM_DATA_VALUE)
                )
                .andExpect(status().isOk())
                .andDo(result -> {
                    byte[] finalSignedFileBytes = result.getResponse().getContentAsByteArray();
                    assertNotEquals(0, finalSignedFileBytes.length);
                    String signatureResult = "build/test-results/intTest_" + getClass().getSimpleName() + ".p7s";
                    File signedFile = new File(signatureResult);
                    writeByteArrayToFile(signedFile, finalSignedFileBytes);
                });
    }


}
