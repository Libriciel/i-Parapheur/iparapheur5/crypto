/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.utils;

import eu.europa.esig.dss.enumerations.SignatureAlgorithm;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.util.Base64;

import static org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA_256;
import static org.apache.commons.lang3.StringUtils.deleteWhitespace;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SuppressWarnings("SpellCheckingInspection")
public class IntegrationTestUtils {

    private static final String KEYSTORE_JKS_FILE = "test.jks";
    private static final String KEYSTORE_JKS_ALIAS = "test";
    private static final String KEYSTORE_JKS_PASSWORD = "1234";
    public static final String TEST_PDF_FILENAME = "test.pdf";
    public static final String TEST_XML_FILENAME = "pes_multi.xml";


    public static byte[] buildFileContent(@NotNull ClassLoader classLoader, @NotNull String fileName) throws IOException {
        InputStream fileInputStream = classLoader.getResourceAsStream(fileName);
        assertNotNull(fileInputStream);
        return IOUtils.toByteArray(fileInputStream);
    }


    public static String getDigestBase64(@NotNull ClassLoader classLoader, @NotNull String fileName) throws IOException {
        try (InputStream inputStream = classLoader.getResourceAsStream(fileName)) {
            byte[] hashBytes = new DigestUtils(SHA_256).digest(inputStream);
            return Base64.getEncoder().encodeToString(hashBytes);
        }
    }


    public static @NotNull String getCertBase64(@NotNull ClassLoader classLoader) throws Exception {

        KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
        char[] password = KEYSTORE_JKS_PASSWORD.toCharArray();
        InputStream inputStream = classLoader.getResourceAsStream(KEYSTORE_JKS_FILE);
        keystore.load(inputStream, password);

        Key key = keystore.getKey(KEYSTORE_JKS_ALIAS, password);
        if (!(key instanceof PrivateKey)) {
            throw new KeyStoreException("Something is wrong, inside");
        }

        java.security.cert.Certificate cert = keystore.getCertificate(KEYSTORE_JKS_ALIAS);
        return Base64.getEncoder().encodeToString(cert.getEncoded());
    }


    public static @NotNull KeyPair getDemoKeyPair(@NotNull ClassLoader classLoader) throws Exception {

        InputStream ins = classLoader.getResourceAsStream(KEYSTORE_JKS_FILE);
        KeyStore keyStore = KeyStore.getInstance("JCEKS");
        keyStore.load(ins, KEYSTORE_JKS_PASSWORD.toCharArray());

        KeyStore.PasswordProtection keyPassword = new KeyStore.PasswordProtection(KEYSTORE_JKS_PASSWORD.toCharArray());
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEYSTORE_JKS_ALIAS, keyPassword);

        java.security.cert.Certificate cert = keyStore.getCertificate(KEYSTORE_JKS_ALIAS);
        PublicKey publicKey = cert.getPublicKey();
        PrivateKey privateKey = privateKeyEntry.getPrivateKey();

        return new KeyPair(publicKey, privateKey);
    }


    public static @NotNull String sign(@NotNull ClassLoader classLoader, @NotNull String dataToSignBase64,
                                       @NotNull SignatureAlgorithm algorithm) throws Exception {

        byte[] byteToSign = Base64.getDecoder().decode(deleteWhitespace(dataToSignBase64));
        KeyPair pair = getDemoKeyPair(classLoader);
        String signature = sign(byteToSign, pair.getPrivate(), algorithm);
        assertTrue(verify(byteToSign, signature, pair.getPublic(), algorithm));

        return signature;
    }


    public static @NotNull String sign(byte[] bytesToSign, @NotNull PrivateKey privateKey,
                                       @NotNull SignatureAlgorithm signatureAlgorithm) throws Exception {

        Signature privateSignature = Signature.getInstance(signatureAlgorithm.getJCEId());
        privateSignature.initSign(privateKey);
        privateSignature.update(bytesToSign);

        byte[] signature = privateSignature.sign();
        return Base64.getEncoder().encodeToString(signature);
    }


    public static boolean verify(byte[] bytesToSign, @NotNull String signature, @NotNull PublicKey publicKey,
                                 @NotNull SignatureAlgorithm signatureAlgorithm) throws Exception {

        Signature publicSignature = Signature.getInstance(signatureAlgorithm.getJCEId());
        publicSignature.initVerify(publicKey);
        publicSignature.update(bytesToSign);

        byte[] signatureBytes = Base64.getDecoder().decode(signature);
        return publicSignature.verify(signatureBytes);
    }

}
