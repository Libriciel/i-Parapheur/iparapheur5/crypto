/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.utils;

import lombok.extern.log4j.Log4j2;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.jetbrains.annotations.NotNull;


@Log4j2
public class DocumentUtils {


    /**
     * Compute the page, if exists :
     * * 1 : first page
     * * 2 : second page
     * <p>
     * Some default case:
     * * 0 : last page (IPv4 behavior...)
     * <p>
     * Counting from the end:
     * * -1 : last page
     * * -2 : second to last page
     *
     * @param numberOfPages the real PDF count
     * @param page          the asked one
     * @return the computed one
     */
    public static int computePdfPageNumber(int numberOfPages, int page) {

        if (page == 0) {
            page = -1;
        }

        boolean positiveOverflow = page > numberOfPages;
        if (positiveOverflow) {
            log.warn("computePdfPageNumber overflow (page {} of {}), using the last page instead...", page, numberOfPages);
            return numberOfPages;
        }

        boolean negativeOverflow = (-page) > numberOfPages;
        if (negativeOverflow) {
            log.warn("computePdfPageNumber negative overflow (page {} of {}), using the first page instead...", page, numberOfPages);
            return 1;
        }

        if (page < 0) {
            return numberOfPages + 1 + page;
        }

        return page;
    }


    public static PDType0Font loadFont(@NotNull PDDocument document, @NotNull String fontPath) {
        try {
            return PDType0Font.load(document, DocumentUtils.class.getClassLoader().getResourceAsStream(fontPath));
        } catch (Exception e) {
            log.error("Error while loading font {}", fontPath, e);
            throw new RuntimeException(e);
        }
    }


}
