/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.crypto.utils;

import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.xades.DSSXMLUtils;
import eu.europa.esig.dss.xades.XAdESSignatureParameters;
import eu.europa.esig.dss.xml.utils.XMLCanonicalizer;
import org.bouncycastle.util.encoders.Base64;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Date;

import static coop.libriciel.crypto.utils.StringsUtils.EMPTY;
import static eu.europa.esig.dss.enumerations.DigestAlgorithm.SHA256;
import static eu.europa.esig.dss.enumerations.SignatureAlgorithm.RSA_SHA256;
import static javax.xml.crypto.dsig.CanonicalizationMethod.EXCLUSIVE;
import static javax.xml.crypto.dsig.Transform.ENVELOPED;


/**
 * Refactor of XADES111SignUtil for v1.2.2
 */
@Component
@Deprecated
public class PesV2Sha256Utils {

    private static final String DS_NAMESPACE = "http://www.w3.org/2000/09/xmldsig#";
    private static final String DS_PREFIX = "ds";
    private static final String XAD_NAMESPACE = "http://uri.etsi.org/01903/v1.2.2#";
    private static final String XAD_PREFIX = "xad";
    private static final String XAD_NAMESPACE_SIGNED_PROPERTIES = "http://uri.etsi.org/01903/v1.2.2#SignedProperties";

    private static String sPolicyId;
    private static String sPolicyDigest;
    private static String sSpUri;
    private static String sPolicyDesc;


    public static byte[] generateSignedInfo(DSSDocument toSign, XAdESSignatureParameters signatureParams)
            throws GeneralSecurityException, IOException, XMLStreamException {

        byte[] signedProperties = generateSignedProperties(
                signatureParams.getSigningCertificate().getCertificate(),
                signatureParams.bLevel().getSigningDate(),
                toSign.getName(),
                signatureParams.bLevel().getSignerLocation().getLocality(),
                signatureParams.bLevel().getSignerLocation().getCountry(),
                signatureParams.bLevel().getSignerLocation().getPostalCode(),
                signatureParams.bLevel().getClaimedSignerRoles().get(0));

        return generateSignedInfo(toSign.getName(), Base64.decode(toSign.getDigest(SHA256)), signedProperties);
    }


    public static byte[] signature(DSSDocument toSign, XAdESSignatureParameters params, String signatureBase64)
            throws GeneralSecurityException, IOException, XMLStreamException {

        return signature(
                params.getSigningCertificate().getCertificate(),
                Base64.decode(toSign.getDigest(params.getDigestAlgorithm())),
                params.bLevel().getSigningDate(),
                toSign.getName(),
                params.bLevel().getSignerLocation().getLocality(),
                params.bLevel().getSignerLocation().getCountry(),
                params.bLevel().getSignerLocation().getPostalCode(),
                params.bLevel().getClaimedSignerRoles().get(0),
                signatureBase64
        );
    }


    // <editor-fold desc="Properties">


    @SuppressWarnings({"Duplicates", "squid:S1199", ""})
    static byte[] generateSignedProperties(@NotNull X509Certificate certificate, @NotNull Date date, @NotNull String pesId, @NotNull String city,
                                           @NotNull String country, @NotNull String postalCode, @NotNull String claimedRole)
            throws XMLStreamException, IOException, NoSuchAlgorithmException, CertificateEncodingException {

        String signatureId = pesId + "_SIG_1";
        String signedPropertiesId = signatureId + "_SP";

        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {

            XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
            XMLStreamWriter writer = xmlOutputFactory.createXMLStreamWriter(byteArrayOutputStream);
            XMLSignatureFactory xmlSignatureFactory = XMLSignatureFactory.getInstance();

            writer.writeStartDocument();
            writer.writeStartElement(XAD_PREFIX, "SignedProperties", XAD_NAMESPACE);
            writer.writeNamespace(XAD_PREFIX, XAD_NAMESPACE);
            writer.writeNamespace(DS_PREFIX, DS_NAMESPACE);
            writer.writeAttribute("Id", signedPropertiesId);
            {
                writer.writeStartElement(XAD_PREFIX, "SignedSignatureProperties", XAD_NAMESPACE);
                {
                    writer.writeStartElement(XAD_PREFIX, "SigningTime", XAD_NAMESPACE);
                    writer.writeCharacters(StringsUtils.toIso8601String(date));
                    writer.writeEndElement();

                    writer.writeStartElement(XAD_PREFIX, "SigningCertificate", XAD_NAMESPACE);
                    {
                        writer.writeStartElement(XAD_PREFIX, "Cert", XAD_NAMESPACE);
                        {
                            writer.writeStartElement(XAD_PREFIX, "CertDigest", XAD_NAMESPACE);
                            {
                                writer.writeStartElement(XAD_PREFIX, "DigestMethod", XAD_NAMESPACE);
                                writer.writeAttribute("Algorithm", SHA256.getUri());
                                writer.writeEndElement();

                                writer.writeStartElement(XAD_PREFIX, "DigestValue", XAD_NAMESPACE);
                                MessageDigest messageDigest = MessageDigest.getInstance(SHA256.getJavaName());
                                messageDigest.update(certificate.getEncoded());
                                ByteArrayOutputStream certDigestByteArrayInputStream = new ByteArrayOutputStream();
                                Base64.encode(messageDigest.digest(), certDigestByteArrayInputStream);
                                writer.writeCharacters(certDigestByteArrayInputStream.toString());
                                writer.writeEndElement();
                            }
                            writer.writeEndElement();

                            writer.writeStartElement(XAD_PREFIX, "IssuerSerial", XAD_NAMESPACE);
                            {
                                writer.writeStartElement(DS_PREFIX, "X509IssuerName", DS_NAMESPACE);
                                writer.writeCharacters(certificate.getIssuerX500Principal().getName());
                                writer.writeEndElement();

                                writer.writeStartElement(DS_PREFIX, "X509SerialNumber", DS_NAMESPACE);
                                writer.writeCharacters(certificate.getSerialNumber().toString());
                                writer.writeEndElement();
                            }
                            writer.writeEndElement();
                        }
                        writer.writeEndElement();
                    }
                    writer.writeEndElement();

                    writer.writeStartElement(XAD_PREFIX, "SignaturePolicyIdentifier", XAD_NAMESPACE);
                    {
                        writer.writeStartElement(XAD_PREFIX, "SignaturePolicyId", XAD_NAMESPACE);
                        {
                            writer.writeStartElement(XAD_PREFIX, "SigPolicyId", XAD_NAMESPACE);
                            {
                                writer.writeStartElement(XAD_PREFIX, "Identifier", XAD_NAMESPACE);
                                writer.writeCharacters(sPolicyId);
                                writer.writeEndElement();

                                writer.writeStartElement(XAD_PREFIX, "Description", XAD_NAMESPACE);
                                writer.writeCharacters(sPolicyDesc);
                                writer.writeEndElement();
                            }
                            writer.writeEndElement();

                            writer.writeStartElement(XAD_PREFIX, "SigPolicyHash", XAD_NAMESPACE);
                            {
                                writer.writeStartElement(XAD_PREFIX, "DigestMethod", XAD_NAMESPACE);
                                writer.writeAttribute("Algorithm", SHA256.getUri());
                                writer.writeEndElement();

                                writer.writeStartElement(XAD_PREFIX, "DigestValue", XAD_NAMESPACE);
                                writer.writeCharacters(sPolicyDigest);
                                writer.writeEndElement();
                            }
                            writer.writeEndElement();

                            writer.writeStartElement(XAD_PREFIX, "SigPolicyQualifiers", XAD_NAMESPACE);
                            {
                                writer.writeStartElement(XAD_PREFIX, "SigPolicyQualifier", XAD_NAMESPACE);
                                {
                                    writer.writeStartElement(XAD_PREFIX, "SPURI", XAD_NAMESPACE);
                                    writer.writeCharacters(sSpUri);
                                    writer.writeEndElement();
                                }
                                writer.writeEndElement();
                            }
                            writer.writeEndElement();
                        }
                        writer.writeEndElement();
                    }
                    writer.writeEndElement();

                    writer.writeStartElement(XAD_PREFIX, "SignatureProductionPlace", XAD_NAMESPACE);
                    {
                        writer.writeStartElement(XAD_PREFIX, "City", XAD_NAMESPACE);
                        writer.writeCharacters(city);
                        writer.writeEndElement();

                        writer.writeStartElement(XAD_PREFIX, "PostalCode", XAD_NAMESPACE);
                        writer.writeCharacters(postalCode);
                        writer.writeEndElement();

                        writer.writeStartElement(XAD_PREFIX, "CountryName", XAD_NAMESPACE);
                        writer.writeCharacters(country);
                        writer.writeEndElement();
                    }
                    writer.writeEndElement();

                    writer.writeStartElement(XAD_PREFIX, "SignerRole", XAD_NAMESPACE);
                    {
                        writer.writeStartElement(XAD_PREFIX, "ClaimedRoles", XAD_NAMESPACE);
                        {
                            writer.writeStartElement(XAD_PREFIX, "ClaimedRole", XAD_NAMESPACE);
                            writer.writeCharacters(claimedRole);
                            writer.writeEndElement();
                        }
                        writer.writeEndElement();
                    }
                    writer.writeEndElement();
                }
                writer.writeEndElement();
            }
            writer.writeEndElement();
            writer.writeEndDocument();

            writer.flush();
            writer.close();

            return XMLCanonicalizer.createInstance(EXCLUSIVE).canonicalize(byteArrayOutputStream.toByteArray());
        }
    }


    public static byte[] generateSignedInfo(@NotNull String pesId, @NotNull byte[] digest, @NotNull String publicKeyBase64, @NotNull Date date,
                                            @NotNull String city, @NotNull String country, @NotNull String postalCode, @NotNull String claimedRole)
            throws GeneralSecurityException, XMLStreamException, IOException {

        X509Certificate certificate = StringsUtils.toX509Certificate(publicKeyBase64);
        byte[] signedProperties = generateSignedProperties(certificate, date, pesId, city, country, postalCode, claimedRole);

        return generateSignedInfo(pesId, digest, signedProperties);
    }


    @SuppressWarnings({"Duplicates", "squid:S1199"})
    static byte[] generateSignedInfo(@NotNull String pesId, byte[] digest, byte[] signedProperties)
            throws GeneralSecurityException, XMLStreamException, IOException {

        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {

            XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
            XMLStreamWriter writer = xmlOutputFactory.createXMLStreamWriter(byteArrayOutputStream);

            writer.writeStartDocument();
            writer.writeStartElement(DS_PREFIX, "SignedInfo", DS_NAMESPACE);
            writer.writeNamespace(DS_PREFIX, DS_NAMESPACE);
            {
                writer.writeStartElement(DS_PREFIX, "CanonicalizationMethod", DS_NAMESPACE);
                writer.writeAttribute("Algorithm", EXCLUSIVE);
                writer.writeEndElement();

                writer.writeStartElement(DS_PREFIX, "SignatureMethod", DS_NAMESPACE);
                writer.writeAttribute("Algorithm", RSA_SHA256.getUri());
                writer.writeEndElement();

                writer.writeStartElement(DS_PREFIX, "Reference", DS_NAMESPACE);
                writer.writeAttribute("URI", "#" + pesId);
                {
                    writer.writeStartElement(DS_PREFIX, "Transforms", DS_NAMESPACE);
                    {
                        writer.writeStartElement(DS_PREFIX, "Transform", DS_NAMESPACE);
                        writer.writeAttribute("Algorithm", ENVELOPED);
                        writer.writeEndElement();

                        writer.writeStartElement(DS_PREFIX, "Transform", DS_NAMESPACE);
                        writer.writeAttribute("Algorithm", EXCLUSIVE);
                        writer.writeEndElement();
                    }
                    writer.writeEndElement();

                    writer.writeStartElement(DS_PREFIX, "DigestMethod", DS_NAMESPACE);
                    writer.writeAttribute("Algorithm", SHA256.getUri());
                    writer.writeEndElement();

                    writer.writeStartElement(DS_PREFIX, "DigestValue", DS_NAMESPACE);
                    writer.writeCharacters(Base64.toBase64String(digest));
                    writer.writeEndElement();
                }
                writer.writeEndElement();

                writer.writeStartElement(DS_PREFIX, "Reference", DS_NAMESPACE);
                writer.writeAttribute("Type", XAD_NAMESPACE_SIGNED_PROPERTIES);
                writer.writeAttribute("URI", "#" + pesId + "_SIG_1_SP");
                {
                    writer.writeStartElement(DS_PREFIX, "Transforms", DS_NAMESPACE);
                    {
                        writer.writeStartElement(DS_PREFIX, "Transform", DS_NAMESPACE);
                        writer.writeAttribute("Algorithm", EXCLUSIVE);
                        writer.writeEndElement();
                    }
                    writer.writeEndElement();

                    writer.writeStartElement(DS_PREFIX, "DigestMethod", DS_NAMESPACE);
                    writer.writeAttribute("Algorithm", SHA256.getUri());
                    writer.writeEndElement();

                    writer.writeStartElement(DS_PREFIX, "DigestValue", DS_NAMESPACE);
                    MessageDigest messageDigest = MessageDigest.getInstance(SHA256.getJavaName());
                    messageDigest.update(signedProperties);
                    ByteArrayOutputStream certDigestByteArrayInputStream = new ByteArrayOutputStream();
                    Base64.encode(messageDigest.digest(), certDigestByteArrayInputStream);
                    writer.writeCharacters(certDigestByteArrayInputStream.toString());
                    writer.writeEndElement();
                }
                writer.writeEndElement();
            }
            writer.writeEndElement();
            writer.writeEndDocument();

            writer.flush();
            writer.close();

            return XMLCanonicalizer.createInstance(EXCLUSIVE).canonicalize(byteArrayOutputStream.toByteArray());
        }
    }


    @SuppressWarnings("squid:S1199")
    public static byte[] signature(@NotNull X509Certificate certificate, @NotNull byte[] digest, @NotNull Date date, @NotNull String pesId, @NotNull String city,
                                   @NotNull String country, @NotNull String postalCode, @NotNull String claimedRole, @NotNull String signatureBase64)
            throws GeneralSecurityException, XMLStreamException, IOException {

        byte[] signedProperties = generateSignedProperties(certificate, date, pesId, city, country, postalCode, claimedRole);

        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
            XMLStreamWriter writer = xmlOutputFactory.createXMLStreamWriter(byteArrayOutputStream);

            writer.writeStartDocument();
            writer.writeStartElement("DocumentDetachedExternalSignature");
            writer.writeNamespace(DS_PREFIX, DS_NAMESPACE);
            writer.writeNamespace(XAD_PREFIX, XAD_NAMESPACE);
            {
                writer.writeStartElement(DS_PREFIX, "Signature", DS_NAMESPACE);
                writer.writeAttribute("Id", pesId + "_SIG_1");
                {
                    writer.writeCharacters(EMPTY); // This enforces the full creation of the previous <element> into the XmlStreamWriter.
                    writer.flush();
                    byte[] signedInfo = generateSignedInfo(pesId, digest, signedProperties);
                    byteArrayOutputStream.write(signedInfo);
                    byteArrayOutputStream.flush();

                    writer.writeStartElement(DS_PREFIX, "SignatureValue", DS_NAMESPACE);
                    writer.writeAttribute("Id", pesId + "_SIG_1_SV");
                    writer.writeCharacters(signatureBase64);
                    writer.writeEndElement();

                    writer.writeStartElement(DS_PREFIX, "KeyInfo", DS_NAMESPACE);
                    {
                        writer.writeStartElement(DS_PREFIX, "X509Data", DS_NAMESPACE);
                        {
                            writer.writeStartElement(DS_PREFIX, "X509Certificate", DS_NAMESPACE);
                            writer.writeCharacters(new String(Base64.encode(certificate.getEncoded())));
                            writer.writeEndElement();
                        }
                        writer.writeEndElement();
                    }
                    writer.writeEndElement();

                    writer.writeStartElement(DS_PREFIX, "Object", DS_NAMESPACE);
                    {
                        writer.writeStartElement(XAD_PREFIX, "QualifyingProperties", XAD_NAMESPACE);
                        writer.writeAttribute("Target", "#" + pesId + "_SIG_1");
                        {
                            writer.writeCharacters(EMPTY); // This enforces the full creation of the previous <element> into the XmlStreamWriter.
                            writer.flush();
                            byteArrayOutputStream.write(signedProperties);
                            byteArrayOutputStream.flush();
                        }
                        writer.writeEndElement();
                    }
                    writer.writeEndElement();
                }
                writer.writeEndElement();
            }
            writer.writeEndElement();
            writer.writeEndDocument();

            writer.flush();
            writer.close();

            return XMLCanonicalizer.createInstance(EXCLUSIVE).canonicalize(byteArrayOutputStream.toByteArray());
        }
    }


    // </editor-fold desc="Properties">


    @Value("${pesv2_sha256.policy_id}")
    @SuppressWarnings("squid:S2696")
    public void setPolicyId(String policyId) {
        sPolicyId = policyId;
    }


    @Value("${pesv2_sha256.policy_digest}")
    @SuppressWarnings("squid:S2696")
    public void setPolicyDigest(String policyDigest) {
        sPolicyDigest = policyDigest;
    }


    @Value("${pesv2_sha256.spuri}")
    @SuppressWarnings("squid:S2696")
    public void setSpuri(String spuri) {
        sSpUri = spuri;
    }


    @Value("${pesv2_sha256.policy_desc}")
    @SuppressWarnings("squid:S2696")
    public void setPolicyDesc(String policyDesc) {
        sPolicyDesc = policyDesc;
    }

}

