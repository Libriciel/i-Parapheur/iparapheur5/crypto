/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.utils;

import lombok.Data;


public class ApiUtils {


    /**
     * This is only used on error declaration.
     * TODO: Find the SpringBoot inner error message model instead.
     */
    @Data
    public static class ErrorResponse {

        private String timestamp;
        private int status;
        private String error;
        private String message;
        private String path;

    }


    public static final String CODE_200 = "200";
    public static final String CODE_201 = "201";
    public static final String CODE_204 = "204";
    public static final String CODE_400 = "400";
    public static final String CODE_401 = "401";
    public static final String CODE_403 = "403";
    public static final String CODE_404 = "404";
    public static final String CODE_406 = "406";
    public static final String CODE_409 = "409";
    public static final String CODE_500 = "500";
    public static final String CODE_507 = "507";


    /**
     * Validated annotations support translated templates wrapped in curly braces : @Min{value=5, message="{message.xxx}"}
     * Those messages will be properly translated and returned by the SpringBoot engine. Nothing more to do.
     * <p>
     * Yet, wrapping those directly will mark translations as unused.
     * Splitting the braces wrapper and the real "message.xxx", computing those on the fly, will solve everything.
     */
    public static final String JAVAX_MESSAGE_PREFIX = "{";
    public static final String JAVAX_MESSAGE_SUFFIX = "}";


}
