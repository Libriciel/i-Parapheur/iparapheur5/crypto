/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.utils;

import coop.libriciel.crypto.models.stamp.PdfSignatureStamp;
import coop.libriciel.crypto.models.stamp.PdfSignatureStampElement;
import eu.europa.esig.dss.enumerations.SignatureAlgorithm;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.lang.Float.MAX_VALUE;
import static org.bouncycastle.jce.provider.BouncyCastleProvider.PROVIDER_NAME;


@Log4j2
public class StringsUtils extends org.apache.commons.lang3.StringUtils {


    public static final String MESSAGE_BUNDLE = "messages";

    private static final String DATE_FORMAT_ISO8601 = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    private static final String TIMEZONE_UTC = "UTC";
    public static final String PKCS7 = "PKCS7";
    static final String CERTIFICATE_TYPE_X509 = "X.509";


    public static @NotNull String toIso8601String(@NotNull Date date) {
        DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_ISO8601);
        dateFormat.setTimeZone(TimeZone.getTimeZone(TIMEZONE_UTC));
        return dateFormat.format(date);
    }


    public static @NotNull X509Certificate toX509Certificate(@NotNull String publicKeyBase64)
            throws GeneralSecurityException, IOException {

        byte[] publicKeyDer = java.util.Base64.getDecoder().decode(publicKeyBase64);
        try (ByteArrayInputStream pubKeyInputStream = new ByteArrayInputStream(publicKeyDer)) {
            CertificateFactory certFactory = CertificateFactory.getInstance(CERTIFICATE_TYPE_X509, PROVIDER_NAME);
            return (X509Certificate) certFactory.generateCertificate(pubKeyInputStream);
        }
    }


    public static Optional<PublicKey> parsePublicKey(String publicKey) {
        try {
            InputStream certStream = new ByteArrayInputStream(Base64.getDecoder().decode(publicKey));
            Certificate cert = CertificateFactory.getInstance("X.509").generateCertificate(certStream);
            PublicKey key = cert.getPublicKey();
            return Optional.of(key);
        } catch (GeneralSecurityException e) {
            log.error("Error on public key parse", e);
            return Optional.empty();
        }
    }


    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean verify(String dataToSignBase64, @Nullable String signatureBase64, @Nullable PublicKey publicKey,
                                 @NotNull SignatureAlgorithm signatureAlgorithm) {

        if (StringUtils.isAnyEmpty(dataToSignBase64, signatureBase64)) {
            return false;
        }

        try {
            Signature publicSignature = Signature.getInstance(signatureAlgorithm.getJCEId());
            publicSignature.initVerify(publicKey);
            publicSignature.update(Base64.getDecoder().decode(dataToSignBase64));

            byte[] signatureBytes = Base64.getDecoder().decode(signatureBase64);
            return publicSignature.verify(signatureBytes);
        } catch (GeneralSecurityException e) {
            log.error("Error on signature verification", e);
            return false;
        }
    }


    public static @Nullable String escapeMessageFormatSpecialChars(@Nullable String message) {

        if (message == null) {
            return null;
        }

        return message.replaceAll("'", "''");
    }


    /**
     * Calculate optimal font size, based on Font characters max height and width and element dimensions
     *
     * @param signatureStamp The general stamp, to get stamp width and height
     * @param element        The element to analyze, we use Font, fontSize, and dimensions
     * @param font           A loaded PDFont
     * @param lines          Text to show, split to 'lines' based on char '\n'
     * @return The best fontSize for the dimension and Font
     */
    public static float calculateOptimalFontSize(PdfSignatureStamp signatureStamp, PdfSignatureStampElement element, PDFont font, List<String> lines) throws IOException {

        // Some inner elements may overlap the global boundaries.
        // We want the smaller ones.

        double elementX = Optional.ofNullable(element.getX()).orElse(0F);
        double elementY = Optional.ofNullable(element.getY()).orElse(0F);
        double elementWidth = Optional.ofNullable(element.getWidth()).orElse(MAX_VALUE);
        double elementHeight = Optional.ofNullable(element.getHeight()).orElse(MAX_VALUE);

        double containerWidth = Math.min(signatureStamp.getWidth() - elementX, elementWidth);
        double containerHeight = Math.min(signatureStamp.getHeight() - elementY, elementHeight);

        // Compute width

        double maxTextWidthSize = -1;
        for (String line : lines) {
            maxTextWidthSize = Math.max(font.getStringWidth(line) / 1000, maxTextWidthSize);
        }
        double maxWidthFontSize = containerWidth / maxTextWidthSize;

        // Compute height
        // Note : This is not a very precise formula, and results may upon the used font. Yet, computing a String height is incredibly tricky.
        // This is not pixel-perfect like the width-one, but this will do, for the limited amount of cases that we'll encounter.

        double linesHeightSize = font.getFontDescriptor().getCapHeight() / 1000 * lines.size();
        double interlinesHeightSize = (lines.size() - 1) * 0.7F;
        double maxTextHeightSize = linesHeightSize + interlinesHeightSize;
        double maxHeightFontSize = containerHeight / maxTextHeightSize;

        // Result

        log.debug("calculateOptimalFontSize...");
        log.debug("    containerWidth:{} / maxTextWidthSize:{} = maxWidthFontSize:{}", containerWidth, maxTextWidthSize, maxWidthFontSize);
        log.debug("    containerHeight:{} / maxTextHeightSize:{} = maxHeightFontSize:{}", containerHeight, maxTextHeightSize, maxHeightFontSize);

        double maxFontSize = Math.min(maxWidthFontSize, maxHeightFontSize);

        if ((element.getFontSize() == null) || (element.getFontSize() == 0)) {
            return (float) maxFontSize;
        } else if (element.getFontSize() > maxFontSize) {
            log.info("The given font size ({}) is too large for the container. Using a computed one instead.", element.getFontSize());
            return (float) maxFontSize;
        } else {
            return element.getFontSize();
        }
    }


    /**
     * Auto-jump lines for long Strings in specified width
     *
     * @param lines   Text to show, split to 'lines' based on char '\n'
     * @param element The element to analyze, we use Font, fontSize, and dimensions
     * @param font    A loaded PDFont
     * @return The best fontSize for the dimension and Font
     */
    public static List<String> splitOverflowingLines(PdfSignatureStamp signatureStamp, PdfSignatureStampElement element, PDFont font, List<String> lines) throws IOException {

        // Some inner elements may overlap the global boundaries.
        // We want the smaller ones.

        double elementX = Optional.ofNullable(element.getX()).orElse(0F);
        double elementWidth = Optional.ofNullable(element.getWidth()).orElse(MAX_VALUE);
        double maxWidth = Math.min(signatureStamp.getWidth() - elementX, elementWidth);

        // Compute width

        ArrayList<String> result = new ArrayList<>();
        for (String line : lines) {
            result.addAll(splitOverflowingLine(line, (float) maxWidth, font, element.getFontSize()));
        }

        return result;
    }


    /**
     * Auto-jump lines for long Strings in specified width
     *
     * @param text     The text to handle
     * @param width    The maximum width to put the text in
     * @param font     The font we use for displaying "text"
     * @param fontSize The size of the font
     * @return String array of wrapped 'text' inside 'width' dimension
     * @throws IOException Thrown if we can not have line width with current Font
     */
    public static List<String> splitOverflowingLine(String text, float width, PDFont font, float fontSize) throws IOException {
        List<String> lines = new ArrayList<>();
        int lastSpace = -1;
        while (text.length() > 0) {
            int spaceIndex = text.indexOf(' ', lastSpace + 1);
            if (spaceIndex < 0)
                spaceIndex = text.length();
            String subString = text.substring(0, spaceIndex);

            float size = fontSize * font.getStringWidth(subString) / 1000;

            if (size > width) {
                if (lastSpace < 0) {
                    lastSpace = spaceIndex;
                }
                subString = text.substring(0, lastSpace);
                lines.add(subString);
                text = text.substring(lastSpace).trim();
                lastSpace = -1;
            } else if (spaceIndex == text.length()) {
                lines.add(text);
                text = "";
            } else {
                lastSpace = spaceIndex;
            }
        }
        return lines;
    }


}
