/*
 * DSS - Digital Signature Services
 * Copyright (C) 2015 European Commission, provided under the CEF programme
 * Modified work - Copyright (C) Libriciel SCOP
 *
 * This file is part of the "DSS - Digital Signature Services" project.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package coop.libriciel.crypto.utils.dss;

import coop.libriciel.crypto.models.dss.IStreamableDssDocument;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.DSSException;
import eu.europa.esig.dss.model.InMemoryDocument;
import eu.europa.esig.dss.pades.PAdESCommonParameters;
import eu.europa.esig.dss.pdf.PDFServiceMode;
import eu.europa.esig.dss.pdf.pdfbox.PdfBoxDocumentReader;
import eu.europa.esig.dss.pdf.pdfbox.PdfBoxSignatureService;
import eu.europa.esig.dss.pdf.pdfbox.visible.PdfBoxSignatureDrawerFactory;
import lombok.extern.log4j.Log4j2;
import org.apache.pdfbox.pdmodel.PDDocument;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;


@Log4j2
public class CustomPdfBoxSignatureService extends PdfBoxSignatureService {

    public CustomPdfBoxSignatureService(PDFServiceMode serviceMode, PdfBoxSignatureDrawerFactory signatureDrawerFactory) {
        super(serviceMode, signatureDrawerFactory);
    }


    @Override
    public DSSDocument sign(final DSSDocument toSignDocument, final byte[] signatureValue, final PAdESCommonParameters parameters) {

        // Shortcut, since we may already have the outputStream...
        return Optional.of(toSignDocument)
                .filter(d -> d instanceof IStreamableDssDocument)
                .map(IStreamableDssDocument.class::cast)
                .filter(d -> d.getOutputStream() != null)
                .map(d -> {
                    log.debug("Shortcut sign called");
                    try (OutputStream baos = d.getOutputStream()) {
                        DSSDocument document = (DSSDocument) d;
                        PdfBoxDocumentReader documentReader = new PdfBoxDocumentReader(document, Arrays.toString(parameters.getPasswordProtection()));
                        return signDocument(parameters, signatureValue, baos, documentReader);
                    } catch (IOException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                        throw new DSSException(e);
                    }
                })
                .orElseGet(() -> {
                    log.debug("Super sign called");
                    return super.sign(toSignDocument, signatureValue, parameters);
                });
    }


    /**
     * This is an "public" method of the private {@link #signDocumentAndReturnDigest(PAdESCommonParameters, byte[], OutputStream, PDDocument)} one.
     * <p>
     * Reflective call is kind of ugly, yes.
     * But we really want to avoid the private's method `new InmemoryDocument(baos.getBytes())` line.
     * <p>
     * TODO : Check in SD-DSS v5.8+, if there is anything protected/public, that can be properly done.
     */
    @SuppressWarnings("JavadocReference")
    private DSSDocument signDocument(final PAdESCommonParameters parameters, final byte[] signatureBytes, final OutputStream fileOutputStream,
                                     final PdfBoxDocumentReader pdfDocumentReader) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        Method method = getClass()
                .getSuperclass()
                .getDeclaredMethod("signDocumentAndReturnDigest", PAdESCommonParameters.class, byte[].class, OutputStream.class, PdfBoxDocumentReader.class);

        method.setAccessible(true);
        method.invoke(this, parameters, signatureBytes, fileOutputStream, pdfDocumentReader);

        return new InMemoryDocument();
    }

}
