/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.utils.dss;

import coop.libriciel.crypto.models.stamp.PdfSignatureStamp;
import coop.libriciel.crypto.models.stamp.PdfSignatureStampElement;
import coop.libriciel.crypto.utils.DocumentUtils;
import eu.europa.esig.dss.pades.DSSFileFont;
import eu.europa.esig.dss.pdf.AnnotationBox;
import eu.europa.esig.dss.pdf.AnnotationBox;
import eu.europa.esig.dss.pdf.pdfbox.visible.AbstractPdfBoxSignatureDrawer;
import eu.europa.esig.dss.pdf.pdfbox.visible.nativedrawer.PdfBoxDSSFontMetrics;
import eu.europa.esig.dss.pdf.visible.DSSFontMetrics;
import eu.europa.esig.dss.pdf.visible.SignatureFieldDimensionAndPosition;
import eu.europa.esig.dss.pdf.visible.SignatureFieldDimensionAndPositionBuilder;
import eu.europa.esig.dss.pdf.visible.SignatureFieldDimensionAndPosition;
import eu.europa.esig.dss.pdf.visible.SignatureFieldDimensionAndPositionBuilder;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAppearanceDictionary;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAppearanceStream;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDSignatureField;
import org.apache.pdfbox.util.Matrix;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import static java.lang.Float.MAX_VALUE;
import static coop.libriciel.crypto.utils.StringsUtils.calculateOptimalFontSize;
import static coop.libriciel.crypto.utils.StringsUtils.splitOverflowingLines;
import static org.apache.pdfbox.cos.COSName.NEED_APPEARANCES;


@Log4j2
public class LibricielPdfBoxSignatureDrawer extends AbstractPdfBoxSignatureDrawer {


    /**
     * Ratio used to compute the space between the line. That's the "leaning".
     */
    private static final float LINE_LEANING_RATIO = 1.3F;


    @Override
    protected String getExpectedColorSpaceName() {
        return null;
    }


    @Override
    public void draw() throws IOException {

        int updatedPage = DocumentUtils.computePdfPageNumber(document.getNumberOfPages(), parameters.getFieldParameters().getPage());
        parameters.getFieldParameters().setPage(updatedPage);

        PDPage page = document.getPage(parameters.getFieldParameters().getPage() - 1);
        PDRectangle mediaBox = page.getMediaBox();
        AnnotationBox pageBox = new AnnotationBox(mediaBox.getLowerLeftX(), mediaBox.getLowerLeftY(), mediaBox.getUpperRightX(), mediaBox.getUpperRightY());
        int rotation = page.getRotation();

        // DSS-747. Using the DPI resolution to convert java size to dot
        SignatureFieldDimensionAndPosition signFieldDimensionAndPosition =
                new SignatureFieldDimensionAndPositionBuilder(parameters, getDSSFontMetrics(), pageBox, rotation).build();

        Rectangle2D.Float originRect = new Rectangle2D.Float(
                signFieldDimensionAndPosition.getBoxX(),
                signFieldDimensionAndPosition.getBoxY(),
                parameters.getFieldParameters().getWidth(),
                parameters.getFieldParameters().getHeight()
        );

        PDRectangle rect = createSignatureRectangle(page, originRect);

        try (InputStream signatureInputStream = createVisualSignatureTemplate(document, rect)) {
            signatureOptions.setVisualSignature(signatureInputStream);
            signatureOptions.setPage(parameters.getFieldParameters().getPage() - 1); // DSS-1138
        }
    }


    void removeNeedAppearancesFlag() {
        try {
            PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();
            if ((acroForm != null) && acroForm.getNeedAppearances()) {
                acroForm.getCOSObject().removeItem(NEED_APPEARANCES);
                acroForm.refreshAppearances();
            }
        } catch (IOException e) {
            log.error("Cannot remove need appearances flag : " + e.getLocalizedMessage(), e);
        }
    }


    /**
     * This method changes the signature position within the page.
     * Note : it doesn't flip the signature itself, it just changes its position in the page.
     *
     * @link <a href="https://github.com/apache/pdfbox/blob/trunk/examples/src/main/java/org/apache/pdfbox/examples/signature/CreateVisibleSignature2.java">source</a>
     */
    private PDRectangle createSignatureRectangle(PDPage page, Rectangle2D.Float originRect) {

        float x = (float) originRect.getX();
        float y = (float) originRect.getY();
        float width = (float) originRect.getWidth();
        float height = (float) originRect.getHeight();
        PDRectangle cropBox = page.getCropBox();
        PDRectangle rect = new PDRectangle();

        // Signing should be at the same position regardless of page rotation.

        log.debug("    Rotation:{}", page.getRotation());
        log.debug("    Mediabox:{}", page.getMediaBox().toString());
        log.debug("    Cropbox :{}", page.getCropBox().toString());

        switch (page.getRotation()) {
            case 90 -> {
                rect.setLowerLeftX(cropBox.getLowerLeftX() + cropBox.getWidth() - y - height);
                rect.setLowerLeftY(cropBox.getLowerLeftY() + x);
                rect.setUpperRightX(cropBox.getLowerLeftX() + cropBox.getWidth() - y);
                rect.setUpperRightY(cropBox.getLowerLeftY() + x + width);
            }
            case 180 -> {
                rect.setLowerLeftX(cropBox.getLowerLeftX() + cropBox.getWidth() - x - width);
                rect.setLowerLeftY(cropBox.getLowerLeftY() + cropBox.getHeight() - y - height);
                rect.setUpperRightX(cropBox.getLowerLeftX() + cropBox.getWidth() - x);
                rect.setUpperRightY(cropBox.getLowerLeftY() + cropBox.getHeight() - y);
            }
            case 270 -> {
                rect.setLowerLeftX(cropBox.getLowerLeftX() + y);
                rect.setLowerLeftY(cropBox.getLowerLeftY() + cropBox.getHeight() - x - width);
                rect.setUpperRightX(cropBox.getLowerLeftX() + y + height);
                rect.setUpperRightY(cropBox.getLowerLeftY() + cropBox.getHeight() - x);
            }
            default -> {
                rect.setLowerLeftX(cropBox.getLowerLeftX() + x);
                rect.setLowerLeftY(cropBox.getLowerLeftY() + y);
                rect.setUpperRightX(cropBox.getLowerLeftX() + x + width);
                rect.setUpperRightY(cropBox.getLowerLeftY() + y + height);
            }
        }

        return rect;
    }


    /**
     * Create a template PDF document with empty signature and return it as a stream.
     * Taken and modified from <a href="https://issues.apache.org/jira/browse/PDFBOX-3198">here</a>
     * (under Apache Licence, AGPL3+ compatible)
     */
    @SuppressWarnings("squid:S1301")
    private InputStream createVisualSignatureTemplate(PDDocument srcDoc, PDRectangle rect) throws IOException {
        PdfSignatureStamp signatureStamp = ((SignatureImageConfigParameters) parameters).getPdfSignatureStamp();

        try (PDDocument doc = new PDDocument();
             ByteArrayOutputStream baos = new ByteArrayOutputStream()) {

            PDPage oldPage = srcDoc.getPage(parameters.getFieldParameters().getPage() - 1);
            PDPage page = new PDPage(oldPage.getMediaBox());
            page.setRotation(oldPage.getRotation());

            doc.addPage(page);
            PDAcroForm acroForm = new PDAcroForm(doc);
            doc.getDocumentCatalog().setAcroForm(acroForm);
            PDSignatureField signatureField = new PDSignatureField(acroForm);
            PDAnnotationWidget widget = signatureField.getWidgets().get(0);
            List<PDField> acroFormFields = acroForm.getFields();
            acroForm.setSignaturesExist(true);
            acroForm.setAppendOnly(true);
            acroForm.getCOSObject().setDirect(true);
            acroFormFields.add(signatureField);

            widget.setRectangle(rect);

            // from PDVisualSigBuilder.createHolderForm()
            PDStream stream = new PDStream(doc);
            PDFormXObject form = new PDFormXObject(stream);
            PDResources res = new PDResources();
            form.setResources(res);
            form.setFormType(1);
            PDRectangle bbox = new PDRectangle(rect.getWidth(), rect.getHeight());
            float height = bbox.getHeight();
            Matrix initialScale = null;

            // This section flips the signature to match the page rotation.
            // It doesn't change the signature position itself,
            // it just flips the canvas itself clockwise/counter-clockwise.
            switch (page.getRotation()) {
                case 90 -> {
                    form.setMatrix(AffineTransform.getQuadrantRotateInstance(1));
                    initialScale = Matrix.getScaleInstance(bbox.getWidth() / bbox.getHeight(), bbox.getHeight() / bbox.getWidth());
                    height = bbox.getWidth();
                }
                case 180 -> form.setMatrix(AffineTransform.getQuadrantRotateInstance(2));
                case 270 -> {
                    form.setMatrix(AffineTransform.getQuadrantRotateInstance(3));
                    initialScale = Matrix.getScaleInstance(bbox.getWidth() / bbox.getHeight(), bbox.getHeight() / bbox.getWidth());
                    height = bbox.getWidth();
                }

            }

            form.setBBox(bbox);

            // from PDVisualSigBuilder.createAppearanceDictionary()
            PDAppearanceDictionary appearance = new PDAppearanceDictionary();
            appearance.getCOSObject().setDirect(true);
            PDAppearanceStream appearanceStream = new PDAppearanceStream(form.getCOSObject());
            appearance.setNormalAppearance(appearanceStream);
            widget.setAppearance(appearance);

            try (PDPageContentStream cs = new PDPageContentStream(doc, appearanceStream)) {

                // for 90° and 270° scale ratio of width / height
                // not really sure about this
                // why does scale have no effect when done in the form matrix???
                boolean isInitialScaleValid = initialScale != null;
                boolean isTargetScaleValid = (rect.getWidth() != 0) && (rect.getHeight() != 0);
                if (isInitialScaleValid && isTargetScaleValid) {
                    cs.transform(initialScale);
                }

                for (PdfSignatureStampElement element : signatureStamp.getElements()) {
                    switch (element.getType()) {
                        case IMAGE -> handleImageElement(doc, height, cs, element);
                        case TEXT -> handleTextElement(signatureStamp, height, cs, element);
                    }
                }
            }

            // No need to set annotations and /P entry

            doc.save(baos);
            return new ByteArrayInputStream(baos.toByteArray());
        }
    }


    private void handleTextElement(PdfSignatureStamp signatureStamp, float height, PDPageContentStream cs, PdfSignatureStampElement element)
            throws IOException {

        String textToWrite = element.getValue();
        for (Map.Entry<String, String> metadata : signatureStamp.getMetadata().entrySet()) {
            textToWrite = textToWrite.replaceAll("%" + metadata.getKey() + "%", metadata.getValue());
        }

        PDFont font = element.getFont().getPdFontLoader().apply(document);
        float fontSize = element.getFontSize();

        List<String> lines = Arrays.stream(StringUtils.split(textToWrite, "\n"))
                .map(line -> filteringIncompatibleChars(line, font))
                .toList();

        if (fontSize == 0) {
            fontSize = calculateOptimalFontSize(signatureStamp, element, font, lines);
        }

        lines = splitOverflowingLines(signatureStamp, element, font, lines);

        // Writing text
        cs.beginText();
        cs.setFont(font, fontSize);
        cs.setNonStrokingColor(Color.decode(element.getColorCode()));
        // We have to subtract line height as well, or the first line will not be visible
        cs.newLineAtOffset(element.getX(), height - element.getY() - (font.getFontDescriptor().getCapHeight() / 1000 * fontSize));
        cs.setLeading(fontSize * LINE_LEANING_RATIO);
        for (String line : lines) {
            cs.showText(line);
            cs.newLine();
        }
        cs.endText();
    }


    private void handleImageElement(PDDocument doc, float height, PDPageContentStream cs, PdfSignatureStampElement element) throws IOException {

        byte[] imageBytes = Base64.getDecoder().decode(element.getValue());
        PDImageXObject img = PDImageXObject.createFromByteArray(doc, imageBytes, null);

        // Computing scale with width/height given

        float scale = 1;
        if ((element.getWidth() != null) && (element.getHeight() != null)) {
            scale = Math.min(element.getWidth() / img.getWidth(), element.getHeight() / img.getHeight());
        }

        float scaledX = element.getX() / scale;
        float scaledY = (height - element.getY() - img.getHeight() * scale) / scale;
        float scaledWidth = img.getWidth() * scale;
        float scaledHeight = img.getHeight() * scale;

        float marginLeft = (element.getWidth() - scaledWidth) / 2;
        float marginBottom = (element.getHeight() - scaledHeight) / 2;
        float unscaledMarginLeft = marginLeft / scale;
        float unscaledMarginBottom = marginBottom / scale;

        log.trace("    image width:{} height:{}", img.getWidth(), img.getHeight());
        log.trace("    element width:{} height:{}", element.getWidth(), element.getHeight());
        log.trace("    scale:{}", scale);
        log.trace("    scaledX:{} scaledY:{}", scaledX, scaledY);
        log.trace("    scaledWidth:{} scaledHeight:{}", scaledWidth, scaledHeight);
        log.trace("    marginLeft:{} marginBottom:{}", marginLeft, marginBottom);
        log.trace("    unscaledMarginLeft:{} unscaledMarginBottom:{}", unscaledMarginLeft, unscaledMarginBottom);

        cs.saveGraphicsState();
        cs.transform(Matrix.getScaleInstance(scale, scale));
        cs.drawImage(img, scaledX + unscaledMarginLeft, scaledY - unscaledMarginBottom);
        cs.restoreGraphicsState();
    }


    static String filteringIncompatibleChars(String string, PDFont font) {
        String result = string;

        for (String letter : string.split("")) {
            try {
                font.getStringWidth(letter);
            } catch (IllegalArgumentException | IOException e) {
                log.error("Incompatible font:{} letter:'{}'", font.getName(), letter, e);
                result = result.replaceAll(letter, "");
            }
        }

        return result;
    }


    @Override
    public SignatureFieldDimensionAndPosition buildSignatureFieldBox() {
        // We won't return anything, since we don't want the lib to perform verifications (yet).
        // The SD-DSS is picky on signature and form fields, and throw some (weird) exception.
        // Since we actually want to keep the previous behavior, we'll send null back
        return null;

        // TODO: Fix everything, and enjoy the SD-DSS checking ?
        //  int pageNumber = DocumentUtils.computePdfPageNumber(document.getNumberOfPages(), parameters.getFieldParameters().getPage());
        //  parameters.getFieldParameters().setPage(pageNumber);
        //  return () -> new AnnotationBox(parameters.getFieldParameters());
    }


    @Override
    protected DSSFontMetrics getDSSFontMetrics() {
        DSSFileFont dssFont = DSSFileFont.initializeDefault();
        try (InputStream fontInputStream = dssFont.getInputStream()) {
            PDFont font = PDType0Font.load(document, fontInputStream);
            return new PdfBoxDSSFontMetrics(font);
        } catch (IOException e) {
            log.error("Unable to load font", e);
            throw new RuntimeException(e);
        }
    }


}
