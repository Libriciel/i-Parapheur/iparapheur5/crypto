/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.crypto.utils.dss;

import coop.libriciel.crypto.models.stamp.PdfSignatureStamp;
import eu.europa.esig.dss.pades.SignatureImageParameters;
import lombok.Getter;
import lombok.Setter;

/**
 * Overridden method, to add some PDF signature stamp data,
 * all along the PdfBoxSignatureDrawerFactory engine,
 * and retrieve it easily in the final draw() method.
 */
@Setter
@Getter
public class SignatureImageConfigParameters extends SignatureImageParameters {

    private PdfSignatureStamp pdfSignatureStamp;

}
