/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.controller.legacy;

import coop.libriciel.crypto.models.DataToSign;
import coop.libriciel.crypto.models.request.CadesParameters;
import coop.libriciel.crypto.models.request.legacy.LegacyDataToSignHolder;
import coop.libriciel.crypto.models.request.legacy.LegacySignatureParameters;
import coop.libriciel.crypto.models.request.legacy.SignatureHolder;
import coop.libriciel.crypto.services.CryptoService;
import coop.libriciel.crypto.services.impl.CadesCryptoService;
import coop.libriciel.crypto.services.impl.PesV2CryptoService;
import coop.libriciel.crypto.utils.PesV2XAdESSignatureParameters;
import eu.europa.esig.dss.cades.CAdESSignatureParameters;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static coop.libriciel.crypto.MainApplication.API_ROOT_PATH;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@Log4j2
@Deprecated
@RestController
@RequestMapping(API_ROOT_PATH)
public class SignatureController {


    // <editor-fold desc="Beans">


    private final CadesCryptoService cadesCryptoService;
    private final PesV2CryptoService pesV2CryptoService;


    @Autowired
    public SignatureController(CadesCryptoService cadesCryptoService, PesV2CryptoService pesV2CryptoService) {
        this.cadesCryptoService = cadesCryptoService;
        this.pesV2CryptoService = pesV2CryptoService;
    }


    // </editor-fold desc="Beans">


    @PostMapping(value = "generateDataToSign", consumes = APPLICATION_JSON_VALUE)
    public LegacyDataToSignHolder generateDataToSign(@RequestBody CadesParameters parameters) {

        parameters.setSignatureDateTime(ObjectUtils.firstNonNull(parameters.getSignatureDateTime(), new Date().getTime()));
        AtomicLong signatureTime = new AtomicLong();

        List<DataToSign> dataToSignList = parameters.getDataToSignList()
                .stream()
                .map(d -> CryptoService.generateDigestDocument(d, parameters.getDigestAlgorithm().getDssValue()))
                .map(d -> {
                    if (StringUtils.equals(parameters.getSignatureFormat(), "xades-env-1.2.2-sha256")) {
                        PesV2XAdESSignatureParameters params = pesV2CryptoService.getSignatureParameters(parameters);
                        signatureTime.set(params.bLevel().getSigningDate().getTime());
                        return pesV2CryptoService.getDataToSign(d, params);
                    } else {
                        CAdESSignatureParameters params = cadesCryptoService.getSignatureParameters(parameters);
                        signatureTime.set(params.bLevel().getSigningDate().getTime());
                        return cadesCryptoService.getDataToSign(d, params);
                    }
                })
                .toList();

        // Result

        return new LegacyDataToSignHolder(
                dataToSignList.stream().map(DataToSign::getDataToSignBase64).toList(),
                signatureTime.get(),
                dataToSignList.stream().map(DataToSign::getDigestBase64).toList(),
                parameters.getPayload()
        );
    }


    @PostMapping(value = "generateSignature", consumes = APPLICATION_JSON_VALUE)
    public SignatureHolder generateSignature(@RequestBody LegacySignatureParameters parameters) {

        if (parameters.getSignatureDateTime() == null) {
            throw new ResponseStatusException(BAD_REQUEST, "Signature time must be set");
        }

        List<String> signatureList = parameters.getDataToSignList()
                .stream()
                .map(d -> Pair.of(
                        CryptoService.generateDigestDocument(d, parameters.getDigestAlgorithm().getDssValue()),
                        d.getSignatureValue())
                )
                .map(p -> {
                    if (StringUtils.equals(parameters.getSignatureFormat(), "xades-env-1.2.2-sha256")) {
                        PesV2XAdESSignatureParameters params = pesV2CryptoService.getSignatureParameters(parameters);
                        return pesV2CryptoService.getSignature(params, p.getLeft(), p.getRight());
                    } else {
                        CAdESSignatureParameters params = cadesCryptoService.getSignatureParameters(parameters);
                        String rawSignature = cadesCryptoService.getSignature(params, p.getLeft(), p.getRight());
                        return Base64.getEncoder().encodeToString(rawSignature.getBytes(UTF_8));
                    }
                })
                .toList();

        return new SignatureHolder(
                signatureList,
                parameters.getPayload()
        );
    }


}
