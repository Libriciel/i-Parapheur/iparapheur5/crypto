/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.controller.legacy;

import coop.libriciel.crypto.models.DataToSign;
import coop.libriciel.crypto.models.request.CadesParameters;
import coop.libriciel.crypto.models.request.legacy.LegacyDataToSignHolder;
import coop.libriciel.crypto.models.request.legacy.LegacySignatureParameters;
import coop.libriciel.crypto.models.request.legacy.SignatureHolder;
import coop.libriciel.crypto.services.CryptoService;
import coop.libriciel.crypto.services.impl.PesV2CryptoService;
import coop.libriciel.crypto.utils.PesV2XAdESSignatureParameters;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.DigestDocument;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static coop.libriciel.crypto.MainApplication.API_ROOT_PATH;
import static eu.europa.esig.dss.enumerations.DigestAlgorithm.SHA256;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;


/**
 * Controller for PESv2 Signature.
 * The parameter is the dataToSign object type
 */
@Log4j2
@Deprecated
@RestController
@RequestMapping(API_ROOT_PATH)
public class PesV2CryptoController {


    // <editor-fold desc="Beans">


    private final PesV2CryptoService pesV2CryptoService;


    @Autowired
    public PesV2CryptoController(PesV2CryptoService pesV2CryptoService) {
        this.pesV2CryptoService = pesV2CryptoService;
    }


    // </editor-fold desc="Beans">


    @PostMapping(value = {"pesv2/generateDataToSign", "xades/generateDataToSign"}, consumes = MULTIPART_FORM_DATA_VALUE)
    public LegacyDataToSignHolder getDataToSignV1(@RequestParam CadesParameters model) {

        model.setSignatureDateTime(ObjectUtils.firstNonNull(model.getSignatureDateTime(), new Date().getTime()));
        PesV2XAdESSignatureParameters params = pesV2CryptoService.getSignatureParameters(model);

        List<DataToSign> dataToSignList = model.getDataToSignList().stream()
                .map(d -> CryptoService.generateDigestDocument(d, SHA256))
                .map(d -> pesV2CryptoService.getDataToSign(d, params))
                .toList();

        // Result

        return new LegacyDataToSignHolder(
                dataToSignList.stream().map(DataToSign::getDataToSignBase64).collect(toList()),
                model.getSignatureDateTime(),
                dataToSignList.stream().map(DataToSign::getDigestBase64).collect(toList()),
                model.getPayload()
        );
    }


    @PostMapping(value = {"pesv2/generateSignature", "xades/generateSignature"}, consumes = MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<SignatureHolder> getSignatureV1(@RequestParam LegacySignatureParameters model) {

        AtomicInteger index = new AtomicInteger(0);
        List<String> signatureList = model.getDataToSignList()
                .stream()
                .map(d -> Pair.of(
                        (DSSDocument) new DigestDocument(SHA256, d.getDigestBase64(), d.getId()),
                        Optional.ofNullable(d.getSignatureValue())
                                .or(() -> Optional.ofNullable(CollectionUtils.get(model.getSignatureBase64List(), index.getAndIncrement())))
                                .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, "Missing signature"))
                ))
                .map(p -> pesV2CryptoService.getSignature(pesV2CryptoService.getSignatureParameters(model), p.getLeft(), p.getRight()))
                .toList();

        return new ResponseEntity<>(new SignatureHolder(signatureList, model.getPayload()), OK);
    }


}
