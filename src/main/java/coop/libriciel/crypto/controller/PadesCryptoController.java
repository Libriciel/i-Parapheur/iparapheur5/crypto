/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.controller;

import coop.libriciel.crypto.models.DataToSign;
import coop.libriciel.crypto.models.dss.StreamableDssDocument;
import coop.libriciel.crypto.models.request.DataToSignHolder;
import coop.libriciel.crypto.models.request.PadesParameters;
import coop.libriciel.crypto.models.request.legacy.LegacyDataToSignHolder;
import coop.libriciel.crypto.models.request.legacy.LegacyPadesSignatureParameters;
import coop.libriciel.crypto.services.impl.PadesCryptoService;
import coop.libriciel.crypto.utils.ApiUtils.ErrorResponse;
import coop.libriciel.crypto.utils.StringsUtils;
import eu.europa.esig.dss.enumerations.SignatureAlgorithm;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.pades.PAdESSignatureParameters;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.PublicKey;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import static coop.libriciel.crypto.MainApplication.*;
import static coop.libriciel.crypto.utils.ApiUtils.CODE_200;
import static coop.libriciel.crypto.utils.ApiUtils.CODE_500;
import static eu.europa.esig.dss.enumerations.EncryptionAlgorithm.RSA;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;
import static org.springframework.http.MediaType.*;


/**
 * Controller for Pades Signature.
 * The parameter is the dataToSign object type
 */
@Log4j2
@RestController
@RequestMapping(API_ROOT_PATH)
public class PadesCryptoController {


    // <editor-fold desc="Beans">


    private final PadesCryptoService padesCryptoService;


    @Autowired
    public PadesCryptoController(PadesCryptoService padesCryptoService) {
        this.padesCryptoService = padesCryptoService;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Legacy API">


    @Deprecated
    @PostMapping(value = "pades/generateDataToSign",
            consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_JSON_VALUE)
    public LegacyDataToSignHolder getDataToSignV1(@RequestParam PadesParameters model,
                                                  @RequestPart(value = "file") final MultipartFile multipartFile) throws IOException {

        model.setSignatureDateTime(ObjectUtils.firstNonNull(model.getSignatureDateTime(), new Date().getTime()));
        try (InputStream fileInputStream = multipartFile.getInputStream();
             OutputStream baos = new ByteArrayOutputStream()) {
            DSSDocument dssDocument = new StreamableDssDocument(fileInputStream, baos);
            PAdESSignatureParameters signatureParams = padesCryptoService.getSignatureParameters(model, null);

            // Building result

            DataToSign dataToSign = padesCryptoService.getDataToSign(dssDocument, signatureParams);
            return new LegacyDataToSignHolder(singletonList(dataToSign), signatureParams, model.getPayload());
        }
    }


    @Deprecated
    @PostMapping(value = "pades/generateSignature",
            consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_OCTET_STREAM_VALUE)
    public void getSignatureV1(@RequestParam LegacyPadesSignatureParameters model,
                               @RequestPart(value = "file") final MultipartFile document,
                               @NotNull HttpServletResponse response) throws IOException {

        // Parsing request

        model.setSignatureDateTime(ObjectUtils.firstNonNull(model.getSignatureDateTime(), new Date().getTime()));

        DataToSign documentToSign = Optional.ofNullable(model.getDataToSignList())
                .filter(l -> l.size() < 2)
                .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, "The dataToSign list should contain only one element"))
                .stream()
                .findFirst()
                .orElse(new DataToSign(document.getName(), null, null, null));

        String signature = Optional.ofNullable(documentToSign.getSignatureValue())
                .or(() -> Optional.ofNullable(CollectionUtils.get(model.getSignatureBase64List(), 0)))
                .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, "Missing signature in request"));

        // Building signature

        try (InputStream inputStream = document.getInputStream();
             OutputStream outputStream = response.getOutputStream()) {
            DSSDocument parsedDocument = new StreamableDssDocument(inputStream, outputStream);
            if (StringUtils.isEmpty(documentToSign.getDigestBase64())) {
                log.warn("getSignature called without digest.\n" +
                        "It will be faster, and less RAM-consuming if the getDataToSign's digest is given.");
            }

            PAdESSignatureParameters params = padesCryptoService.getSignatureParameters(model, documentToSign.getDigestBase64());

            response.setContentType(APPLICATION_OCTET_STREAM_VALUE);
            response.setHeader(CONTENT_DISPOSITION, "attachment; filename=" + document.getOriginalFilename());
            padesCryptoService.getSignature(params, parsedDocument, signature);
        }
    }


    @PostMapping(value = API_V2 + "/pades/generateDataToSign",
            consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_JSON_VALUE)
    public DataToSignHolder getDataToSignApiV2(@RequestParam PadesParameters model,
                                               @RequestPart(value = "file") final MultipartFile multipartFile) throws IOException {
        return getPadesDataToSign(model, multipartFile);
    }


    @PostMapping(value = API_V2 + "/pades/generateSignature",
            consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_PDF_VALUE)
    public void getSignatureApiV2(@RequestParam PadesParameters model,
                                  @RequestPart(value = "file") final MultipartFile document,
                                  @NotNull HttpServletResponse response) throws IOException {
        getPadesSignature(model, document, response);
    }


    // </editor-fold desc="Legacy API">


    @PostMapping(value = API_V3 + "/pades/generateDataToSign", consumes = MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "Retrieve the data to sign prior to a PAdES signature")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_500, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public DataToSignHolder getPadesDataToSign(@RequestPart PadesParameters model,
                                               @RequestPart(value = "file") final MultipartFile multipartFile) throws IOException {

        log.info("getPadesDataToSign");
        log.debug("getPadesDataToSign - model : {}", model);

        model.setSignatureDateTime(ObjectUtils.firstNonNull(model.getSignatureDateTime(), new Date().getTime()));
        try (InputStream fileInputStream = multipartFile.getInputStream()) {
            DSSDocument dssDocument = new StreamableDssDocument(fileInputStream, null);
            PAdESSignatureParameters signatureParams = padesCryptoService.getSignatureParameters(model, null);

            return new DataToSignHolder(
                    Optional.ofNullable(padesCryptoService.getDataToSign(dssDocument, signatureParams))
                            .map(dts -> new DataToSign(
                                    StringUtils.firstNonEmpty(multipartFile.getOriginalFilename(), UUID.randomUUID().toString()),
                                    null,
                                    dts.getDataToSignBase64(),
                                    null
                            ))
                            .map(Collections::singletonList)
                            .orElse(emptyList()),
                    signatureParams.bLevel().getSigningDate().getTime()
            );
        }
    }


    @PostMapping(value = API_V3 + "/pades/generateSignature", consumes = MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "Inject the PAdES signature and return the PDF file")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = APPLICATION_PDF_VALUE)),
            @ApiResponse(responseCode = CODE_500, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void getPadesSignature(@RequestPart PadesParameters model,
                                  @RequestPart(value = "file") final MultipartFile document,
                                  @NotNull HttpServletResponse response) throws IOException {

        log.info("getPadesSignature");
        log.debug("getPadesSignature - model : {}", model);

        // Integrity check

        DataToSign documentToSign = Optional.ofNullable(model.getDataToSignList())
                .filter(l -> l.size() == 1)
                .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, "The dataToSign list should contain a single element"))
                .stream()
                .findFirst()
                .filter(d -> StringUtils.isNotEmpty(d.getDataToSignBase64()))
                .filter(d -> StringUtils.isNotEmpty(d.getSignatureValue()))
                .orElseThrow(() -> {
                    log.error("Missing dataToSign or signature in request, abort");
                    return new ResponseStatusException(BAD_REQUEST, "Missing dataToSign or signature in request");
                });

        PublicKey pubKey = StringsUtils.parsePublicKey(model.getPublicCertificateBase64())
                .orElseThrow(() -> {
                    log.error("Cannot parse given public key, abort");
                    return new ResponseStatusException(BAD_REQUEST, "Cannot parse given cert");
                });

        if (!StringsUtils.verify(documentToSign.getDataToSignBase64(), documentToSign.getSignatureValue(),
                pubKey, SignatureAlgorithm.getAlgorithm(RSA, model.getDigestAlgorithm().getDssValue()))) {
            log.error("The given signature is not acceptable, abort");
            throw new ResponseStatusException(NOT_ACCEPTABLE, "The given signature is not acceptable");
        }

        String digestBase64 = Optional.ofNullable(PadesCryptoService.getDigestBase64(documentToSign.getDataToSignBase64()))
                .orElseThrow(() -> {
                    log.error("Cannot read hash from dataToSign, abort");
                    return new ResponseStatusException(BAD_REQUEST, "Cannot read hash from dataToSign");
                });

        if (model.getSignatureDateTime() == null) {
            log.error("Missing signature date, abort");
            throw new ResponseStatusException(BAD_REQUEST, "The signature time must be properly set");
        }

        // Building signature

        try (InputStream documentInputStream = document.getInputStream();
             OutputStream responseOutputStream = response.getOutputStream()) {
            DSSDocument parsedDocument = new StreamableDssDocument(documentInputStream, responseOutputStream);
            PAdESSignatureParameters params = padesCryptoService.getSignatureParameters(model, digestBase64);

            response.setHeader(CONTENT_DISPOSITION, "attachment; filename=" + document.getOriginalFilename());
            padesCryptoService.getSignature(params, parsedDocument, documentToSign.getSignatureValue());
        }
    }


}
