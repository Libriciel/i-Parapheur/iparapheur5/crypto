/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.services.impl;

import coop.libriciel.crypto.models.DataToSign;
import coop.libriciel.crypto.models.SignatureFormat;
import coop.libriciel.crypto.models.request.XadesParameters;
import coop.libriciel.crypto.services.CryptoService;
import coop.libriciel.crypto.utils.LocalizedStatusException;
import coop.libriciel.crypto.utils.PesV2XAdESSignatureParameters;
import eu.europa.esig.dss.enumerations.SignatureLevel;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.Policy;
import eu.europa.esig.dss.model.SignatureValue;
import eu.europa.esig.dss.model.ToBeSigned;
import eu.europa.esig.dss.spi.DSSUtils;
import eu.europa.esig.dss.utils.Utils;
import eu.europa.esig.dss.xades.reference.*;
import eu.europa.esig.dss.xades.signature.XAdESService;
import eu.europa.esig.dss.xml.common.definition.DSSNamespace;
import eu.europa.esig.dss.xml.utils.DomUtils;
import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import static coop.libriciel.crypto.models.SignatureFormat.XADES;
import static eu.europa.esig.dss.enumerations.DigestAlgorithm.SHA256;
import static eu.europa.esig.dss.enumerations.SignatureAlgorithm.RSA_SHA256;
import static eu.europa.esig.dss.enumerations.SignatureLevel.XAdES_BASELINE_B;
import static eu.europa.esig.dss.enumerations.SignaturePackaging.ENVELOPED;
import static eu.europa.esig.dss.xades.definition.XAdESNamespace.XADES_122;
import static eu.europa.esig.dss.xades.definition.XAdESNamespace.XADES_132;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static javax.xml.crypto.dsig.CanonicalizationMethod.EXCLUSIVE;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;


@Log4j2
@Service
public class XadesCryptoService extends CryptoService<PesV2XAdESSignatureParameters> {


    public static final String PES_V2_SPLIT_XPATH = "//Bordereau";
    public static final String PES_V2_GLOBAL_XPATH = "/*";
    public static final String PES_V2_ID_TO_SIGN = "Id";

    /**
     * The "xad" prefix is a part of the Helios specs, the regular "xades122" prefix will be refused.
     * We have to (sadly) force this prefix.
     */
    public static final DSSNamespace XADES_122_HELIOS = new DSSNamespace(XADES_122.getUri(), "xad");


    static {
        DomUtils.registerNamespace(XADES_122_HELIOS);
    }


    private final @Getter Supplier<PesV2XAdESSignatureParameters> signatureParametersSupplier = PesV2XAdESSignatureParameters::new;
    private final @Getter SignatureLevel signatureLevel = XAdES_BASELINE_B;
    private final @Getter SignatureFormat signatureFormat = XADES;

    private @Value("${pesv2_sha256.policy_id}") String policyId;
    private @Value("${pesv2_sha256.policy_digest}") String policyDigest;
    private @Value("${pesv2_sha256.policy_desc}") String policyDesc;
    private @Value("${pesv2_sha256.spuri}") String spuri;
    private Policy dgfipPolicy;


    // <editor-fold desc="Beans">


    private final XAdESService xAdESService;


    @Autowired
    public XadesCryptoService(XAdESService xAdESService) {
        this.xAdESService = xAdESService;
    }


    @PostConstruct
    public void setup() {
        dgfipPolicy = new Policy();
        dgfipPolicy.setId(policyId);
        dgfipPolicy.setDescription(policyDesc);
        dgfipPolicy.setDigestAlgorithm(SHA256);
        dgfipPolicy.setDigestValue(Base64.getDecoder().decode(policyDigest));
        dgfipPolicy.setSpuri(spuri);
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Signature parameters">


    public PesV2XAdESSignatureParameters getSignatureParameters(@NotNull DSSDocument toSignDocument,
                                                                @NotNull String currentId,
                                                                @NotNull XadesParameters params) {
        log.debug("getSignatureParameters");

        PesV2XAdESSignatureParameters signatureParameters = super.getSignatureParameters(params);
        signatureParameters.setSignaturePackaging(params.getSignaturePackaging().getDssValue());

        DSSReference reference = new DSSReference();
        reference.setDigestMethodAlgorithm(SHA256);
        reference.setContents(toSignDocument);
        signatureParameters.setReferences(singletonList(reference));

        // We only use enveloped signature on PES
        if (params.getSignaturePackaging().getDssValue() == ENVELOPED) {

            // XAdES 1.2.2 specific parameters
            signatureParameters.setSigningCertificateDigestMethod(SHA256);
            signatureParameters.setXadesNamespace(XADES_122_HELIOS);
            signatureParameters.setEn319132(false);
            signatureParameters.setXPathLocationString("%s[@%s='%s']".formatted(params.getXpath(), PES_V2_ID_TO_SIGN, currentId));
            signatureParameters.bLevel().setSignaturePolicy(dgfipPolicy);

            reference.setUri("#%s".formatted(currentId));
            reference.setTransforms(asList(new EnvelopedSignatureTransform(), new CanonicalizationTransform(EXCLUSIVE)));
        } else {
            signatureParameters.setXadesNamespace(XADES_132);
            reference.setId("r-%s".formatted(currentId));
        }

        return signatureParameters;
    }


    public Pair<String, NodeList> getPesV2XpathAndNodeListToSign(DSSDocument dssDocument, @Nullable String forcedXpath) {
        log.debug("getPesV2XpathAndNodeListToSign");
        Document document = DomUtils.buildDOM(dssDocument);
        NodeList nodeList;
        String xpath;

        if (StringUtils.isNotEmpty(forcedXpath)) {
            log.debug("getPesV2XpathAndNodeListToSign - using forced xpath : {}", forcedXpath);
            xpath = forcedXpath;
            nodeList = DomUtils.getNodeList(document, xpath);

            // We crash if there is no result to sign
            if (hasSomeUndefinedId(nodeList)) {
                log.error("At least one  element in the PES document matching the passed Xpath has no Id and cannot be signed. Aborting.");
                throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.no_data_to_sign_at_the_given_xpath");
            }

        } else {
            log.debug("getPesV2XpathAndNodeListToSign - using default xpath");
            // Search mode, the split-xpath has the priority
            xpath = PES_V2_SPLIT_XPATH;
            nodeList = DomUtils.getNodeList(document, xpath);

            if (hasSomeUndefinedId(nodeList)) {
                // Fallback on global-xpath
                xpath = PES_V2_GLOBAL_XPATH;
                nodeList = DomUtils.getNodeList(document, xpath);

                // We crash if there is no result to sign
                if (hasSomeUndefinedId(nodeList)) {
                    log.error("The PES document has no acceptable element with an appropriate Id to be signed. Aborting.");
                    throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.no_data_to_sign_in_the_given_document");
                }
            }
        }

        return Pair.of(xpath, nodeList);
    }


    private static boolean hasSomeUndefinedId(@NotNull NodeList nodeList) {

        //noinspection ConstantConditions
        List<String> foundIds = IntStream.range(0, nodeList.getLength())
                .mapToObj(nodeList::item)
                .filter(Objects::nonNull)
                .map(Node::getAttributes)
                .filter(Objects::nonNull) // Irrelevant Warning here : the getAttributes can actually be null
                .map(attributes -> attributes.getNamedItem(PES_V2_ID_TO_SIGN))
                .filter(Objects::nonNull)
                .map(Node::getNodeValue)
                .toList();

        boolean noNodeFound = nodeList.getLength() == 0;
        boolean someIdIsMissing = foundIds.size() != nodeList.getLength();
        boolean someIdIsEmpty = foundIds.stream().anyMatch(StringUtils::isEmpty);
        return noNodeFound || someIdIsMissing || someIdIsEmpty;
    }


    // </editor-fold desc="Signature parameters">


    @Override
    public DataToSign getDataToSign(DSSDocument toSign, PesV2XAdESSignatureParameters signatureParams) {
        log.debug("getDataToSign");
        ToBeSigned toBeSigned = xAdESService.getDataToSign(toSign, signatureParams);
        String dataToSignBase64 = Base64.getEncoder().encodeToString(toBeSigned.getBytes());
        return new DataToSign(signatureParams.getDeterministicId(), null, dataToSignBase64, null);
    }


    @Override
    public String getSignature(PesV2XAdESSignatureParameters params, DSSDocument toSign, String base64Signature) {
        SignatureValue signatureValue = new SignatureValue(params.getSignatureAlgorithm(), Utils.fromBase64(base64Signature));
        DSSDocument signedDocument = xAdESService.signDocument(toSign, params, signatureValue);
        return Base64.getEncoder().encodeToString(DSSUtils.toByteArray(signedDocument));
    }


    public DSSDocument signDocument(PesV2XAdESSignatureParameters params, DSSDocument toSign, String signatureBase64) {
        SignatureValue signatureValue = new SignatureValue(RSA_SHA256, Utils.fromBase64(signatureBase64));
        return xAdESService.signDocument(toSign, params, signatureValue);
    }


}
