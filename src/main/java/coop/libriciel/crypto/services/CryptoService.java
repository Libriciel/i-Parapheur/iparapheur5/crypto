/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.crypto.services;

import coop.libriciel.crypto.models.DataToSign;
import coop.libriciel.crypto.models.request.CadesParameters;
import coop.libriciel.crypto.services.impl.PadesCryptoService;
import eu.europa.esig.dss.enumerations.DigestAlgorithm;
import eu.europa.esig.dss.enumerations.SignatureLevel;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.DigestDocument;
import eu.europa.esig.dss.model.SignerLocation;
import eu.europa.esig.dss.model.TimestampParameters;
import eu.europa.esig.dss.signature.AbstractSignatureParameters;
import eu.europa.esig.dss.spi.DSSUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.Optional;
import java.util.function.Supplier;


@Log4j2
public abstract class CryptoService<T extends AbstractSignatureParameters<? extends TimestampParameters>> {


    // <editor-fold desc="Utils">


    @Deprecated
    public static DSSDocument generateDigestDocument(DataToSign dataToSign, DigestAlgorithm algorithm) {

        String currentDigest = Optional.ofNullable(dataToSign.getDataToSignBase64())
                .map(PadesCryptoService::getDigestBase64)
                .orElse(dataToSign.getDigestBase64());

        if (StringUtils.isEmpty(currentDigest)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "digestBase64 property is mandatory for detached signature");
        }

        DigestDocument toSignDocument = new DigestDocument();
        toSignDocument.setName(dataToSign.getId());
        toSignDocument.addDigest(algorithm, currentDigest);

        return toSignDocument;
    }


    // </editor-fold desc="Utils">


    // <editor-fold desc="Signature parameters">


    protected abstract SignatureLevel getSignatureLevel();


    protected abstract Supplier<T> getSignatureParametersSupplier();

    protected T getSignatureParameters(@NotNull CadesParameters model) {

        T signatureParameters = getSignatureParametersSupplier().get();
        signatureParameters.setSignatureLevel(getSignatureLevel());
        signatureParameters.setSigningCertificate(DSSUtils.loadCertificateFromBase64EncodedString(model.getPublicCertificateBase64()));
        signatureParameters.setDigestAlgorithm(model.getDigestAlgorithm().getDssValue());
        signatureParameters.bLevel().setSigningDate(new Date(model.getSignatureDateTime()));
        signatureParameters.bLevel().setClaimedSignerRoles(model.getClaimedRoles());

        Optional.of(model)
                .filter(m -> !StringUtils.isAllEmpty(m.getCountry(), m.getCity(), m.getZipCode()))
                .map(m -> {
                    SignerLocation location = new SignerLocation();
                    location.setCountry(m.getCountry());
                    location.setPostalCode(m.getZipCode());
                    location.setLocality(m.getCity());
                    return location;
                })
                .ifPresent(l -> signatureParameters.bLevel().setSignerLocation(l));

        return signatureParameters;
    }


    // </editor-fold desc="Signature parameters">


    public abstract DataToSign getDataToSign(DSSDocument toSign, T signatureParams);


    public abstract String getSignature(T params, DSSDocument toSign, String base64Signature);


}
