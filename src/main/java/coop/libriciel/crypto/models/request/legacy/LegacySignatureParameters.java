/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.models.request.legacy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import coop.libriciel.crypto.models.request.CadesParameters;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@Deprecated
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LegacySignatureParameters extends CadesParameters {


    private List<String> signatureBase64List;


    public LegacySignatureParameters(CadesParameters parameters) {
        super(
                parameters.getDataToSignList(),
                parameters.getSignatureFormat(),
                parameters.getPublicCertificateBase64(),
                parameters.getDigestAlgorithm(),
                parameters.getSignatureDateTime(),
                parameters.getPayload(),
                null,
                parameters.getCountry(),
                parameters.getCity(),
                parameters.getZipCode()
        );
    }

}
