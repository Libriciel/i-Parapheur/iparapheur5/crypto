/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.models.stamp;

import coop.libriciel.crypto.utils.DocumentUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.util.function.Function;


@Data
public class PdfSignatureStampElement {


    @Schema(enumAsRef = true)
    public enum SignatureStampElementType {
        TEXT,
        IMAGE
    }


    @Getter
    @AllArgsConstructor
    @Schema(enumAsRef = true)
    public enum SignatureStampFont {

        TIMES_ROMAN(doc -> PDType1Font.TIMES_ROMAN),
        TIMES_BOLD(doc -> PDType1Font.TIMES_BOLD),
        TIMES_ITALIC(doc -> PDType1Font.TIMES_ITALIC),
        TIMES_BOLD_ITALIC(doc -> PDType1Font.TIMES_BOLD_ITALIC),

        HELVETICA(doc -> PDType1Font.HELVETICA),
        HELVETICA_BOLD(doc -> PDType1Font.HELVETICA_BOLD),
        HELVETICA_OBLIQUE(doc -> PDType1Font.HELVETICA_OBLIQUE),
        HELVETICA_BOLD_OBLIQUE(doc -> PDType1Font.HELVETICA_BOLD_OBLIQUE),

        COURIER(doc -> PDType1Font.COURIER),
        COURIER_BOLD(doc -> PDType1Font.COURIER_BOLD),
        COURIER_OBLIQUE(doc -> PDType1Font.COURIER_OBLIQUE),
        COURIER_BOLD_OBLIQUE(doc -> PDType1Font.COURIER_BOLD_OBLIQUE),

        MARIANNE(doc -> DocumentUtils.loadFont(doc, "fonts/Marianne-Regular.ttf")),
        MARIANNE_BOLD(doc -> DocumentUtils.loadFont(doc, "fonts/Marianne-Bold.ttf")),
        MARIANNE_ITALIC(doc -> DocumentUtils.loadFont(doc, "fonts/Marianne-RegularItalic.ttf")),
        MARIANNE_BOLD_ITALIC(doc -> DocumentUtils.loadFont(doc, "fonts/Marianne-BoldItalic.ttf")),

        SPECTRAL(doc -> DocumentUtils.loadFont(doc, "fonts/Spectral-Regular.ttf")),
        SPECTRAL_BOLD(doc -> DocumentUtils.loadFont(doc, "fonts/Spectral-Bold.ttf")),
        SPECTRAL_ITALIC(doc -> DocumentUtils.loadFont(doc, "fonts/Spectral-Italic.ttf")),
        SPECTRAL_BOLD_ITALIC(doc -> DocumentUtils.loadFont(doc, "fonts/Spectral-BoldItalic.ttf")),

        DANCING_SCRIPT(doc -> DocumentUtils.loadFont(doc, "fonts/DancingScript-Regular.ttf")),
        DANCING_SCRIPT_BOLD(doc -> DocumentUtils.loadFont(doc, "fonts/DancingScript-Bold.ttf")),

        CAVEAT(doc -> DocumentUtils.loadFont(doc, "fonts/Caveat-Regular.ttf")),
        CAVEAT_BOLD(doc -> DocumentUtils.loadFont(doc, "fonts/Caveat-Bold.ttf")),

        SATISFY(doc -> DocumentUtils.loadFont(doc, "fonts/Satisfy-Regular.ttf"));

        private final Function<PDDocument, PDFont> pdFontLoader;

    }


    private SignatureStampElementType type;
    private String value;

    private SignatureStampFont font;
    private Integer fontSize;
    private String colorCode;

    private Float x;
    private Float y;
    private Float width;
    private Float height;

}
