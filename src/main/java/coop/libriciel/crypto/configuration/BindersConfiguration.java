/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.configuration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.crypto.models.request.CadesParameters;
import coop.libriciel.crypto.models.request.PadesParameters;
import coop.libriciel.crypto.models.request.XadesParameters;
import coop.libriciel.crypto.models.request.legacy.LegacyPadesSignatureParameters;
import coop.libriciel.crypto.models.request.legacy.LegacySignatureParameters;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditorSupport;
import java.util.stream.Stream;


@Log4j2
@ControllerAdvice
public class BindersConfiguration {


    /**
     * Define the binder manually, cause spring cannot handle multipart and object
     */
    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {

        Stream.of(CadesParameters.class, PadesParameters.class, XadesParameters.class, LegacyPadesSignatureParameters.class, LegacySignatureParameters.class)
                .forEach(cz -> dataBinder.registerCustomEditor(
                        cz,
                        new PropertyEditorSupport() {

                            Object value;


                            @Override
                            public Object getValue() {
                                return value;
                            }


                            @Override
                            public void setAsText(String text) {
                                try {
                                    value = new ObjectMapper().readValue(text, cz);
                                } catch (JsonProcessingException e) {
                                    log.error("Error on reading SignatureModel", e);
                                }
                            }
                        })
                );
    }


}
