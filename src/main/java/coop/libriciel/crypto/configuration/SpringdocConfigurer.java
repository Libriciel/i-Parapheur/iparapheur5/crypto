/*
 * Crypto
 * Copyright (C) 2018-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.crypto.configuration;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Paths;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.media.Schema;
import org.apache.commons.lang3.StringUtils;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;

import static coop.libriciel.crypto.MainApplication.*;
import static org.apache.commons.lang3.StringUtils.EMPTY;


@Configuration
@ConditionalOnProperty(value = "springdoc.api-docs.enabled", havingValue = "true")
public class SpringdocConfigurer {


    private @Value("${application.version?:DEVELOP}") String version;
    private @Value("${info.application.name}") String applicationName;
    private @Value("${info.application.author.name") String authorName;


    @Bean
    public OpenAPI apiInfo() {
        return new OpenAPI()
                .info(new Info()
                        .title(applicationName)
                        .description(
                                """
                                i-Parapheur v5.x Crypto application.

                                Contains every crypto-related logic. Prepare signatures for mobile devices.
                                """
                        )
                        .version(version)
                        .license(new License().name("Affero GPL 3.0").url("https://www.gnu.org/licenses/agpl-3.0.en.html"))
                        .contact(new Contact().name(authorName).url("https://libriciel.fr").email("iparapheur@libriciel.coop"))
                )
                .externalDocs(new ExternalDocumentation().url("https://www.libriciel.fr/i-parapheur/"));
    }


    /**
     * We need to customize 2 things here:
     * - Schemas that aren't natively sorted alphabetically
     * - API context path that should not be in the server variable
     *
     * @param environment An auto-wired environment
     * @return OpenApiCustomiser
     * @see <a href="https://github.com/springdoc/springdoc-openapi/issues/1459">The issue that talk about this</a>, and the source of this fix.
     * @see <a href="https://github.com/springdoc/springdoc-openapi/issues/741#issuecomment-1165401132">The issue that talk about schema sorting</a>, and the source of this fix.
     */
    private OpenApiCustomizer contextPathCustomizer(Environment environment) {
        String contextPath = environment.getProperty("server.servlet.context-path", "/");
        return openApi -> {

            // Sort Schemas alphabetically
            // This should definitely be a native option, like any other YAML parameter.
            // But for some reason it is not

            //noinspection rawtypes
            Map<String, Schema> schemas = openApi.getComponents().getSchemas();
            openApi.getComponents().setSchemas(new TreeMap<>(schemas));

            // For everything Crypto-related, we have a context-path, the /crypto/api.
            // In the OpenApi v3 specs, this context-path is automatically added to the root server URL.
            // Yet, we don't want that.

            if (!StringUtils.equals("/", contextPath)) {

                // Strip context path from the "server"
                openApi.getServers().forEach(server -> server.setUrl(server.getUrl().replace(contextPath, EMPTY)));

                // Add context path to each "path"
                Paths openApiPaths = openApi.getPaths();
                HashSet<String> pathsToDelete = new HashSet<>();
                Paths tempOpenApiPaths = new Paths();

                // Do everything in temp objects to not mess up iteration
                openApiPaths.forEach((key, value) -> {
                    tempOpenApiPaths.put(contextPath + key, value);
                    pathsToDelete.add(key);
                });

                pathsToDelete.forEach(openApiPaths::remove);
                openApiPaths.putAll(tempOpenApiPaths);
            }
        };
    }


    @Bean
    public GroupedOpenApi apiV1(Environment environment) {
        return GroupedOpenApi.builder()
                .group(API_V1)
                .pathsToMatch("/api/cades/**", "/api/pades/**", "/api/pesv2/**", "/api/xades/**")
                .addOpenApiCustomizer(contextPathCustomizer(environment))
                .build();
    }


    @Bean
    public GroupedOpenApi apiV2(Environment environment) {
        return GroupedOpenApi.builder()
                .group(API_V2)
                .pathsToMatch("/api/%s/**".formatted(API_V2))
                .addOpenApiCustomizer(contextPathCustomizer(environment))
                .build();
    }


    @Bean
    public GroupedOpenApi apiV3(Environment environment) {
        return GroupedOpenApi.builder()
                .group(API_V3)
                .pathsToMatch("/api/%s/**".formatted(API_V3))
                .addOpenApiCustomizer(contextPathCustomizer(environment))
                .build();
    }


}
