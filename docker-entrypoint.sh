#!/bin/bash

#
# Crypto
# Copyright (C) 2018-2024 Libriciel-SCOP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

echo "running ip-core entrypoint script"

if [[ -n "$*" ]]; then
  echo "running additional commands : $@"
  /bin/bash -c "$*"
fi

echo "running main command"

java -XX:+UseContainerSupport -XX:MaxRAMPercentage=75.0 -Duser.country=FR -Duser.language=fr $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /crypto.jar
