# Contributing

## Importing project

##### Setup in IntelliJ :

- Preferences → Plugins...
- Search and install `Lombok`
- File → Open...
- Open the `build.gradle` file
- Enable Auto-import
- Go to Preferences → Build, Execution, Deployment → Compiler → Annotation Processors
- Check Enable annotation processing

##### Code style

The `.idea` folder has been committed. It should bring the appropriate code-style.  
Obviously, every modified file should be auto-formatted before any git push.  
No hook would reject a mis-formatted file for now... But if everything goes bananas, we may set it up.

## Unit tests

Set in `src/test`, these tests should have an extensive coverage, on any parameters.  
Every test method should be callable individually, mocking every state-related data.  
Code coverage is computed here, we target the highest percentage possible.

```bash
$ gradle clean test
```

## Integration tests

Set in `src/intTest`, these tests should mock list API calls made by known clients, and focus on the REST API testing.  
By convention, each classes shall be named `ClientVersionIntegrationTest`.

These should cover every entry point actually used by the client's version.  
This should prevent any breaking changes and assure compatibility if/when a refactor is needed.  
For the same reason, it is wise to NOT test more that what is actually used by the mocked client.

Object mapping is prohibited here, to ensure a carved-into-stone stability.  
In-and-out JSON should be a hardcoded `String`.

Out of support client/version targeted classes should be deleted.

```bash
$ gradle clean integrationTest
```
