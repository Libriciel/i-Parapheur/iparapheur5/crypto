#
# Crypto
# Copyright (C) 2018-2024 Libriciel-SCOP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

FROM hubdocker.libriciel.fr/eclipse-temurin:17.0.12_7-jre-jammy@sha256:6c4c0938462c9678fe380bf01c7da7f4242dc20f54362f24ced3bb70f6e0183a

HEALTHCHECK --interval=5s --timeout=2s --retries=60 \
  CMD curl --fail --silent http://localhost:8080/crypto/actuator/health | grep UP || exit 1

# Open Containers Initiative parameters
ARG CI_COMMIT_REF_NAME=""
ARG CI_COMMIT_SHA=""
ARG CI_PIPELINE_CREATED_AT=""
# Non-standard and/or deprecated variables, that are still widely used.
# If it is already set in the FROM image, it has to be overridden.
MAINTAINER Libriciel SCOP
LABEL maintainer="Libriciel SCOP"
LABEL org.label-schema.build-date="$CI_PIPELINE_CREATED_AT"
LABEL org.label-schema.name="ip-workflow"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.vendor="Libriciel SCOP"
# Open Containers Initiative's image specifications
LABEL org.opencontainers.image.authors="Libriciel SCOP"
LABEL org.opencontainers.image.created="$CI_PIPELINE_CREATED_AT"
LABEL org.opencontainers.image.description="A webservice wrapper around the SD-DSS library"
LABEL org.opencontainers.image.licenses="GNU Affero GPL v3"
LABEL org.opencontainers.image.revision="$CI_COMMIT_SHA"
LABEL org.opencontainers.image.source="registry.libriciel.fr:443/public/signature/core"
LABEL org.opencontainers.image.title="ip-workflow"
LABEL org.opencontainers.image.vendor="Libriciel SCOP"
LABEL org.opencontainers.image.version="$CI_COMMIT_REF_NAME"

# The docker image does not contain any other language than English by default.
# Additional compatible languages shall be installed at build-time.
RUN locale-gen en_US.UTF-8
RUN locale-gen fr_FR.UTF-8
# Then, languages can be set at runtime, with a regular environment variable.
# Timezones aren't affected by the build, and any timezone would work.
ENV LANG="fr_FR.UTF-8"
ENV LANGUAGE="fr_FR:en_US:en"
ENV LC_ALL="fr_FR.UTF-8"
ENV TZ="Europe/Paris"

# Adding fonts
RUN apt update
RUN apt install -y fontconfig fonts-dejavu-core
RUN echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections
RUN apt install -y ttf-mscorefonts-installer

VOLUME /tmp

ADD build/libs/crypto-*.jar crypto.jar
ENV JAVA_OPTS=""

COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
RUN ln -s /usr/local/bin/docker-entrypoint.sh /

ENTRYPOINT [ "docker-entrypoint.sh" ]
