#
# Crypto
# Copyright (C) 2018-2024 Libriciel-SCOP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

include:
  - component: $LIBRICIEL_CATALOG_PATH/renovate-runner/runner@1.1.1
  - component: $LIBRICIEL_CATALOG_PATH/gradle-and-spring-boot/prepare-cache@1.4.0
  - component: $LIBRICIEL_CATALOG_PATH/gradle-and-spring-boot/openapi-definition@1.4.0
  - component: $LIBRICIEL_CATALOG_PATH/gradle-and-spring-boot/build-jar@1.4.0
  - component: $LIBRICIEL_CATALOG_PATH/gradle-and-spring-boot/integration-test@1.4.0
  - component: $LIBRICIEL_CATALOG_PATH/gradle-and-spring-boot/junit@1.4.0
  - component: $LIBRICIEL_CATALOG_PATH/openapi-library-generate-and-deploy/jobs@1.1.1
    inputs:
      project_name: "crypto"
      java: "true"
      openapi_json_path: "./build/docs/${CI_PROJECT_NAME}-openapi.json"
  - component: $LIBRICIEL_CATALOG_PATH/software-release/build@2.0.5
  - component: $LIBRICIEL_CATALOG_PATH/software-release/publish-docker@2.0.5
  - template: Code-Quality.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml


# <editor-fold desc=".pre">


openapi-definition:
  needs: [ prepare-gradle-cache ]


prepare-gradle-cache:
  needs: [ ]


renovate:
  needs: [ ]


# </editor-fold desc=".pre">


# <editor-fold desc="Build">


build-jar:
  before_script:
    # Note : We're not using any env var for the Sentry DSN, because we don't want it to be overridden.
    # We don't want to commit this value either, so a sed seems to be the way to go.
    - sed -i "s/SENTRY_URL_WITH_DSN/$SENTRY_URL_WITH_DSN/g" src/main/resources/application.yml


kaniko-build:
  needs: [ build-jar ]


# </editor-fold desc="Build">


# <editor-fold desc="Test">


code_quality:
  needs: [ ]
  tags: [ cq-sans-dind ]


container_scanning:
  needs: [ kaniko-build ]


gemnasium-maven-dependency_scanning:
  needs: [ ]


integration-test:
  needs: [ prepare-gradle-cache ]


junit:
  needs: [ prepare-gradle-cache ]


secret_detection:
  needs: [ ]


semgrep-sast:
  needs: [ ]


# </editor-fold desc="Test">


# <editor-fold desc="Deploy">


publish-docker-to-nexus:
  needs: [ junit, integration-test, kaniko-build ]


# </editor-fold desc="Deploy">

