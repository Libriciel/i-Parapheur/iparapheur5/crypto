Crypto
======

[![pipeline](https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/badges/develop/pipeline.svg)](https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/commits/develop) [![coverage](https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/badges/develop/coverage.svg)](https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-crypto/-/commits/develop) [![lines_of_code](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Acrypto&metric=ncloc)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Acrypto)  
[![quality_gate](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Acrypto&metric=alert_status)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Acrypto) [![maintenability](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Acrypto&metric=sqale_rating)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Acrypto) [![reliability](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Acrypto&metric=reliability_rating)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Acrypto) [![security](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Acrypto&metric=security_rating)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Acrypto)

A webservice wrapper around the SD-DSS project.

## Public API

Launch the MainApplication in dev mode, and find the Swagger2 location here :  
http://localhost:8085/crypto/swagger-ui.html
